'use strict';

var Account = require('../models/account');
var AccountRepository = require('../repositories/accountRepository');
var Log = require('../logging/log');


function handleError(err,res){
    Log.debug('error:' + err);
    res.json(404,{error:'Not found'});
}
// Create a new account
// ----------------------------------------------------
function createAccount(req, res) {

    var account = new Account({
        username: req.body.username,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email
        //facebookUserId: facebookUserId || null
    });

    AccountRepository.createAccount(account).then(
        function (account) {
            res.json(201, account);
        },
        function (err) {
            handleError(err,res);
        }
    );
}

// List all accounts
// ----------------------------------------------------
function getAccounts(req, res) {
    Log.debug('list');

    AccountRepository.list().then(
        function (accounts) {
            Log.debug('length:' + accounts.length);
            res.json(200, account);
        },
        function (err) {
            handleError(err,res);
        }
    );
}

// Get a account
// ----------------------------------------------------
function getAccount(req, res) {
    AccountRepository.getAccountById(req.params.account_id).then(
        function (account) {
            if(!account){
                Log.debug('acccount not found');
                res.json(404,{error:'Not found'});
            }else {
                Log.debug(account.username);
                Log.debug('get:' + account.username);
                res.json(200, account);
            }
        },
        function (err) {
            handleError(err,res);
        }
    );
}

// Update account data
// ----------------------------------------------------
function updateAccount(req, res) {

    var account = new Account({
        username: req.body.username,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email
        //facebookUserId: facebookUserId || null
    });

    AccountRepository.updateAccount(req.params.account_id, account).then(
        function (account) {
            Log.debug(account.username);
            res.json(200, account);
        },
        function (err) {
            handleError(err,res);
        }
    );

}

// Delete account
// ----------------------------------------------------
function deleteAccount(req, res) {
    Log.debug('deleting' + req.params.account_id);
    AccountRepository.deleteAccount(req.params.account_id).then(
        function () {
            Log.debug('deleted' );
            res.json(204,{});
        },
        function (err) {
            handleError(err,res);
        }
    );
}


// Exports
// ----------------------------------------------------
module.exports = {
    create: createAccount,
    list: getAccounts,
    get: getAccount,
    update: updateAccount,
    delete: deleteAccount
};