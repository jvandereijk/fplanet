function setup(router, handlers) {

    var Account = require('./models/account');

    // middleware to use for all requests
    router.use(function (req, res, next) {
        // do logging
        console.log('Something is happening.');
        next(); // make sure we go to the next routes and don't stop here
    });

    // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
    router.get('/', function (req, res) {
        res.json({ message: 'Home of the account api' });
    });

    // on routes that end in /accounts
    // ----------------------------------------------------
    router.route('/accounts')
        // create a account (accessed at POST http://localhost:8080/api/accounts)
        .post(handlers.account.create)
        // get all the accounts (accessed at GET http://localhost:8080/api/accounts)
        .get(handlers.account.list);

    // on routes that end in /accounts/:account_id
    // ----------------------------------------------------
    router.route('/accounts/:account_id')

        // get the account with that id (accessed at GET http://localhost:8080/api/accounts/:account_id)
        .get(handlers.account.get)
        // update the account with this id (accessed at PUT http://localhost:8080/api/accounts/:account_id)
        .put(handlers.account.update)
        // delete the account with this id (accessed at DELETE http://localhost:8080/api/accounts/:account_id)
        .delete(handlers.account.delete);
}


// Exports
// ----------------------------------------------------

exports.setup = setup;