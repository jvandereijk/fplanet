'use strict';

var Account = require('../models/account');
var Log = require('../logging/log');
var Q = require('q');

function list() {
    var deferred = Q.defer();
    Log.debug('list');

    Account.find(function (err, accounts) {
        if (err) {
            deferred.reject(new Error(err));
        }else {
            deferred.resolve(accounts);
        }
    });
    return deferred.promise;
}
function getAccountById(id) {
    var deferred = Q.defer();
    Log.debug('getAccountById');
    Account.findById(id, function (err, account) {
        if (err) {
            Log.debug('err get:'+err);
            deferred.reject(new Error(err));
        }else {
            deferred.resolve(account);
        }
    });
    return deferred.promise;
}

function createAccount(newAccount) {
    var deferred = Q.defer();

    Log.debug('createAccount');

    newAccount.save(function (err, account) {
        if (err) {
            Log.debug('err:'+err);
            deferred.reject(new Error(err));
        }else {
            deferred.resolve(account);
        }
    });

    return deferred.promise;
}

function updateAccount(id, newAccount) {
    var deferred = Q.defer();
    Log.debug('updateAccount');

    // use our account model to find the account we want
    Account.findById(id, function (err, account) {
        if (err) {
            Log.debug('err account not found');
            deferred.reject(new Error(err));
        }else {
            account.username = newAccount.username;
            account.password = newAccount.password;
            account.firstName = newAccount.firstName;
            account.lastName = newAccount.lastName;
            account.email = newAccount.email;
            //account.facebookUserId:

            // save the account
            account.save(function (err) {
                if (err) {
                    Log.debug('err:' + err);
                    deferred.reject(new Error(err));
                }else {
                    console.log('saved');
                    deferred.resolve(account);
                }
            });
        }
    });

    return deferred.promise;
}

function deleteAccount(id) {
    var deferred = Q.defer();

    Log.debug('deleteAccount');

    Account.remove({ '_id': id }, function (err, account) {
        if (err) {
            Log.debug('err:'+err);
            deferred.reject(new Error(err));
        }else {
            Log.debug('deleted.');
            deferred.resolve(account);
        }
    });

    return deferred.promise;
}

module.exports = {
    list: list,
    getAccountById: getAccountById,
    createAccount: createAccount,
    updateAccount: updateAccount,
    deleteAccount: deleteAccount
}