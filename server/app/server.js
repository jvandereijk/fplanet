// server.js

// BASE SETUP
// =============================================================================

var port = process.env.PORT || 9076; 		// set our port

// call the packages we need
var fs = require('fs');
var express = require('express'); 		// call express
var app = express(); 				// define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser());


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); 				// get an instance of the express Router
var accountHandler = require('./handlers/accountHandler');
var handlers = { account: accountHandler};
var routes = require('./Routes');

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);


// START THE SERVER
// =============================================================================
function start() {
    routes.setup(router, handlers);
    app.listen(port);
    console.log("Express server listening on port %d in %s mode", port, app.settings.env);
}
// *******************************************************
exports.start = start;
exports.app = app;