'use strict';


function debug(msg){console.log(msg);}

function warn(msg){console.warn(msg);}

function error(msg){console.error(msg);}


// Exports
// ----------------------------------------------------
module.exports = {
    debug: debug,
    warn: warn,
    error: error
};