var config = require('./config-debug');
var mongoose = require('mongoose');
var server = require('./server');

console.log("Connecting to MongoDB...");

mongoose.connect(config.db.mongodb);
console.log("Successfully connected to MongoDB. Starting web server...");

server.start();
console.log("Successfully started web server. Waiting for incoming connections...");