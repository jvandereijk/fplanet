'use strict';

var should = require('should');
var assert = require('assert');
var request = require('supertest');


describe('AccountHandler', function () {
    var url = 'http://localhost:9076/api/accounts';
    var newAccount = {
            username: 'handler_test_user',
            password: 'handler_test_pwd',
            email: 'handler_test@jackvandereijk.nl',
            firstName: 'handler_test_first',
            lastName: 'handler_test_last'
        },
        accountId = 'xxx';

    describe('Unknown Account', function () {
        it('should FAIL to get', function (done) {
            request(url)
                .get('/' + accountId)
                .send()
                .expect(404) //Status code
                .expect('Content-Type', 'application/json')
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
        it('should FAIL to update', function (done) {
            var body = {
                username: 'jack',
                password: 'test',
                email: 'jack@test.nl'
            };
            request(url)
                .put('/' + accountId)
                .send(body)
                .expect('Content-Type', 'application/json')
                .expect(404) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
        it('should FAIL to delete', function (done) {
            request(url)
                .delete('/' + accountId)
                .expect(404) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
    });
    describe('Account', function () {
        it('should create', function (done) {

            request(url)
                .post('/')
                .send(newAccount)
                .expect(201) //Status code created
                .expect('Content-Type', 'application/json')
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body.email.should.equal(newAccount.email);
                    accountId = res.body._id;
                    done();
                });
        });
        it('should get', function (done) {
            request(url)
                .get('/' + accountId)
                .expect(200) //Status code
                .expect('Content-Type', 'application/json')
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body.email.should.equal(newAccount.email);
                    done();
                });
        });
        it('should update', function (done) {
            var body = {
                username: 'jack',
                password: 'test',
                email: 'updated_email@jackvandereijk.nl'
            };
            request(url)
                .put('/' + accountId)
                .send(body)
                .expect('Content-Type', 'application/json')
                .expect(200) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
        it('should delete', function (done) {
            request(url)
                .delete('/' + accountId)
                .expect(204) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
    });
    describe('Deleted Account', function () {
        it('should FAIL to get', function (done) {
            request(url)
                .get('/' + accountId)
                .send()
                .expect(404) //Status code
                .expect('Content-Type', 'application/json')
                .end(function (err, res) {
                    console.log('end');
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
    });

});