'use strict';

var Account = require('../../app/models/account');
var AccountRepository = require('../../app/repositories/accountRepository');

var should = require('should');
var assert = require('assert');


//var mongoose = require('mongoose');
//var config = require('../../app/config-debug');

describe('AccountRepository', function () {

    var newAccount = new Account({
            username: 'repository_test_user',
            password: 'repository_test_pwd',
            email: 'repository_test@jackvandereijk.nl',
            firstName: 'repository_test_first',
            lastName: 'repository_test_last'
            //,facebookUserId: null
        }),
        accountId = 'xxx';


    describe('Unknown Account', function () {
        it('should FAIL to get', function (done) {
            AccountRepository.getAccountById(accountId).then(
                function (account) {
                    account.should.be(null);
                }, function err(err) {
                    console.log(err);
                    done();
                }
            );
        });

        it('should FAIL to update', function (done) {
            var tmpAccount = new Account(JSON.parse(JSON.stringify(newAccount)));
            tmpAccount.email = 'updated_repository_test@jackvandereijk.nl';
            AccountRepository.updateAccount(accountId, tmpAccount).then(
                function (account) {
                    throw 'failed, should not return a valid account';
                    //account.email.should.equal( 'updated_repository_test@jackvandereijk.nl' );
                }, function (err) {
                    console.log(err);
                    done();
                }
            );
        });

        it('should FAIL to delete unknown account', function (done) {
            AccountRepository.deleteAccount(accountId).then(
                function (account) {
                    throw 'failed, should not return a valid account';
                }, function (err) {
                    console.log(err);
                    done();
                }
            );
        });
    });

    describe('Account', function () {
        it('should create', function (done) {
            AccountRepository.createAccount(newAccount).then(
                function (account) {
                    account.email.should.equal(newAccount.email);
                    accountId = account._id;
                    done();
                }, function (err) {
                    console.log(err);
                    throw err;
                }
            );
        });

        it('should get', function (done) {
            AccountRepository.getAccountById(accountId).then(
                function (account) {
                    account.email.should.equal(newAccount.email);
                    done();
                }, function err(err) {
                    console.log(err);
                    throw err;
                }
            );
        });

        it('should update', function (done) {
            var tmpAccount = new Account(JSON.parse(JSON.stringify(newAccount)));
            tmpAccount.email = 'updated_repository_test@jackvandereijk.nl';
            AccountRepository.updateAccount(accountId, tmpAccount).then(
                function (account) {
                    account.email.should.equal(tmpAccount.email);
                    done();
                }, function err(err) {
                    console.log(err);
                    throw err;
                    //done();
                }
            );
        });
        it('should delete', function (done) {
            AccountRepository.deleteAccount(accountId).then(
                function (account) {
                    done();
                }, function (err) {
                    console.log(err);
                    done();
                }
            );
        });
    });
    describe('Deleted Account', function () {
        it('should FAIL to get', function (done) {
            AccountRepository.getAccountById(accountId).then(
                function (account) {
                    should.not.exist(account);
                    done();
                }, function err(err) {
                    console.log(err);
                    done();
                }
            );
        });

    });

});