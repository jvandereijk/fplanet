'use strict';

var assert = require('assert');
var mongoose = require('mongoose');
var config = require('../app/config-debug');

before(function (done) {
    // In our repository tests we use the test db
    console.log('connect db ');
    mongoose.connection.on('connected', function () {
        console.log('connected');
    });
    mongoose.connection.on('disconnected', function () {
        console.log('disconnected');
    });
    mongoose.connect(config.db.mongodb);
    console.log('db state:' + mongoose.connection.readyState);
    done();
});