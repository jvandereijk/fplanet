var should = require('should');
var assert = require('assert');
var request = require('supertest');

describe('Routing', function () {
    var url = 'http://localhost:9076';
    // within before() you can run all the operations that are needed to setup your tests. In this case
    // I want to create a connection with the database, and when I'm done, I call done().

    describe('/api', function () {
        it('should return error trying to get an unknown url /xxx', function (done) {

            request(url)
                .get('/xxx')
                .expect('Content-Type', 'text/html')
                .expect(404) //Status code

                // end handles the response
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });
        it('should correctly retrieve message from /api', function (done) {

            request(url)
                .get('/api')
                .expect('Content-Type', 'application/json')
                .expect(200) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body.message.should.equal('Home of the account api');
                    done();
                });
        });
    });

});