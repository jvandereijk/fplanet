/*globals define, console */
define([
        'lib/create',
        'lib/hammer',
        'core/assets',
        'core/game',
        'core/events',
        'controllers/scene',
        'ui/loader',
        'ui/ui',
        'lib/extensions' //global hackery
    ],
    function (createJs, hammer, assets, game, events, scene, loader, ui) {
        'use strict';

        game.state = assets.enums.gameStates.Startup;

        var canvas,
            gameLoaded,
            loaderContainer,
            hasFocus = true,
            filestoLoad = 0,
            filesLoaded = 0,
            createJsStage = null;

        function run() {
            createJs.MotionGuidePlugin.install(createJs.Tween);
            //set up canvas & loader
            canvas = document.createElement('canvas');
            document.body.appendChild(canvas);
            canvas.height = window.innerHeight;
            canvas.width = window.innerWidth;

            //min width
            var ratio = canvas.width / canvas.height;
            if (ratio < 9 / 16) { //add horizontal bars if needed for portrait
                ratio = 9 / 16;
                canvas.height = canvas.width * 16 / 9;
                canvas.style.top = ((window.innerHeight - canvas.height) / 2) + 'px';
            }
            if (ratio > 16 / 9) { //add vertical bars if needed for landscape
                ratio = 16 / 9;
                canvas.width = canvas.height * 16 / 9;
                canvas.style.left = ((window.innerWidth - canvas.width) / 2) + 'px';
            }
            assets.gameWidth = assets.gameHeight * ratio;
            asjustPixelRatio(canvas);

            createJsStage = new createJs.Stage(canvas);
            createJs.Touch.enable(createJsStage);

            createJsStage.scaleX = createJsStage.scaleY = canvas.height / assets.gameHeight;

            createJsStage.mouseMoveOutside = true;
            createJsStage.addChild(loaderContainer = loader.create(assets.gameWidth, assets.gameHeight));

            createJs.Ticker.maxDelta = 75;
            createJs.Ticker.timingMode = 'requestAnimationFrame';
            createJs.Ticker.setFPS(60);
            createJs.Ticker.addEventListener('tick', tick);

            if (document && document.createElement) {

                var fps = document.createElement('div');
                fps.setAttribute('id', 'fps');
                document.body.appendChild(fps);

                var debug = document.createElement('div');
                debug.setAttribute('id', 'debug');
                var debugClose = document.createElement('div');
                debugClose.className = 'hide';
                debugClose.setAttribute('id', 'debugclose');
                debugClose.innerText = 'X';
                document.body.appendChild(debug);
                document.body.appendChild(debugClose);
            }
            if (document && document.getElementById('debug')) {
                //catch console.log, warn and error to debug
                var log = console.log;
                console.log = function () {
                    document.getElementById('debug').innerHTML = '<span class="' + arguments[1] + '">' + arguments[0] + '</span>' +
                        '<br/>' + document.getElementById('debug').innerHTML;
                    log.apply(this, Array.prototype.slice.call(arguments));
                };
                console.warn = function () {
                    document.getElementById('debug').innerHTML = '<span class="warn">' + arguments[0] + '</span>' +
                        '<br/>' + document.getElementById('debug').innerHTML;
                    log.apply(this, Array.prototype.slice.call(arguments));
                };
                console.error = function () {
                    document.getElementById('debug').innerHTML = '<span class="error">' + arguments[0] + '</span>' +
                        '<br/>' + document.getElementById('debug').innerHTML;
                    log.apply(this, Array.prototype.slice.call(arguments));
                };
            }

            events.on('gameLoaded', onGameLoaded);
            events.on('gamePartLoaded', onGamePartLoaded);
            events.on('levelLoading', onLevelLoading);
            events.on('levelStarted', onLevelStarted);

            //load game
            filestoLoad = game.load();

            /*createjs.Sound.registerSound({
             id: "click",
             src: "audio/click.mp3"
             });

             createjs.Sound.addEventListener("fileload", handleSoundFileLoad);*/
        }

        /*function handleSoundFileLoad(event) {
         if (event && event.id === 'loop1') {
         createJs.Sound.play('click', {
         loop: 0, //-1,
         volume: 0.5
         });
         }
         }*/

        /*function destroy() {
         events.off(onGameLoaded);
         events.off(onGamePartLoaded);
         events.off(onLevelLoading);
         events.off(onLevelStarted);
         scene.destroy();
         ui.destroy();

         //todo cleanup hammer listeners, althoug game should only be loaded once
         }*/


        function onGamePartLoaded(/*id, args*/) {
            filesLoaded++;
            loader.setProgress(Math.round(filesLoaded / filestoLoad * 100));
        }

        function onGameLoaded() {
            //return;
            createJsStage.removeChild(loaderContainer);
            if (document && document.getElementById('debug')) {
                //debug window
                document.getElementById('debug').addEventListener('click', function (event) {
                    event.preventDefault();
                    if (document.getElementById('debug').className !== 'expand') {
                        document.getElementById('debug').className = 'expand';
                        document.getElementById('debugclose').className = '';
                    }
                });

                document.getElementById('debugclose').addEventListener('click', function (event) {
                    event.preventDefault();
                    document.getElementById('debug').className = '';
                    document.getElementById('debugclose').className = 'hide';
                });
            }

            //catch touch events, and throw as game events
            var pointer = hammer(canvas, {
                drag_max_touches: 0
            });

            pointer.on('tap', function (evt) {
                events.trigger('pointerTap', evt);
            });

            pointer.on('doubletap', function (evt) {
                events.trigger('pointerDoubletap', evt);
            });

            pointer.on('dragstart', function (evt) {
                events.trigger('pointerDragstart', evt);
            });

            pointer.on('drag', function (evt) {
                events.trigger('pointerDrag', evt);
            });

            pointer.on('release ', function (evt) {
                events.trigger('pointerRelease', evt);
            });

            pointer.on('pinchin', function (evt) {
                events.trigger('pointerPinchin', evt);
            });

            pointer.on('pinchout', function (evt) {
                events.trigger('pointerPinchout', evt);
            });


            createJsStage.addChild(scene.create(canvas));
            createJsStage.addChild(ui.create());

            game.state = assets.enums.gameStates.Menu;
            gameLoaded = true;
        }

        var loadStartTime;

        function onLevelLoading() {
            loader.setText('loading');
            createJsStage.addChild(loaderContainer);
            loadStartTime = new Date().getTime();
            //console.log('load level ');
        }

        function onLevelStarted() {
            createJsStage.removeChild(loaderContainer);
            //console.log('load level complete in ms: ' + (new Date().getTime() - loadStartTime));
        }

        document.onkeydown = function (evt) {
            evt = evt || window.event;
            events.trigger('keyDown', evt);
        };

        document.onkeyup = function (evt) {
            evt = evt || window.event;
            events.trigger('keyUp', evt);
        };

        window.onerror = function (msg, url, linenumber) {
            console.error('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber, 'error');
            return false;
        };

        window.onfocus = function () {
            if (!hasFocus) {
                //createJs.Ticker.setPaused(false);
                hasFocus = true;
            }
        };

        window.onblur = function () {
            if (hasFocus && game.state === assets.enums.gameStates.Playing) {
                game.state = assets.enums.gameStates.Paused;
                //createJs.Ticker.setPaused(true);
                hasFocus = false;
            }
            events.trigger('inputReset');
        };

        function asjustPixelRatio(canvas) {
            // Find upscale ratio: //
            var ratio = window.devicePixelRatio || 1;
            if (canvas.getContext('2d').webkitBackingStorePixelRatio < 2) {
                // default to 1 if property not set //
                ratio = window.devicePixelRatio || 1;
            }

            console.log('ratio:' + ratio);

            var w = canvas.width;
            var h = canvas.height;
            canvas.width=w*ratio;
            canvas.height=h*ratio;
            console.log(canvas.width + ' x ' + canvas.height);
            //canvas.setAttribute('width', w / ratio);
            //canvas.setAttribute('height', h / ratio);
            canvas.style.width= w +'px';
            canvas.style.height= h +'px';
        }

        function tick(event) {
            if (gameLoaded) {
                scene.update(event);
                ui.update(event);
            }
            if (createJsStage) {
                createJsStage.update(event);
            }
            if (document && document.getElementById('fps')) {
                var fps = Math.round(createJs.Ticker.getMeasuredFPS());
                document.getElementById('fps').innerText = fps + ' fps';
            }
        }

        run();

    });