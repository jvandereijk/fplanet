/*globals define */
define(
    function () {
        'use strict';

        //vars are not unique for objects, maybe its better to store on object dynamically, although its not necessary
        //because all gameobjects will now just animate in sync
        var angleDif = 0;
        var angleDifDir = 1;
        var baseScale = null;
        var scale = 1;
        var scaleDir = 1;

        function none(object, speed) {
            //object.shape.rotation = object.r - 90;
        }

        function twiggle(object, speed) {
            //object.shape.rotation -= delta;
            object.shape.rotation = object.r + 90 + angleDif;
            angleDif += 10 * angleDifDir * speed * 0.15;
            if (angleDif >= 20) {
                angleDifDir = -1;
            }
            if (angleDif <= -20) {
                angleDifDir = 1;
            }
        }

        function spin(object, speed) {
            object.shape.rotation -= speed;
        }

        function vibrate(object, speed) {
            object.shape.rotation = object.r - 90;
            if (!baseScale) {
                baseScale = object.shape.scaleX;
            }
            object.shape.scaleX = scale * baseScale;
            object.shape.scaleY = scale * baseScale;
            scale += 0.005 * scaleDir * speed;
            if (scale >= 1.15) {
                scaleDir = -1;
            }
            if (scale <= 0.85) {
                scaleDir = 1;
            }
        }

        function headFirst(object, speed) {
            object.shape.rotation = object.r - 90;
        }

        return {
            twiggle: twiggle,
            spin: spin,
            vibrate: vibrate,
            none: none,
            headFirst: headFirst
        };
    });