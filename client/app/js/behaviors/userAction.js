/*globals define */
define(['actors/critter',
        'core/assets'],
    function (critter, assets) {
        'use strict';

        function tap(object, container) {
            if (object.changeable) {
                object.changeable.current++;
                if (object.changeable.current >= object.changeable.subtypes.length) {
                    object.changeable.current = 0;
                }
                object.shape = critter.updateType(object.shape, object.changeable.subtypes[object.changeable.current]);
                object.type = object.changeable.subtypes[object.changeable.current];
                object.value = assets.defaults.critter[object.type].value;
            }
        }

        return {
            tap: tap
        };

    });