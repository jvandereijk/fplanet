/*globals define */
define(['lib/create',
        'models/graph/circularGraph'],
    function (createJs, circularGraph) {
        'use strict';

        function rolling(node) {
            var graph = circularGraph.get();
            if (node.content !== null && !node.locked) {
                for (var i = 0; i < node.parents.length; i++) {
                    if (graph[node.parents[i]].content === null && !node.parents[i].locked) {
                        if (circularGraph.addNodeAction('move', node, graph[node.parents[i]])) {
                            break; //target found, break from loop
                        }
                    }
                }
            }
        }

        function rollingStickSameType(node) {
            var graph = circularGraph.get();
            if (node.content !== null && !node.locked && node.layer > 1) {
                //check for block parents, that can support this block
                if (node.parents.length === 1) {
                    //got only 1 parent, try move there
                    if (graph[node.parents[0]].content === null) {
                        //1 parent and free, move up
                        circularGraph.addNodeAction('move', node, graph[node.parents[0]]);
                    }
                } else if (node.parents.length === 2 || node.parents.length === 4) {
                    //got 2 parents, both can support a block, pick order randomly
                    var p = Math.random() >= 0.5 ? 1 : 0;
                    var alt = p > 0 ? 0 : 1;
                    p += node.parents.length === 4 ? 1 : 0;
                    alt += node.parents.length === 4 ? 1 : 0;
                    var supportFound =
                        (graph[node.parents[p]].content !== null && graph[node.parents[p]].content.behavior === node.content.behavior) ||
                        (graph[node.parents[alt]].content !== null && graph[node.parents[alt]].content.behavior === node.content.behavior);
                    if (!supportFound) { //no parents with support, try drop one layer
                        if (graph[node.parents[p]].content === null) {
                            //2 parents and ' + p + ' is free, move up
                            circularGraph.addNodeAction('move', node, graph[node.parents[p]]);
                        } else if (graph[node.parents[alt]].content === null) {
                            //first rnd parent failed (already taken), try other one
                            //2 parents and ' + alt + ' is free, move up
                            circularGraph.addNodeAction('move', node, graph[node.parents[alt]]);
                        }
                    }
                } else if (node.parents.length === 3) {
                    //try move to middle parent
                    //
                    if (graph[node.parents[1]].content === null) {
                        //console.log('3 parents and middle is free, move up');
                        circularGraph.addNodeAction('move', node, graph[node.parents[1]]);
                    } else {
                        //got 3 parents, only middle one can support this block (if its a block too)
                        if (graph[node.parents[1]].content.behavior !== node.content.behavior) {
                            //try move to a rnd parent (will fail if spot is already taken)
                            var r = Math.random() >= 0.5 ? 2 : 0;
                            if (graph[node.parents[r]].content === null) {
                                //3 parents and ' + r + ' is free, move up
                                circularGraph.addNodeAction('move', node, graph[node.parents[r]]);
                            } else if (graph[node.parents[r > 0 ? 0 : 2]].content === null) {
                                //3 parents and ' + (r > 0 ? 0 : 2) + ' is free, move up
                                circularGraph.addNodeAction('move', node, graph[node.parents[r > 0 ? 0 : 2]]);
                            }
                        }
                    }
                }
            }
        }


        //randomly let a critter 'jump'
        function randomAnimate(node, tileSize, planetSize) {
            if (node.content && node.content !== 0 && node.content !== null) {
            }
            if (Math.random() < 0.001) {
                var ringSize = node.ringSize;
                var distance = tileSize * (planetSize + node.layer - 1);

                var x = distance * 1.05 * Math.cos(node.radians);
                var y = distance * 1.05 * Math.sin(node.radians);
                var ox = distance * Math.cos(node.radians);
                var oy = distance * Math.sin(node.radians);

                createJs.Tween.get(node.content.shape, {
                    override: false
                }).to({
                    x: x,
                    y: y
                }, 250, createJs.Ease.backIn).to({
                    x: ox,
                    y: oy
                }, 250, createJs.Ease.backOut);
            }
        }

        return {
            rolling: rolling,
            rollingStickSameType: rollingStickSameType,
            randomAnimate: randomAnimate
        };
    });