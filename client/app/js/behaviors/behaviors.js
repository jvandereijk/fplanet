/*globals define */
define(['core/assets',
        'behaviors/fallingStyle',
        'behaviors/fallingCollisions',
        'behaviors/graphEnter',
        'behaviors/graphUpdate',
        'behaviors/userAction'
    ],
    function (assets, fallingStyle, fallingCollisions, graphEnter, graphUpdate, userAction) {
        'use strict';

        var Types = assets.enums.gameObjectBehaviors;
        var Boll = {
            Type: Types.Boll,
            updateFalling: fallingStyle.spin,
            findDropSpot: fallingCollisions.rolling,
            onHit: graphEnter.encapsulate,
            onTap: userAction.tap,
            nodeUpdate: function (node, tileSize, planetSize) {
                graphUpdate.rolling(node, tileSize, planetSize);
                graphUpdate.randomAnimate(node, tileSize, planetSize);
            }
        };
        var Block = {
            Type: Types.Block,
            updateFalling: fallingStyle.twiggle,
            findDropSpot: fallingCollisions.rollingstickSameType,
            onHit: function (node, isOrigin) {
                graphEnter.stack(node, isOrigin);
                graphEnter.encapsulate(node, isOrigin);
            },
            onTap: userAction.tap,
            nodeUpdate: graphUpdate.rollingStickSameType
        };
        var Wedge = {
            Type: Types.Wedge,
            updateFalling: fallingStyle.headFirst,
            findDropSpot: fallingCollisions.pushAside,
            onHit: function (node, isOrigin) {
                graphEnter.triangle(node, isOrigin);
                graphEnter.encapsulate(node, isOrigin);
            },
            onTap: userAction.tap,
            nodeUpdate: function (node, tileSize, planetSize) {
                graphUpdate.rolling(node, tileSize, planetSize);
            }
        };
        var Bomb = {
            Type: Types.Bomb,
            updateFalling: fallingStyle.vibrate,
            findDropSpot: fallingCollisions.rolling,
            onHit: graphEnter.blowUp,
            onTap: userAction.tap,
            nodeUpdate: graphUpdate.rolling
        };

        function get(behavior) {
            switch (behavior) {
                case Types.Boll:
                    return Boll;
                case Types.Block:
                    return Block;
                case Types.Wedge:
                    return Wedge;
                case Types.Cluster:
                    //Boll.onHit(node);
                    break;
                case Types.Bomb:
                    return Bomb;
            }
        }

        return {
            get: get,
            Types: Types
        };
    });