/*globals define */
define(['models/graph/circularGraph'],
    function (circularGraph) {
        'use strict';

        function rollingInternal(object, layer, delta, backupDistance, stickToParent, onSucces, onFail) {
            var graph = circularGraph.get();
            var ringSize = circularGraph.cellCount(layer);
            var rotation = require('controllers/stage').rotation(); //dont like this dependency, but its needed (for now)
            var angle = ((object.r - rotation) / 360).maxToZero(1.0); //approach angle
            var cell = (angle * ringSize + 0.5 | 0).mod(ringSize);

            //already in illegal territory, move or backup
            if (!graph[layer + '_' + cell].isEmpty()) {
                if (layer === 0) {
                    object.distance += backupDistance * 2;
                    return false;
                }
                var leftPath = circularGraph.getParentPath(layer + '_' + (cell - 1).mod(ringSize));
                var rightPath = circularGraph.getParentPath(layer + '_' + (cell + 1).mod(ringSize));
                if (leftPath.length === 0 && rightPath.length === 0) { //both left and right spots are full
                    if (layer === circularGraph.layerCount()) {
                        return onFail(layer, cell);
                    }
                    var nearest = circularGraph.getNearestFreeNode(layer, cell);
                    if (nearest === 0) {
                        object.distance += backupDistance; //all sibling full, backup one layer
                    } else {
                        object.r += nearest < 0 ? -5 : 5; //adjust angle of object to nearest empty
                    }
                    return true; //destroy obj
                }
                if (leftPath.length === 0) { // go right if left is taken
                    object.r = graph[rightPath[0]].radians / (2 * Math.PI) * 360 + rotation;
                    object.distance += delta;
                    return false;
                }
                //go left in all other cases
                object.r = graph[leftPath[0]].radians / (2 * Math.PI) * 360 + rotation;
                object.distance += delta;
                return false;
            }
            //check next layer
            var nLayer = layer - 1;
            var nRingSize = circularGraph.cellCount(nLayer);
            var nCell = (angle * nRingSize + 0.5 | 0).mod(nRingSize);
            //next spot taken ?
            if (graph[nLayer + '_' + nCell].content !== null) {
                if (stickToParent && graph[layer + '_' + cell].isEmpty()) {
                    return onSucces(object, layer, cell);
                }
                //try left and/or right
                var anyDirection = nCell === angle * nRingSize || angle === 1; //( angle  === 1 also means cell === 0)
                var l = circularGraph.getParentPath(nLayer + '_' + (nCell - 1).mod(nRingSize));
                var r = circularGraph.getParentPath(nLayer + '_' + (nCell + 1).mod(nRingSize));
                var deltaR = Math.max(delta / backupDistance * 100, 5);
                if (l.length > 0 && ((anyDirection || l.length <= r.length) || r.length === 0)) { //go left
                    object.r = object.r - deltaR;
                    object.distance += delta / 2;
                    return false;
                }
                if (r.length > 0 && ((anyDirection || l.length > r.length) || l.length === 0)) { //go right
                    object.r = object.r + deltaR;
                    object.distance += delta / 2;
                    return false;
                }
                //no options found, try fill current
                if (!stickToParent && graph[layer + '_' + cell].isEmpty()) {
                    return onSucces(object, layer, cell);
                }
            }
            return false;
        }

        function rolling(object, layer, delta, backupDistance, onSucces, onFail) {
            return rollingInternal(object, layer, delta, backupDistance, false, onSucces, onFail);
        }

        function rollingstickSameType(object, layer, delta, backupDistance, onSucces, onFail) {
            return rollingInternal(object, layer, delta, backupDistance, true, onSucces, onFail);
        }

        function pushAside(object, layer, delta, backupDistance, onSucces, onFail) {
            var graph = circularGraph.get();
            var ringSize = circularGraph.cellCount(layer);
            var angle = ((object.r - require('controllers/stage').rotation()) / 360).maxToZero(1.0); //approach angle
            var cell = (angle * ringSize + 0.5 | 0).mod(ringSize);
            //already in illegal territory,  backup
            if (!graph[layer + '_' + cell].isEmpty()) {
                if (layer === 0) {
                    object.distance += backupDistance * 2;
                    return false;
                }
                if (layer === circularGraph.layerCount()) {
                    return onFail(layer, cell);
                }
                object.distance += backupDistance; //spot taken, backup one layer
                return true;
            }
            //check next layer
            var nLayer = layer - 1;
            var nRingSize = circularGraph.cellCount(nLayer);
            var nCell = (angle * nRingSize + 0.5 | 0).mod(nRingSize);
            //next spot taken ?
            if (graph[nLayer + '_' + nCell].content !== null) {
                // try fill current
                var node = graph[layer + '_' + cell];
                if (node.isEmpty()) {
                    var succes = onSucces(object, layer, cell); //fill cell
                    if (succes && node.layer > 1) {
                        if (graph[nLayer + '_' + nCell].content.type !== node.content.type) { //dont push same types
                            var p = circularGraph.getFirstEmptyChild(nLayer + '_' + nCell, 'both');
                            if (p) {
                                circularGraph.addNodeAction('move', graph[nLayer + '_' + nCell], graph[p]); //push parent away
                            }
                        }
                    }
                    return succes;
                }
            }
            return false;
        }

        return {
            rolling: rolling,
            rollingstickSameType: rollingstickSameType,
            pushAside: pushAside
        };
    });