/*globals define */
define([
        'core/assets',
        'core/events',
        'models/graph/circularGraph',
        'helpers/shapeDetection'
    ],
    function (assets, events, circularGraph, shapeDetection) {
        'use strict';

        function testEncapsulate(node) {

            var layerFull = circularGraph.layerIsFull(node.layer),
                types = circularGraph.layerNodeTypes(node.layer);

            //check if outer layer is full and cant be encapsulated
            if (layerFull && node.layer === circularGraph.layerCount()) {
                if (types.length > 2) {
                    events.trigger('gameOver', 'outer-layer-full');
                }
            }

            if (layerFull && types.length == 2) { //if ring has only 2 unique types and no empty nodes,  convert left and right nodes

                var direction, i;

                //lock layer, to prevent other changes while clearing ring
                var layerLock = circularGraph.setLayerLock(node.layer, 'change', {
                    type: node.content.type,
                    value: node.content.value,
                    triggeringNode: node
                });

                if (layerLock) { //set type to layer lock type
                    for (direction = -1; direction <= 1; direction += 2) { //check left and right direction
                        for (i = 1; i <= node.ringSize; i++) {
                            var neighbour = circularGraph.getNode(node.layer + '_' + (node.cell + i * direction).mod(node.ringSize));

                            if (!neighbour.locked && neighbour.content && layerLock.type !== neighbour.content.type) {
                                circularGraph.addNodeAction('change', neighbour, {
                                    type: layerLock.type,
                                    value: layerLock.value
                                });
                                break; //break after first change, this change will trigger a new encapsulate test eventually for changing more neighbours
                            }
                        }
                    }
                }

            } else {

                var nodeRight = circularGraph.getNode(node.layer + '_' + (node.cell + 1).mod(node.ringSize)),
                    nodeLeft = circularGraph.getNode(node.layer + '_' + (node.cell - 1).mod(node.ringSize)),
                    doneRight, doneLeft;

                if (nodeRight.content && nodeRight.content.type !== node.content.type) {
                    //if different type is found on right, check if its encapsulated
                    doneRight = shapeDetection.checkEncapsulatedBy(nodeRight);
                }
                if (nodeLeft.content && nodeLeft.content.type !== node.content.type) {
                    //if different type is found on left, check if its encapsulated
                    doneLeft = shapeDetection.checkEncapsulatedBy(nodeLeft);
                }

                if (!(doneRight || doneLeft) && node.content && assets.defaults.critter[node.content.type].allowEncapsulateSingle) {
                    shapeDetection.checkEncapsulatedBy(node); //check if current node is directly encapsulated
                }
            }
        }

        function testStack(node /*, isOrigin*/) {
            if (node.content && node.content !== null) { // && node.layer > 3) {
                var i, item, data, count = 0,
                    stack = shapeDetection.findStack(node);

                if (stack.length > 3) {
                    for (i = 0; i < stack.length; i++) {
                        item = circularGraph.getNode(stack[i]).content;
                        count = (item.behavior.Type === node.content.behavior.Type && // block behavior type
                            node.content.type === item.type) ? count + 1 : 0; //check stack is same type
                        if (count > 3) break;
                    }
                    if (count > 3) { //remove full stack if 4 adjecent squares are found
                        events.triggerShapeDetected('stack', node.data(), getLiteNodes(stack));

                        for (i = 0; i < stack.length; i++) {
                            if (i === stack.length - 1) {
                                data = new events.ShapeEvent('removed', 'stack', node.data(), stack);
                            }
                            circularGraph.addNodeAction('empty', circularGraph.getNode(stack[i]), data);
                        }

                    }
                }
            }
        }

        function blowUp(node, isOrigin) {
            circularGraph.addNodeAction('destroy', node, {
                blowUpNeighbours: isOrigin,
                delay: 100
            });
        }

        function getLiteNodes(nodeIds) {
            var i, liteNodes = [];
            for (i = 0; i < nodeIds.length; i++) {
                liteNodes.push(circularGraph.getNode(nodeIds[i]).data());
            }
            return liteNodes;
        }

        function testTriangle(node /*, isOrigin*/) {
            var i, children;
            children = shapeDetection.allChildrenWithSameType(node);
            if (children.length > 1) {
                events.triggerShapeDetected('triangle', node.data(), getLiteNodes(children));

                for (i = 0; i < children.length; i++) {
                    circularGraph.addNodeAction('empty', circularGraph.getNode(children[i]), null);
                }
                circularGraph.addNodeAction('empty', node, new events.ShapeEvent('removed', 'triangle', node.data(), children));
            }

            //go up in the tree recursively
            for (i = 0; i < node.parents.length; i++) {
                var pNode = circularGraph.getNode(node.parents[i]);
                if (pNode.content && pNode.content.type === node.content.type) {
                    testTriangle(pNode);
                }
            }
        }

        return {
            encapsulate: testEncapsulate,
            blowUp: blowUp,
            stack: testStack,
            triangle: testTriangle
        };
    });