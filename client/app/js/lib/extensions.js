'use strict';

/* todo: change namenot really modulo, (-1).mod(10)  will result in 9, to wrap arround circular arrays*/
Number.prototype.mod = function (n) {
	return ((this % n) + n) % n;
};


Number.prototype.maxToZero = function (max) {
	var tmp = this;
	while (tmp <= 0) {
		tmp += max;
	}
	while (tmp >= max) {
		tmp -= max;
	}
	return tmp;
};

String.prototype.capitaliseFirstLetter = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
};


Math.getDegrees = function (x, y) {
	//normalize
	var sum = Math.sqrt(Math.abs(x * x + y * y));
	x = x / sum;
	y = y / sum;
	//get angle in radians
	if (x === 0) {
		if (y < 0) {
			return Math.PI / 2;
		}
		if (y > 0) {
			return -Math.PI / 2;
		}
		return 0;
	}
	if (y === 0) {
		if (x < 0) {
			return Math.PI;
		}
		if (x > 0) {
			return 0;
		}
	}
	if (x < 0 && x === y) {
		return Math.PI * 0.75;
	}
	if (x < 0 && x === -y) {
		return -Math.PI * 0.75;
	}
	if (x > 0 && x === y) {
		return -Math.PI * 0.25;
	}
	if (x > 0 && x === -y) {
		return Math.PI * 0.25;
	}
	return Math.atan2(-y, x);
};