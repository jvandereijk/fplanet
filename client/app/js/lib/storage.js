/*globals define */
define(function () {
	'use strict';

	function set(key, value) {
		localStorage.setItem(key, JSON.stringify(value));
	}

	function get(key) {
		var value = localStorage.getItem(key);
		return value && JSON.parse(value);
	}

	return {
		set: set,
		get: get
	};
});