/*globals define */
define(['lib/create',
        'core/assets',
        'core/events',
        'helpers/geometric',
        'helpers/anim'
    ],
    function (createJs, assets, events, geometricHelper, anim) {
        'use strict';

        var container,
            shieldsContainer,
            ringShape,
            strength,
            distance,
            layerCount,
            shields,
            segmentSize,
            shieldCount,
            shieldsActive,
            stageConfig,
            padding;


        function create(config) {
            stageConfig = config;
            container = new createJs.Container();
            if (stageConfig.shields) {
                shieldsContainer = new createJs.Container();
                layerCount = stageConfig.layerCount;
                strength = 0;
                distance = ((stageConfig.planet.r * assets.tileSize) + assets.tileSize * layerCount);
                shields = [];

                shieldCount = layerCount * 6;
                segmentSize = 360 / shieldCount;
                padding = shieldCount * 0.0025;

                for (var i = 0; i < shieldCount; i++) {
                    shields.push({
                        shape: null,
                        strength: 0,
                        radians: i * segmentSize * Math.PI / 180
                    });
                }

                ringShape = new createJs.Shape();
                container.addChild(shieldsContainer);
                createShields();
                container.addChild(ringShape);
            }
            return container;
        }

        function createShields() {
            ringShape.graphics.clear();
            ringShape.graphics.setStrokeStyle(assets.tileSize * 0.05, 1, 1, 3, true)
                .beginStroke(createJs.Graphics.getRGB(255, 255, 255, 0.1))
                .drawCircle(0, 0, distance);
            ringShape.alpha = 0;
            updateShields();
        }

        function updateShields() {
            for (var i = 0; i < shields.length; i++) {
                renderShield(i);
            }
        }

        function renderShield(index) {
            var shield = shields[index],
                startAngle, endAngle;
            if (shield.type) { //} && !shield.destroying) {
                shieldsContainer.removeChild(shield.shape);
                shield.shape = new createJs.Shape();
                startAngle = shield.radians;
                endAngle = (index < shields.length - 1 ? shields[index + 1].radians : shields[0].radians);
                shield.color = assets.defaults.critter[shield.type].color;

                var weight = 1 + distance * shield.strength * 0.075;
                var rect = geometricHelper.getRectangleFromAngles(startAngle, endAngle, weight, distance);


                shield.shape.graphics.setStrokeStyle(weight, 0, 1)
                    .beginStroke(createJs.Graphics.getRGB(shield.color.r, shield.color.g, shield.color.b, 1))
                    .arc(0, 0, distance, startAngle, endAngle, false);

                /*var text = new createJs.Text(+index, 'bold ' + (assets.tileSize / 2) + 'px ' + assets.font('Main'), '#000').set({
                 textAlign: 'center',
                 x: rect.x + rect.w / 2,
                 y: rect.y + rect.h / 2,
                 rotation: startAngle * 180 / Math.PI + 90
                 });*/

                shield.shape.cache(rect.x, rect.y, rect.w, rect.h);
                shieldsContainer.addChild(shield.shape);

                //show fancy anim on shield
                anim.zoom(shield.shape, 0.9, 200, createJs.Ease.easeIn, true).call(function () {
                    anim.zoom(shield.shape, 1.1, 200, createJs.Ease.easeIn, true).call(function () {
                        anim.zoom(shield.shape, 1, 200, createJs.Ease.easeIn, true).call(function () {
                            //shield.destroying = false; //make sure state is reset
                        });
                    });
                });

            }
        }

        function destroyShield(index) {
            var shield = shields[index],
                startAngle, endAngle;
            //if (!shield.destroying) {
            //shield.destroying = true;
            shieldsContainer.removeChild(shield.shape);
            shield.shape = new createJs.Shape();
            startAngle = shield.radians;
            endAngle = (index < shields.length - 1 ? shields[index + 1].radians : shields[0].radians);

            var weight = 1 + distance * shield.strength * 0.05;
            var rect = geometricHelper.getRectangleFromAngles(startAngle, endAngle, weight, distance);

            shield.shape.graphics.setStrokeStyle(weight, 0, 1)
                .beginStroke('#000')
                .arc(0, 0, distance, startAngle, endAngle, false);
            shield.shape.cache(rect.x, rect.y, rect.w, rect.h);
            shieldsContainer.addChild(shield.shape);

            shield.strength = 0;
            shield.type = null;
            anim.zoom(shield.shape, 1.5, 200, createJs.Ease.easeIn, true).call(function () {
                anim.zoom(shield.shape, 0.5, 100, createJs.Ease.easeIn, true).call(function () {
                    createJs.Tween.removeTweens(shield.shape);
                    shieldsContainer.removeChild(shield.shape);
                    shield.shape = null;
                    //shield.destroying = false;
                });
            });
            //}
        }


        //strength is the number of nodes and will be converted to shield 'power'
        function addStrength(type, radians, strength) {
            if (stageConfig.shields) {
                var nodeDegrees = radians * 180 / Math.PI,
                    segment = nodeDegrees / segmentSize + 0.5 | 0;

                distributePower(segment, type, strength / 6);
            }
        }

        /// flowcount to prevent overflowing to far
        function distributePower(segment, type, power, flowCount) {
            if (!flowCount) {
                flowCount = 0;
            }
            if (power <= 0) return;
            if (shields[segment].type !== type) {
                shields[segment].strength -= power; // optional: replace and reset shield ?
                if (shields[segment].strength < 0) shields[segment].strength = 0;
            }

            var overflow = shields[segment].strength + power - 1;
            if (overflow > 0) {
                power -= overflow; // power >1, overflow to sides
            } else {
                overflow = power * 0.4;
                if (overflow > 0.025) { //overflow fraction to sides
                    power -= overflow;
                } else {
                    overflow = 0; //overflow too small
                }
            }
            if (overflow > 0 && flowCount < layerCount) {
                var prev = (segment - 1).mod(shieldCount);
                var next = (segment + 1).mod(shieldCount);
                flowCount++;
                setTimeout(function () {
                    distributePower(prev, type, overflow / 2, flowCount);
                    distributePower(next, type, overflow / 2, flowCount);
                }, 150);
            }

            shields[segment].strength += power;
            //console.log('shield ' + segment + ' gets ' + power + ' power');
            if (shields[segment].strength > 1) shields[segment].strength = 1;
            shields[segment].type = type;
            renderShield(segment);
        }

        function checkMissileImpact(missile) {
            if (shieldsActive) {
                var direction = missile.shape.x < 0 ? -1 : 1;
                var dm = direction * missile.shape.x / 10 | 0; //distance missile
                var d = (distance + assets.tileSize * 2) * container.scaleX / 10 | 0; //distance shield
                if (dm === d) { // hit test with shield distance
                    var stageRotation = require('controllers/stage').rotation(),
                    //angle = Math.atan(missile.shape.x / missile.shape.y) * 180 / Math.PI, //missile angle
                        angleX = -Math.atan2(missile.shape.x, missile.shape.y) * 180 / Math.PI, // angle with x-axis
                        rotation = (angleX - stageRotation + 90 + 3).mod(360), // angle adjusted with stage rotation  ( +3 = tweak )
                        segment = (rotation / segmentSize | 0).maxToZero(shieldCount); //shield segment at missile location
                    //if (angle < 0) {
                    //console.log('rotation:' + rotation + ' segment:' + segment + 'm angle:' + angleX + ' m dir:' + direction);
                    //}

                    var missileDestroyed = false;
                    if (shields[segment].type) missileDestroyed = shieldImpact(missile, segment);
                    segment = (segment - 1).maxToZero(shieldCount);
                    if (!missileDestroyed && shields[segment].type) missileDestroyed = shieldImpact(missile, segment); //add left segment damage
                    segment = (segment + 2).maxToZero(shieldCount);
                    if (!missileDestroyed && shields[segment].type) missileDestroyed = shieldImpact(missile, segment); //add right segment damage
                    return missileDestroyed;
                }
            }
            return false;
        }

        function shieldImpact(missile, segment) {
            var damageMultiplier = 0.75; //tweakable ?
            events.trigger('cameraShake', 0.1); //small shake

            if (isSameColor(missile.color, shields[segment].color)) {
                damageMultiplier *= 0.2;
            }

            if (missile.damage * damageMultiplier > shields[segment].strength) { //destroy shield
                //console.log('missile ' + missile.id + ' destroying shield ' + segment + ' (' + shields[segment].strength + '),  missile damage remaining: ' + (missile.damage - shields[segment].strength) + ' / ' + missile.damage + ' ');
                missile.damage -= shields[segment].strength;
                destroyShield(segment);

                return false;
            } else { //derstroy missile
                //console.log('missile ' + missile.id + ' hitting shield ' + segment + ' for ' + missile.damage * damageMultiplier + ' damage ' + (shields[segment].strength - missile.damage) + ' / ' + shields[segment].strength + ' shield strength left');
                shields[segment].strength -= missile.damage * damageMultiplier;
                missile.damage = 0;
                renderShield(segment);
                return true;
            }
        }

        function isSameColor(c1, c2, compareAlpha) {
            var result = c1.r === c2.r && c1.g === c2.g && c1.b === c2.b;
            return compareAlpha ? result && c1.a === c2.a : result;
        }

        function setActive(active) {
            if (stageConfig.shields) {
                shieldsActive = active;
                ringShape.alpha = active ? 1 : 0;
            }
        }

        return {
            create: create,
            addStrength: addStrength,
            setActive: setActive,
            checkMissileImpact: checkMissileImpact
        };
    });