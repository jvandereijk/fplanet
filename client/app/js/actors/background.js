/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'helpers/anim'
    ],
    function (createJs, assets, game, anim) {
        'use strict';

        var container,
            bgBitmap,
            //bgSprite,
            bgTransform,
            layerTransforms,
            baseScale,
            //backgroundContainer,
            layerContainers,
            //spriteContainer,
            background;

        function create() {
            var width = 1024,
                height = 1024,
                sR,
                scale,
                f;

            background = game && game.level ? game.level.background : {
                id: 'background',
                rotationSpeed: 0,
                scaleToFit: true
            };

            if (!background.alpha) {
                background.alpha = 1;
            }

            container = new createJs.Container();
            //backgroundContainer = new createJs.Container();

            //container.addChild(backgroundContainer);

            if(!background.layers){ //backwards compatible fix
                background.layers=[{scale:1}];
            }


            layerContainers=[background.layers];
            layerTransforms=[background.layers];
            for(var i=0;i< background.layers.length; i++) {
                layerContainers[i] = new createJs.Container();

                var bmp = new createJs.Bitmap(assets.get('bg-' + background.id + '-' + i));
                bmp.x = -bmp.image.naturalWidth / 2;
                bmp.y = -bmp.image.naturalHeight / 2;

                layerContainers[i].addChild(bmp);
                container.addChild(layerContainers[i]);

                if(i===0){ //adjust layer 0 background scale to always fit
                    //when rotating, make sure the bg image scale to full diagonal size
                    sR = Math.sqrt(Math.pow(assets.gameWidth, 2) + Math.pow(assets.gameHeight, 2));
                    scale = background.scaleToFit ? bmp.image.naturalWidth > bmp.image.naturalHeight ? sR / bmp.image.naturalHeight : sR / bmp.image.naturalWidth : 1;

                    background.layers[i].scale= scale*1.25;

                    //baseScale = scale*1.2 * 2.5;
                }

                layerTransforms[i] = {
                    x: (game && game.level ? game.level.background.x || 0.5 : 0.5) * assets.gameWidth,
                    y: (game && game.level ? game.level.background.y || 0.5 : 0.5) * assets.gameHeight,
                    scaleX: background.layers[i].scale || 1, //1.2 for (pinch) zooming
                    scaleY: background.layers[i].scale || 1,
                    r:0
                };



                if (background.layers[i].filters) {
                    layerContainers[i].filters = [];
                    if (background.layers[i].filters.color) {
                        f = background.layers[i].filters.color;
                        layerContainers[i].filters.push(new createJs.ColorFilter(f[0], f[1], f[2], f[3], f[4], f[5], f[6], f[7]));
                    }
                    if (background.layers[i].filters.blur) {
                        f = background.layers[i].filters.blur;
                        layerContainers[i].filters.push(new createJs.BlurFilter(f[0], f[1], f[2]));
                    }
                }
                //layerContainers[i].alpha=0.2 + 0.2*i;

                layerContainers[i].cache(-width / 2, -height / 2, width, height);
            }

            container.alpha = background.alpha;
            return container;
        }

        function zoom(amount) {

            for(var i=0;i<layerTransforms.length;i++) {
                //var scale = amount < 0 ? baseScale * 0.85 : (amount > 0 ? baseScale * 1.2 : baseScale);
                var scale = amount < 0 ? background.layers[i].scale * (1.6-i*0.2) : background.layers[i].scale ;
                createJs.Tween.get(layerTransforms[i], {
                    override: true
                }).to({
                    scaleX: scale,
                    scaleY: scale
                }, 300, createJs.Ease.backOut);
                createJs.Tween.get(layerContainers[i], {
                    override: true
                }).to({
                    alpha: amount < 0 ? 0.93 * background.alpha : background.alpha
                }, 200, createJs.Ease.backOut);
            }
        }

        function fade(alpha, duration, ease) {
            anim.fade(container, alpha * background.alpha, duration, ease || createJs.Ease.cubicInOut);
        }

        function update(event) {
            //console.log('use r:'+ transform.r);
            //bgTransform.r += background.rotationSpeed * event.delta / 1000;

                for (var i = 0; i < layerTransforms.length; i++) {
                    layerTransforms[i].r+= background.rotationSpeed * event.delta / 900/(3-i);
                }
            //
            //if (spriteContainer) spriteContainer.setTransform(null, null, transform.scaleX * 0.1, transform.scaleY * 0.1, null);
            //backgroundContainer.setTransform(bgTransform.x, bgTransform.y, bgTransform.scaleX, bgTransform.scaleY, bgTransform.r);
            //if(layerContainers && layerContainers.length>1) {
                for (var i = 0; i < layerContainers.length; i++) {
                    layerContainers[i].setTransform(layerTransforms[i].x, layerTransforms[i].y, layerTransforms[i].scaleX, layerTransforms[i].scaleY, layerTransforms[i].r);
                }
            //}
        }

        function rotation(r) {
            if (typeof r === 'number' && isFinite(r)) {
                //transform.r = -0.5*r;
                //console.log('set r:'+ transform.r);
                if(layerTransforms && layerTransforms.length>1) {
                    for (var i = 1; i < layerTransforms.length; i++) {
                        var reverse = layerTransforms.length-i;
                        //layerTransforms[i].r=r/(reverse+1)/20*i;
                        //console.log(r+ ' ' + Math.round(r/10/(reverse+1)));
                    }
                }
            }
        }

        function rotate(amount) {
            if (typeof amount === 'number' && isFinite(amount)) {
                if(layerTransforms && layerTransforms.length>1) {
                    for (var i = 1; i < layerTransforms.length; i++) {
                        var reverse = layerTransforms.length-i;
                        //layerTransforms[i].r+=amount/(reverse+1)/2*i;
                    }
                }
            }
        }

        return {
            create: create,
            zoom: zoom,
            fade: fade,
            update: update,
            rotation: rotation,
            rotate:rotate
        };
    }
);