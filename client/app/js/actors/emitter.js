/*globals define */
define(['behaviors/behaviors'],
    function (behaviors) {
        'use strict';

        var startAngle, //(Math.random()*36 -0.5|0),
            angle,
            spread,
            direction,
            seed,
            sequence,
            iterator,
            types;

        function create(o) {
            //check if data has significantly changed
            if (seed !== o.seed || sequence !== o.sequence || types !== o.types) {
                seed = o.seed;
                sequence = o.sequence;
                types = o.types;
                startAngle = o.startAngle || 270; //(Math.random()*36 -0.5|0),
                angle = o.angle || 270;
                spread = 30;
                direction = o.direction || 1;

                iterator = 0;

                if (o.spread !== undefined) spread = o.spread;

                if (sequence.length / 2 !== (sequence.length / 2 | 0)) {
                    throw 'sequence should have an even length';
                }
                /*var t, ratio = 0.0;
                 for (t in types) {
                 ratio += types[t].ratio;
                 }
                 if (ratio !== 1.0) {
                 console.error('ratio is not 1.0 (was ' + ratio + ' instead), set last type from ' + types[types.length - 1].ratio + ' to ' + (1 - types[types.length - 1].ratio));
                 //types[types.length - 1].ratio += 1 - types[types.length - 1].ratio;
                 }*/
            }
        }

        function destroy() {
            seed = 0;
            sequence = null;
            iterator = 0;
        }

        // in order to work 'seed' must NOT be undefined,
        // so in any case, you HAVE to provide a seed

        /*function seededRandom(max, min) {
         max = max || 1;
         min = min || 0;
         seed = (seed * 9301 + 49297) % 233280;
         var rnd = seed / 233280;
         return min + rnd * (max - min);
         }*/

        function createObject(distance, baseSpeed) {
            if (sequence) {
                if (iterator < sequence.length) {
                    var type = sequence.substr(iterator, 2);
                    iterator += 2;
                    if (type === '_ ') {
                        return {
                            type: 'delay'
                        };
                    }

                    return createObjectByTypeId(distance, baseSpeed, type);
                } else {
                    iterator = 0;
                    //return null;
                }
            }
            /*else {  reimplement ratio somehow, if rnd sequence is needed (may implement ratio="t1t1t1t2t3"  for ratio: 0.6 : 0.2 : 0.2)
             var from = 0.0;
             var to = seededRandom();
             for (var i = 0; i < types.length; i++) {
             if (from < to && from + types[i].ratio >= to) {
             return createObjectByType(distance, baseSpeed, types[i]);
             }
             from += types[i].ratio;
             }
             }*/
        }

        function createObjectByTypeId(distance, baseSpeed, typeId) {
            var type = types[typeId],
                obj = {
                    shape: null,
                    r: angle,
                    distance: distance
                };

            if (type.changeable) { //replace type with first subtype, if type = changable
                obj.changeable = type.changeable;
                obj.changeable.current = -1;
                typeId = type.changeable.subtypes[0];
                type = types[typeId];
            }

            obj.type = typeId;
            obj.behavior = behaviors.get(type.behavior);
            obj.speed = baseSpeed * type.speed;
            obj.value = type.value;

            return  obj;

            /*var obj={
             shape: null,
             r: angle,
             distance: distance,
             type: typeId,
             behavior: behaviors.get(type.behavior),
             changeable: type.changeable,
             subtypes: type.subtypes,
             speed: baseSpeed * type.speed,
             value: type.value
             };*/
        }

        function update(event) {
            angle += direction * event.delta / 100;
            if (angle >= startAngle + spread) {
                angle = startAngle + spread;
                direction = -1;
            } //reverse direction
            if (angle <= startAngle - spread) {
                angle = startAngle - spread;
                direction = 1;
            }
        }

        return {
            create: create,
            destroy: destroy,
            createObject: createObject,
            update: update
        };
    }
);