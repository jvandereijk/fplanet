/*globals define */
define(['lib/create',
        'core/assets',
        'core/events',
        'actors/enemyMissile',
        'models/sequence',
        'helpers/anim'
    ],
    function (createJs, assets, events, enemyMissile, Sequence, anim) {
        'use strict';

        function EnemyShip(config) {
            var container = new createJs.Container(),
                shipContainer = new createJs.Container(),
                healthContainer = new createJs.Container(),
                transform,
                flightDuration = 8000,
                maxHealth = config.health || 100,
                health = maxHealth,
                healthAlpha = 0,
                baseScaleX = config.scale * (config.x < 0 ? -1 : 1),
                baseScaleY = config.scale,
                enemyType = assets.defaults.enemy[config.id],
                bgBitmap = new createJs.Bitmap(assets.get(enemyType.assetId)),
                origin,
                width,
                height,
                active = false,
                visible = false,
                missileBaseSpeed = config.speed || 1,
                hideOnDelay = config.missiles && config.missiles.hideOnDelay,
                waves = config.missiles ? new Sequence(config.missiles) : 0,
                currentWave = null,
                missiles,
                currentMissiles = null;

            bgBitmap.sourceRect = new createJs.Rectangle(enemyType.x, enemyType.y, enemyType.w, enemyType.h);
            bgBitmap.regX = enemyType.w * 0.5;
            bgBitmap.regY = enemyType.h * 0.5;

            width = Math.abs(enemyType.w * config.scale);
            height = Math.abs(enemyType.h * config.scale);
            shipContainer.addChild(bgBitmap);

            transform = {
                x: config.x * assets.gameWidth * 0.5,
                y: config.y * assets.gameHeight * 0.5,
                scaleX: config.scale,
                scaleY: config.scale
            };

            origin = {
                x: transform.x,
                y: transform.y
            };

            shipContainer.filters = [new createJs.BlurFilter(3, 3, 1) ];

            if (config.filters && config.filters.color) {
                var f = config.filters.color;
                shipContainer.filters.push(new createJs.ColorFilter(f[0], f[1], f[2], f[3], f[4], f[5], f[6], f[7]));
            } else {
                shipContainer.filters.push(new createJs.ColorFilter(0.7, 0.7, 0.7, 1, 100, 100, 100, 1));
            }

            shipContainer.cache(-enemyType.w * 0.5, -enemyType.h * 0.5, enemyType.w, enemyType.h);
            container.addChild(shipContainer);
            healthContainer.set({
                x: -bgBitmap.regX,
                y: bgBitmap.regY * 1.2
            });
            container.alpha = 0;
            container.addChild(healthContainer);
            showHealth();

            var shipTween;

            function update(event) {

                //animate enemy sprite randomly, todo: make configurable
                var rnd = Math.random();
                if (rnd < 0.005) {
                    var tx = origin.x * (transform.x < 0 ? 1 : -1);
                    if (!shipTween || shipTween.position >= shipTween.duration) {
                        shipTween = createJs.Tween.get(transform).to({
                            x: tx
                        }, 2000, createJs.Ease.backIn);
                    }
                }

                if (waves) {
                    var duration, missile;

                    waves.update(event);
                    if (currentWave === null || waves.current.index != currentWave.index) { //current changed
                        currentWave = waves.current;

                        if (currentWave.index >= 0) {
                            missiles = new Sequence(currentWave.value);
                            currentMissiles = null;
                            //console.log('ENEMY new missiles initialized');
                        }
                    }

                    if (missiles) {
                        missiles.update(event);
                        if (currentMissiles === null || missiles.current.index != currentMissiles.index) { //current changed
                            currentMissiles = missiles.current;
                            if (currentMissiles.index < 0 && hideOnDelay) {
                                setVisible(false);
                            }
                            if (currentMissiles.index >= 0 && currentMissiles.value) {
                                setVisible(true);
                                if (currentMissiles.value !== '_ ') {
                                    duration = (1 / (missileBaseSpeed * (currentWave.value.speed || 1))) * flightDuration;
                                    missile = enemyMissile.create(currentMissiles.value, assets.tileSize * (transform.x < 0 ? 1 : -1), 0, duration, this);
                                    events.trigger('launchMissile', missile);
                                }
                            }
                        }
                    }
                }
                container.setTransform(transform.x, transform.y, transform.scaleX, transform.scaleY, transform.r);
            }

            function setVisible(v) {
                visible = v;
                return anim.fade(container, visible ? 1 : 0, 500);
            }

            function showHealth(current, max) {
                var color = '#fff', //(game.planetHealth > 70 ? '#9f9' : game.planetHealth > 40 ? '#ff9' : game.planetHealth > 15 ? '#f95' : '#f55'),
                    shape = new createJs.Shape(),
                    shape2 = new createJs.Shape(),
                    maxSegments = 24,
                    healthPercentage = current !== undefined ? current / max : 1,
                    bcolor = (healthPercentage > 0.7 ? '#0a5' : healthPercentage > 0.4 ? '#aa5' : healthPercentage > 0.15 ? '#a51' : '#a11');
                //segments = maxSegments;

                if (current !== undefined) {
                    healthAlpha = healthPercentage > 0.3 ? 0 : 0.7 - healthPercentage;
                    //segments = Math.ceil(healthPercentage * maxSegments);
                    healthContainer.alpha = 0.75;
                    anim.fade(healthContainer, healthAlpha, 3000, createJs.Ease.backIn);
                } else {
                    healthContainer.alpha = 0;
                }


                healthContainer.removeAllChildren();

                var strokeWeight = assets.tileSize * 0.4;
                shape.graphics.setStrokeStyle(strokeWeight, 0, 1)
                    .beginStroke(bcolor).moveTo(-strokeWeight * 0.2, 0).lineTo(enemyType.w + strokeWeight * 0.2, 0).endStroke();
                shape2.graphics.setStrokeStyle(strokeWeight * 0.6, 0, 1)
                    .beginStroke(color).moveTo(0, 0).lineTo(healthPercentage * enemyType.w, 0).endStroke();

                healthContainer.addChild(shape);
                healthContainer.addChild(shape2);

                healthContainer.cache(0 - strokeWeight, 0 - strokeWeight, enemyType.w + strokeWeight * 2, strokeWeight * 2);
            }


            function hitTest(x, y) {
                var ufo = {
                    x: container.x - width * 0.5,
                    y: container.y
                };
                return x > ufo.x && y < ufo.y + height && x < ufo.x + width && y > ufo.y;
            }

            return {
                shape: container,
                config: config,
                transform: transform,
                width: width,
                height: height,
                baseScaleX: baseScaleX,
                baseScaleY: baseScaleY,
                update: update,
                set health(value) {
                    health = value;
                    if (health < 0) health = 0;
                    showHealth(health, maxHealth);
                },
                get health() {
                    return health;
                },
                get maxHealth() {
                    return maxHealth;
                },
                hitTest: hitTest,
                get active() {
                    return active;
                },
                set active(value) {
                    active = value;
                },
                get visible() {
                    return visible;
                },
                show: function () {
                    return setVisible(true);
                },
                hide: function () {
                    return setVisible(false);
                }
            };
        }

        return EnemyShip;
    });