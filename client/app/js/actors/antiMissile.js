/*globals define */
define(['lib/create',
        'core/assets'
    ],
    function (createJs, assets) {
        'use strict';

        function create(src, dest, force) {
            force = force > 1 ? 1 : (force < 0 ? 0 : force);
            force = 0.1 + force * 0.9;

            var missile = new createJs.Shape();

            missile.graphics.setStrokeStyle(assets.tileSize * 0.25, 0, 1)
                .beginStroke(createJs.Graphics.getRGB(255, 255, 200, 0.75))
                .arc(0, 0, assets.tileSize * 0.1, 0, Math.PI * 2, false);

            missile.width = Math.abs(dest.x - src.x);
            missile.height = Math.abs(dest.y - src.y);
            missile.x = src.x;
            missile.y = src.y;

            var target = {
                x: (dest.x - src.x) * 5,
                y: (dest.y - src.y) * 5,
                stepX: ((dest.x - src.x) * 5 - src.x) * 0.037 * force,
                stepY: ((dest.y - src.y) * 5 - src.y) * 0.037 * force
            };

            var timeAlive = 50;

            //update ground to air missile animation, decrease speed and alpha over missile lifetime
            function update() {
                missile.x = missile.x + target.stepX;
                missile.y = missile.y + target.stepY;
                //slow down & fadeOut
                target.stepX *= 0.97;
                target.stepY *= 0.97;
                missile.alpha = timeAlive / 50;
                timeAlive--;
            }

            return {
                get timeAlive() {
                    return timeAlive;
                },
                shape: missile,
                force: force,
                target: target,
                update: update
            };
        }

        return {
            create: create

        };
    });