/*globals define */
define(['lib/create',
        'core/assets'
    ],
    function (createJs, assets) {
        'use strict';

        var states = {
                init: 0,
                launch: 1,
                exploding: 2,
                explode: 3,
                hit: 4,
                impact: 5,
                destroyed: 6
            },
            count = 0;

        function create(typeAndStrength, x, y, duration, enemyShip) {
            var delay = 0,
                type = typeAndStrength[0],
                strength = typeAndStrength[1],
                state = states.init,
                objectType = assets.defaults.missile[type],
                w = 128,
                h = 128,
                tween = null,
                scale = assets.tileSize / 2 / w,
                shape = new createJs.Bitmap(assets.get('missiles'));
            shape.sourceRect = new createJs.Rectangle(128 * (strength - 1), 0, 128, 128);
            shape.set({
                scaleX: scale,
                scaleY: scale,
                regX: w * 0.5,
                regY: h * 0.5,
                filters: [
                    new createJs.ColorFilter(1, 1, 1, 1, -255 + objectType.color.r, -255 + objectType.color.g, -255 + objectType.color.b, objectType.color.a)
                ]
            });
            shape.cache(0, 0, assets.tileSize / 2 / scale, assets.tileSize / 2 / scale);
            var container = new createJs.Container();
            container.addChild(shape);
            container.set({
                rotation: x > 0 ? 90 : -90,
                x: x,
                y: y
            });

            function update(event) {
                delay -= event.delta;
                switch (state) {
                    case states.init:
                        state = states.launch;
                        break;
                    case states.hit:
                        state = states.impact;
                        break;
                    case states.exploding:
                        if (delay <= 0) {
                            state = states.destroyed;
                        }
                        break;
                }
            }

            function explode() {
                createJs.Tween.removeTweens(container.children[0]);
                container.removeAllChildren();
                addExplosion();
                state = states.exploding;
                delay = 1000;
            }

            function addExplosion(scale) { //add exploding animation
                var explosion,
                    sheet = new createJs.SpriteSheet({
                        images: [assets.get('explosion')],
                        frames: {
                            width: 128,
                            height: 128,
                            regX: 64,
                            regY: 64,
                            count: 6
                        },
                        framerate: 20
                    });
                scale = (scale === undefined ? 1 : scale) * assets.tileSize / 128;
                explosion = new createJs.Sprite(sheet);
                explosion.alpha = 0.75;
                explosion.set({
                    scaleX: scale,
                    scaleY: scale
                });
                container.addChild(explosion);
                explosion.gotoAndPlay(1);
            }

            function launch(x, y) {
                //console.log('dmg' + (0.5 + (strength * 0.5)));
                var vx = assets.tileSize * 7 * (x > 0 ? -1 : 1), //assets.tileSize * (4 + game.level.stage.layerCount * 0.4) * (x > 0 ? -1 : 1),
                    vy = y * 0.5,
                    tx = 0,
                    ty = 0;

                container.alpha = 0.25;
                tween = createJs.Tween.get(container).to({
                    scaleX: 2,
                    scaleY: 2,
                    alpha: 1,
                    guide: {
                        path: [x, y,
                            vx, y - assets.tileSize, vx, vy,
                            vx, ty + assets.tileSize, tx, ty
                        ],
                        orient: 'fixed'
                    }
                }, duration).call(function () {
                    state = states.hit;
                });
            }

            return {
                id: ++count,
                delay: delay,
                type: type,
                color: objectType.color,
                speed: 0.01,
                damage: 0.5 + (strength * 0.5),
                strength: strength,
                shape: container,
                enemy: enemyShip,
                get state() {
                    return state;
                },
                get tween() {
                    return tween;
                },
                update: update,
                launch: launch,
                explode: explode,
                addExplosion: addExplosion
            };
        }

        return {
            create: create,
            states: states
        };
    });