/*globals define */
define(['lib/create',
        'core/assets'
    ],
    function (createJs, assets) {
        'use strict';

        var face,
            img;

        function create(type, r, d, fromShape, duration) { //, oldX, oldY, duration){
            var objectType = assets.defaults.critter[type],
                scale = assets.tileSize / objectType.w * 1.1,
                container = new createJs.Container(),
                img = new createJs.Bitmap(assets.get(objectType.assetId)),
                color = objectType.colors ? objectType.colors[0] : objectType.color;
            img.sourceRect = new createJs.Rectangle(objectType.x, objectType.y, objectType.w, objectType.h);
            container.set({
                scaleX: scale,
                scaleY: scale,
                regX: objectType.w * 0.5,
                regY: objectType.h * 0.5
            });

            container.rotation = r * 180 / Math.PI + 90 + objectType.orientation;

            if (fromShape && duration) {
                container.x = fromShape.x;
                container.y = fromShape.y;
                createJs.Tween.get(container).to({
                    x: Math.round(d * Math.cos(r)),
                    y: Math.round(d * Math.sin(r))
                }, duration, createJs.Ease.backIn);
            } else {
                container.x = Math.round(d * Math.cos(r));
                container.y = Math.round(d * Math.sin(r));
            }

            if(objectType.filter!==false) {
                img.filters = [
                    new createJs.ColorFilter(1, 1, 1, 1, color.r, color.g, color.b, color.a)
                ];

                img.cache(0, 0, objectType.w, objectType.h);
            }
            container.addChild(img);

            if(objectType.face) {
                var faceType=assets.defaults.face[objectType.face];
                if(faceType) {
                    face = new createJs.Bitmap(assets.get('face-' + faceType.id));
                    if(faceType.x !== void 0) {
                        face.sourceRect = new createJs.Rectangle(faceType.x, faceType.y, faceType.w, faceType.h);
                    }
                    if(faceType.position) {
                        face.set({
                            regX: faceType.w * 0.5,
                            regY: faceType.h * 0.5,
                            x: faceType.position.x*objectType.w,
                            y: faceType.position.y*objectType.h
                        });
                    }
                    container.addChild(face);
                }
            }

            return container;
        }

        function updateType(shape, newType) {
            var objectType = assets.defaults.critter[newType],
                scale = assets.tileSize / objectType.w * 1.1,
                color = objectType.colors ? objectType.colors[0] : objectType.color;

            shape.filters = [
                new createJs.ColorFilter(1, 1, 1, 1, color.r, color.g, color.b, color.a)
            ];

            shape.cache(0, 0, objectType.w, objectType.h);
            return shape;
        }

        function setMarker(type, shape) {
            var objectType = assets.defaults.critter[type],
                color = objectType.colors ? objectType.colors[0] : objectType.color,
                offset = assets.tileSize * 0.1,
                size = assets.tileSize * 0.5;

            shape = shape || new createJs.Shape();

            switch (objectType.shape) {
                case "circle":
                    shape.graphics.beginFill('#777')
                        .drawCircle(-offset, offset, size);
                    shape.graphics.beginFill(createJs.Graphics.getRGB(color.r, color.g, color.b, 0.8))
                        .drawCircle(0, 0, size);
                    break;
                case "triangle":
                    shape.graphics.beginFill('#777')
                        .moveTo(-size - offset, offset - size).lineTo(size - offset, offset - size).lineTo(0, size + offset).lineTo(-size - offset, offset - size).endFill();
                    shape.graphics.beginFill(createJs.Graphics.getRGB(color.r, color.g, color.b, 0.8))
                        .moveTo(-size, -size).lineTo(size, -size).lineTo(0, size).lineTo(-size, -size).endFill();
                    break;
                case "square":
                    size = assets.tileSize * 0.7;
                    shape.graphics.beginFill('#777')
                        .rect(-offset - size * 0.5, offset - size * 0.5, size, size);
                    shape.graphics.beginFill(createJs.Graphics.getRGB(color.r, color.g, color.b, 0.8))
                        .rect(0 - size * 0.5, 0 - size * 0.5, size, size);
                    break;
                case "rectangle":
                    size = assets.tileSize * 0.7;
                    shape.graphics.beginFill('#777')
                        .rect(-offset - size * 0.4, offset - size * 0.6, size * 0.8, size * 1.2);
                    shape.graphics.beginFill(createJs.Graphics.getRGB(color.r, color.g, color.b, 0.8))
                        .rect(0 - size * 0.4, 0 - size * 0.6, size * 0.8, size * 1.2);
                    break;
            }
            shape.y = assets.tileSize * 0.175;

            return shape;
        }

        return {
            create: create,
            updateType: updateType,
            setMarker: setMarker
        };

    }
);