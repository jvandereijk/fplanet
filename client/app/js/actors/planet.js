/*globals define */
define(['lib/create',
        'core/assets',
        'core/events',
        'helpers/color',
        'models/graph/circularGraph'
    ],
    function (createJs, assets, events, colorHelper, circularGraph) {
        'use strict';

        var planet,
            graph,
            radius,
            container,
            backContainer,
            gridContainer,
            planetBitmap,
            debugGrid,
            planetSpritesheet,
            planetSprite,
            planetOverlay,
            damagedTimer,
            Moods = {
                neutral: [0],
                happy: [1, 8],
                scared: [2, 3, 5, 9],
                dead: [7]
            },
            currentMood = Moods.neutral,
            stageConfig,
            planetColor;

        function create(config) {
            var planetScale;
            stageConfig = config;
            if (container) destroy();
            damagedTimer = null;
            graph = circularGraph.get();
            radius = stageConfig.planet.r * assets.tileSize;
            container = new createJs.Container();
            backContainer = new createJs.Container();
            gridContainer = new createJs.Container();
            debugGrid = new createJs.Container();

            planet = stageConfig.planet;
            planetBitmap = new createJs.Bitmap(assets.get('planet-' + planet.id));
            planetScale = assets.tileSize * 2.8 / planetBitmap.image.naturalWidth * 1.1;
            backContainer.regX = planetBitmap.image.naturalWidth / 2;
            backContainer.regY = planetBitmap.image.naturalHeight / 2;
            backContainer.scaleX = planetScale;
            backContainer.scaleY = planetScale;
            backContainer.addChild(planetBitmap);

            planetSpritesheet = new createJs.SpriteSheet({
                images: [assets.get('planet-face')],
                frames: {
                    width: 150,
                    height: 150,
                    regX: 75,
                    regY: 75
                }
            });

            createGrid();

            planetSprite = new createJs.Sprite(planetSpritesheet);
            planetSprite.gotoAndStop(0);
            var r = radius + circularGraph.layerCount() * assets.tileSize;

            debugGrid.visible = false;

            debugGrid.cache(-r, -r, r * 2, r * 2);
            gridContainer.cache(-r, -r, r * 2, r * 2);
            container.addChild(debugGrid);
            container.addChild(backContainer);
            container.addChild(gridContainer);

            planetColor = { //start yellow-ish
                r: planet.color ? planet.color.r : 255,
                g: planet.color ? planet.color.g : 215,
                b: planet.color ? planet.color.b : 155
            };
            setPlanetFace();

            events.on('debugChanged', onDebugChanged);
            events.on('stateChanged', onStateChanged);
            events.on('planetHealthChanged', onPlanetHealthChanged);
            events.on('shapeDetected', onShapeDetected);

            return container;
        }

        function setPlanetFace() {
            container.removeChild(planetOverlay);
            container.removeChild(planetSprite);
            planetOverlay = new createJs.Shape();
            planetOverlay.alpha = 0.55;
            planetOverlay.graphics.beginFill(createJs.Graphics.getRGB(planetColor.r, planetColor.g, planetColor.b, 1)).drawCircle(0, 0, assets.tileSize * 1.50);
            planetOverlay.cache(-assets.tileSize * 1.50, -assets.tileSize * 1.50, assets.tileSize * 3, assets.tileSize * 3);
            if(planet.overlay!==false){container.addChild(planetOverlay);}
            if(planet.face!==false){container.addChild(planetSprite);}
        }

        function createGrid() {
            gridContainer.uncache();
            var id, i, node, ringSize, d, x, y, circle, a, label, label2, pnode, pd, px, py, line, line2, col;
            for (id in graph) {
                if (graph[id] && graph[id].layer) {
                    node = graph[id];
                    ringSize = node.ringSize;
                    d = radius + assets.tileSize * (node.layer - 1);
                    x = d * Math.cos(node.radians);
                    y = d * Math.sin(node.radians);
                    //draw grid
                    circle = new createJs.Shape();
                    a = 0.1 - (node.layer) * (0.05 / stageConfig.layerCount); //0.2 - node.layer * (0.18 / stageConfig.layerCount)
                    circle.graphics.beginRadialGradientFill(
                        [createJs.Graphics.getRGB(255, 255, 255, a), createJs.Graphics.getRGB(0, 0, 0, a)], [0.8, 1], 0,
                        0, 0, 0, 0,
                            assets.tileSize * 0.4
                    ).drawCircle(0, 0, assets.tileSize * 0.4);
                    circle.x = x;
                    circle.y = y;
                    gridContainer.addChild(circle);

                    label = new createJs.Text(node.layer + '.' + node.cell, '13px Arial',
                        createJs.Graphics.getRGB(0, 0, 0, 0.8));
                    label.x = x;
                    label.y = y - assets.tileSize * 0.3;
                    label.textAlign = 'center';
                    debugGrid.addChild(label);
                    label2 = new createJs.Text(Math.round(node.cell / ringSize * 360), '11px Arial',
                        createJs.Graphics.getRGB(0, 0, 0, 0.8));
                    label2.x = x;
                    label2.y = y;
                    label2.textAlign = 'center';
                    debugGrid.addChild(label2);
                    //debug: draw lines to parents
                    for (i = 0; i < node.parents.length; i++) {
                        pnode = graph[node.parents[i]];
                        pd = radius + assets.tileSize * (pnode.layer - 1);
                        px = pd * Math.cos(pnode.radians);
                        py = pd * Math.sin(pnode.radians);
                        line = new createJs.Shape();
                        col = createJs.Graphics.getRGB(0, 255, 0, 0.75);
                        if (node.parents.length === 3) {
                            col = i === 0 ? createJs.Graphics.getRGB(255, 0, 0, 0.75) : i === 1 ? createJs.Graphics.getRGB(0, 255, 0, 0.75) : createJs.Graphics.getRGB(0, 0, 255, 0.75);
                        } else if (node.parents.length === 2) {
                            col = i === 0 ? createJs.Graphics.getRGB(255, 0, 0, 0.75) : createJs.Graphics.getRGB(0, 0, 255, 0.75);
                        }
                        line.graphics.beginStroke(col).moveTo(x, y).lineTo(px, py);
                        debugGrid.addChild(line);
                    }
                    //debug: draw lines to siblings
                    for (i = 0; i < node.siblings.length; i++) {
                        node = graph[node.siblings[i]];
                        d = radius + assets.tileSize * (node.layer - 1);
                        line2 = new createJs.Shape();
                        line2.graphics.beginStroke(createJs.Graphics.getRGB(0, 0, 0, 0.25)).moveTo(x, y).lineTo(d * Math.cos(node.radians),
                                d * Math.sin(node.radians));
                        debugGrid.addChild(line2);
                    }
                }
            }
        }

        function destroy() {
            container.removeAllChildren();
            events.off(onDebugChanged);
            events.off(onStateChanged);
            events.off(onPlanetHealthChanged);
            events.off(onShapeDetected);
        }

        function onDebugChanged(id, debug) {
            debugGrid.visible = debug;
        }

        function setActive(active) {
            setMood(active ? Moods.neutral : Moods.scared);

            createJs.Tween.get(gridContainer, {
                override: true
            }).to({
                alpha: active ? 1 : 0.2
            }, 200, createJs.Ease.backOut);
        }

        function setMood(mood) {
            if (currentMood !== mood) {
                currentMood = mood;
                planetSprite.gotoAndStop(mood[Math.floor(Math.random() * mood.length)]);
            }
        }

        function update(event) {
            if (damagedTimer !== null) {
                damagedTimer -= event.delta;
                if (damagedTimer < 1) {
                    damagedTimer = 0;
                    setMood(Moods.neutral);
                }
            }
        }

        function onStateChanged(msg, state) {
            if (state.newVal === assets.enums.gameStates.Paused) { //blur planet on game pause
                gridContainer.alpha = 0;
                container.filters = [
                    new createJs.BlurFilter(5, 5, 1)
                ];

                container.cache(-radius, -radius, radius * 2, radius * 2);
            }
            if (state.newVal === assets.enums.gameStates.Playing) {
                gridContainer.alpha = 1;
                container.uncache();
            }
        }

        function onPlanetHealthChanged(msg, health) {
            var mood = health.newVal > 0 ? (health.newVal > health.oldVal ? Moods.happy : Moods.scared) : Moods.dead;
            setMood(mood);
            damagedTimer = 2000;
        }

        function onShapeDetected(id, shapeEvent) {
            if (shapeEvent.type === 'ring') {
                var color = assets.defaults.critter[shapeEvent.nodeData.type].color;

                //average
                //planetColor.r = (planetColor.r + color.r) * 0.5 | 0;
                //planetColor.g = (planetColor.g + color.g) * 0.5 | 0;
                //planetColor.b = (planetColor.b + color.b) * 0.5 | 0;

                planetColor = color;
                setPlanetFace();
            }
        }

        function addExplosion() { //add exploding animation

            var explosion,
                sheet = new createJs.SpriteSheet({
                    images: [assets.get('explosion')],
                    frames: {
                        width: 128,
                        height: 128,
                        regX: 64,
                        regY: 64,
                        count: 6
                    },
                    framerate: 20
                });
            explosion = new createJs.Sprite(sheet);
            explosion.alpha = 0.75;

            container.addChild(explosion);
            explosion.gotoAndPlay(1);
            setTimeout(function () {
                container.removeChild(explosion);
                explosion = null;
            }, 1000);
        }


        /*function updateColorInput(color) {
         //console.log(color.r + ' ' + color.g + ' ' + color.b);
         if (document && document.getElementById('bgColor')) {
         //document.getElementById('bgColorAr').value = color.r * 100;
         //document.getElementById('bgColorAg').value = color.g * 100;
         //document.getElementById('bgColorAb').value = color.b * 100;
         //document.getElementById('bgColorAs').value = colorFilter[3] * 100;
         //document.getElementById('bgColorAt').value = colorFilter[7] * 100;

         document.getElementById('bgColor').value = colorHelper.rgbToHex(color.r, color.g, color.b);
         }
         }*/

        if (document && document.getElementById('bgColor')) {
            document.getElementById('bgColor').addEventListener('change', function (event) {
                setPlanetFace(colorHelper.hexToRgb(event.currentTarget.value));
            });
            /*document.getElementById('bgColorAr').addEventListener('change', function (event) {
             colorFilter[0] = event.currentTarget.value / 100;
             setColorFilter(colorFilter);
             });
             document.getElementById('bgColorAg').addEventListener('change', function (event) {
             colorFilter[1] = event.currentTarget.value / 100;
             setColorFilter(colorFilter);
             });
             document.getElementById('bgColorAb').addEventListener('change', function (event) {
             colorFilter[2] = event.currentTarget.value / 100;
             setColorFilter(colorFilter);
             });
             document.getElementById('bgColorAs').addEventListener('change', function (event) {
             colorFilter[3] = event.currentTarget.value / 100;
             setColorFilter(colorFilter);
             });
             document.getElementById('bgColorAt').addEventListener('change', function (event) {
             colorFilter[7] = event.currentTarget.value / 100;
             setColorFilter(colorFilter);
             });*/
            /*
             <input type="range" id="bgColorAs"><br/>
             <input type="range" id="bgColorAt"><br/><br/>
             <input type="color" id="planetColor"><br/>
             <input type="range" id="planetColorAs"><br/>
             <input type="range" id="planetColorAt"><br/>
             */
        }

        return {
            create: create,
            update: update,
            destroy: destroy,
            setActive: setActive,
            Moods: Moods,
            addExplosion: addExplosion
        };

    }
);