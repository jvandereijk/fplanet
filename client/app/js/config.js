/*globals require */
require.config({
    deps: ['main'],
    baseUrl: 'js/',

    shim: {
        'lib/create': {
            exports: 'createjs'
        },
        'lib/hammer': {
            exports: 'Hammer'
        },
        'lib/extensions': {
            exports: 'extensions'
        }
    }
});