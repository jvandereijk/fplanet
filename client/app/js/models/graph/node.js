/*globals define */
define(
    function () {
        'use strict';

        var doublePI = 2 * Math.PI; //constant, so less calculations are needed
        /**
         * Node 'class'
         * @param {Number} layer
         * @param {Number} cell
         */

        function Node(layer, cell) {
            var s = 6 + layer * 6,
                self = this;
            this.layer = layer;
            this.cell = cell;
            this.radians = (cell / s * doublePI).maxToZero(doublePI);
            this.parents = [];
            this.siblings = [];
            this.ringSize = s;
            this.children = [];
            this.locked = false;
            this.content = (layer === 0 ? 0 : null);
            this.id = function () {
                return self.layer + '_' + self.cell;
            };
            this.isEmpty = function () {
                return !self.locked && self.content === null;
            };

            //returns a shallow copy of important node data
            this.data = function () {
                return {
                    layer: this.layer,
                    cell: this.cell,
                    type: this.content ? this.content.type : null,
                    value: this.content ? this.content.value : null,
                    x: this.content ? this.content.shape.x : null,
                    y: this.content ? this.content.shape.y : null,
                    radians: this.radians
                };
            };
        }

        return Node;
    });