// Circular graph model
/*globals define */
define(['models/graph/node'
], function (Node) {
    'use strict';

    var doublePI = 2 * Math.PI, //constant, so less calculations are needed
        _layerCount,
        layerLocks = {},
        graph = {},
        emptyNodeQueue = [],
        moveNodeQueue = [],
        changeNodeQueue = [],
        destroyNodeQueue = [];


    /**
     * return number of layers in graph
     * @return {Number}
     */

    function layerCount() {
        return _layerCount;
    }

    /**
     * Get number of cells in current layer
     * @param  {Number} layer
     * @return {Number}
     */

    function cellCount(layer) {
        return 6 + layer * 6;
    }

    /**
     * Sort parents: low and high cell numbers are next to each other and should be sorted differently,
     * for example: if node has 3 parents : 1_0, 1_1 and 1_11, it should be sorted as: 1_11, 1_0, 1_1
     * @param  {Number} a cellNumber 1
     * @param  {Number} b cellNumber 2
     * @return {Number} sorting order
     */

    function parentNodeSort(a, b) {
        var layer = a.split('_')[0];
        var ac = a.split('_')[1];
        var bc = b.split('_')[1];
        // cell 0 and 1 should be ordered left of max in parentlist
        if ((ac === '0' || ac === '1') &&
            (bc === (cellCount(layer) - 1).toString() ||
                bc === (cellCount(layer) - 2).toString())) {
            return b > a;
        }
        return a > b;
    }

    /**
     * Init: Construct all graph nodes
     * @param  {Number} count
     * @return {Array}
     */

    function create(count) {
        var layer, cell, s;

        _layerCount = count;

        if (graph) destroy(); //cleanup
        //this.graph = {};

        /**
         * method for connecting node to parents, siblings and childrens
         * @param  {Node} node
         */

        function connectNode(node) {
            var left = (node.cell - 1).mod(node.ringSize);
            var right = (node.cell + 1).mod(node.ringSize);
            node.siblings = [node.layer + '_' + left, node.layer + '_' + right];
            if (node.layer > 0) { //top layer has no parents
                var parentLayer = node.layer - 1;
                var parentCellCount = cellCount(parentLayer);
                //find parents(cell / s) * doublePI).maxToZero( doublePI);
                var leftR = ((left - 0.5) / node.ringSize * doublePI).maxToZero(doublePI); //left boundary
                var rightR = ((right + 0.5) / node.ringSize * doublePI).maxToZero(doublePI); //right boundary
                for (var i = 0; i < parentCellCount; i++) {
                    if (isInBetweenCell((i / parentCellCount * doublePI).maxToZero(doublePI), leftR, rightR)) {
                        // add this node to parent child nodes
                        graph[parentLayer + '_' + i].children.push(node.id());
                        // add parent
                        node.parents.push(parentLayer + '_' + i);
                    }
                }
            }
        }

        //built graph
        for (layer = 0; layer <= _layerCount; layer++) {
            s = cellCount(layer);
            for (cell = 0; cell < s; cell++) {
                graph[layer + '_' + cell] = new Node(layer, cell);
                connectNode(graph[layer + '_' + cell]); // make parent, sibling and child connections
            }
        }

        //sort parents
        for (layer = 0; layer <= _layerCount; layer++) {
            s = cellCount(layer);
            for (cell = 0; cell < s; cell++) {
                if (cell === 0 || cell === s - 1) {
                    graph[layer + '_' + cell].parents.sort(parentNodeSort);
                }
            }
        }
        return graph;
    }

    function destroy() {
        var item, c = 0;
        for (item in graph) {
            item = null;
            c++;
        }

        layerLocks = {};
        graph = {};
        emptyNodeQueue = [];
        moveNodeQueue = [];
        changeNodeQueue = [];
        destroyNodeQueue = [];
    }

    /**
     * Cehck if angle x is in between left and right angles
     * @param  {[Number]}  x
     * @param  {[Number]}  left angle
     * @param  {[Number]}  right angle
     * @return {Boolean}
     */

    function isInBetweenCell(x, left, right) {
        //extend bound to wrap around if needed
        if (right < left) {
            if (x <= right) {
                x += doublePI;
            }
            right += doublePI;
        }
        return x >= left && x <= right;
    }

    /**
     * Set node content
     * @param {Number} layer
     * @param {Number} cell
     * @param {Object} content
     */

    function setNode(layer, cell, content) {
        var node = graph[layer + '_' + cell];
        if (!node.isEmpty()) {
            return null;
        }
        node.content = content;
        return node;
    }

    /**
     * [getNode description]
     * @param {Number} layerOrId   [description]
     * @param {Number} cell    [description]
     */
    function getNode(layerOrId, cell) {
        if (cell === undefined) return graph[layerOrId];
        return graph[layerOrId + '_' + cell];
    }

    /**
     * Get queue by action type
     * @param  {String} action
     * @return {Array} queue
     */

    function getQueue(action) {
        return (action === 'empty' ? emptyNodeQueue :
            (action === 'move' ? moveNodeQueue :
                (action === 'destroy' ? destroyNodeQueue :
                    changeNodeQueue)));
    }

    /**
     * Get next node actiopn, remove it from the queue and release lock
     * @param  {String} action
     * @return {Object} NodeAction
     */

    function nextNodeAction(action) {
        var queue = getQueue(action);
        if (queue.length === 0) {
            return null;
        }
        var actionNode = queue.splice(0, 1)[0]; //fifo
        if (action !== 'destroy') {
            graph[actionNode.id].locked = false;
        }
        return actionNode;
    }

    /**
     * Add NodeAction to queue
     * @param {String} action
     * @param {Object} node
     * @param {Object} data
     */

    function addNodeAction(action, node, data) {
        if (layerLocks['layer_' + node.layer]) {
            if (layerLocks['layer_' + node.layer].action !== action && layerLocks['layer_' + node.layer].data !== data) {
                return false;
            }
            /*
             if (layerLocks['layer_' + node.layer].action === action && layerLocks['layer_' + node.layer].data === data) {
             node.locked = false;
             //todo remove current actions for node
             }
             */
        }
        if (node.locked) {
            return false;
        }
        if (action === 'move') {
            if (!data || data.locked) { //important to check for data, since behaviors wont do that and rely on gracefull fail here
                return false;
            }
            data.locked = true; //lock target (after move (animation), lock should be released manually)
            data = data.id();
        }
        node.locked = true;
        var actionNode = {
            id: node.id(),
            action: action,
            data: data
        };
        var q = getQueue(action);
        q.push(actionNode);
        return true;
    }

    /**
     * Get distance to nearest node in layer (left = negative number, righ positive)
     * @param  {Number} layer
     * @param  {Number} cell
     * @return {Number} distance
     */

    function getNearestFreeNode(layer, cell) {
        var i, c, lc = 0,
            rc = 0,
            ringSize = cellCount(layer);
        for (i = 0; i < ringSize; i++) {
            c = cell - i < 0 ? cell - i + ringSize : cell - i;
            if (graph[layer + '_' + (cell - i < 0 ? cell - i + ringSize : cell - i)].content === null) {
                lc = i;
                break;
            }
        }
        for (i = 0; i < ringSize; i++) {
            c = cell + i >= ringSize ? cell + i - ringSize : cell + i;
            if (graph[layer + '_' + (cell - i < 0 ? cell - i + ringSize : cell - i)].content === null) {
                rc = i;
                break;
            }
        }
        if (lc === 0 && rc === 0) {
            return 0;
        }
        return lc <= rc ? -lc : rc;
    }

    /**
     * Find last available spot in parents
     * @param  {String} id
     * @param  {String} prevId
     * @return {Array} array of parent ids
     */

    function getParentPath(id, prevId) {
        var path = [];
        var node = graph[id];
        if (node.content !== null) {
            return path;
        }
        if (prevId && graph[prevId].layer === node.layer) {
            return path;
        } //dont go further sideways
        path.push(id); //add current node
        //check parents
        var parentPath = [];
        for (var i = 0; i < node.parents.length; i++) {
            var pi = getParentPath(node.parents[i], id);
            if (parentPath.length === 0 || pi.length > 0 && pi.length < parentPath.length) {
                parentPath = pi;
            }
        }
        if (parentPath.length > 0) {
            return path.concat(parentPath);
        }
        var leftRoute = getParentPath(node.layer + '_' + (node.cell - 1).mod(node.ringSize), id);
        var rightRoute = getParentPath(node.layer + '_' + (node.cell + 1).mod(node.ringSize), id);
        return path.concat(leftRoute >= rightRoute ? leftRoute : rightRoute);
    }

    /**
     * Find first free spot in children
     * use sideways=true to allow looking through siblings
     * @param  {String} id
     * @param  {String} sideWays
     * @return {String}
     */

    function getFirstEmptyChild(id, sideWays) {
        var node = graph[id];
        if (node.content === null) { //find empty spot
            return id;
        }
        //check children
        //for (var i = 0; i < node.children.length; i++) {
        //return getFirstEmptyChild(node.children[i], sideWays);
        //}
        if (node.children.length > 0) {
            return getFirstEmptyChild(node.children[0], sideWays);
        }
        //check sideways
        if (sideWays === 'left' || sideWays === 'both') {
            return getFirstEmptyChild(node.layer + '_' + (node.cell - 1).mod(node.ringSize), sideWays);
        }
        if (sideWays === 'right' || sideWays === 'both') {
            return getFirstEmptyChild(node.layer + '_' + (node.cell + 1).mod(node.ringSize), sideWays);
        }
        return null;
    }

    /**
     * Empty content of node if not locked
     * @param  {Number} layer
     * @param  {Number} cell
     * @return {Boolean} succes
     */

    function emptyNode(layer, cell) {
        var node = graph[layer + '_' + cell];
        if (node.locked) {
            return false;
        }
        node.content = null;
        return true;
    }

    /**
     * Check if all cells in layer are of the same type
     * @param  {Number} layer
     * @param  {Number} type
     * @return {Boolean}
     */

    function layerIsType(layer, type) {
        if (graph[layer + '_0'].locked || graph[layer + '_0'].content === null) {
            return false;
        }
        type = type || graph[layer + '_0'].content.type;
        for (var i = 1; i < cellCount(layer); i++) {
            if (graph[layer + '_' + i].locked || graph[layer + '_' + i].content === null || graph[layer + '_' + i].content.type !==
                type) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if all cells in layer are filled and of same 2 types
     * @param  {Number} layer
     * @return {Boolean}
     */

    function layerIsTwoTypes(layer) {
        var types = [],
            i = cellCount(layer);
        while (i--) {
            var content = graph[layer + '_' + i].content;
            if (content === null) return false;
            if (content !== null) {
                if (types.indexOf(content.type) < 0) types.push(content.type);
                if (types.length > 2) return false;
            }
        }
        return types.length === 2 ? types : false;
    }

    function layerNodeTypes(layer) {
        var types = [],
            i = cellCount(layer);
        while (i--) {
            var content = graph[layer + '_' + i].content;
            if (content !== null) {
                if (types.indexOf(content.type) < 0) types.push(content.type);
            }
        }
        return types;
    }

    function layerIsFull(layer) {
        if (graph[layer + '_0'].locked || graph[layer + '_0'].content === null) {
            return false;
        }
        var i = cellCount(layer);
        while (i--) {
            if (graph[layer + '_' + i].content === null) return false;
        }
        return true;
    }

    function getLayerLock(layer) {
        return layerLocks['layer_' + layer];
    }

    function removeLayerLock(layer, action) {
        if (layerLocks['layer_' + layer] && layerLocks['layer_' + layer].action === action) {
            delete layerLocks['layer_' + layer];
        }
    }

    function setLayerLock(layer, action, data) {
        if (!layerLocks['layer_' + layer]) { //set new lock
            layerLocks['layer_' + layer] = {
                action: action,
                data: data
            };
        }
        if (layerLocks['layer_' + layer].action === action) { //if action is the same, return current lock
            return layerLocks['layer_' + layer].data; //return existing lock
        }
        console.warn('failed set lock layer ' + layer + ' type ' + data.type + ' action ' + action + ' current lock type:' + layerLocks['layer_' + layer].data.type);
        return false;
    }

    /**
     * Get graph array
     * @return {Array}
     */

    function get() {
        return graph;
    }

    /**
     * Public methods for this module
     */
    return {
        Node: Node,
        create: create,
        destroy: destroy,
        get: get,
        layerCount: layerCount,
        cellCount: cellCount,
        getNearestFreeNode: getNearestFreeNode,
        getParentPath: getParentPath,
        getFirstEmptyChild: getFirstEmptyChild,
        emptyNode: emptyNode,
        nextNodeAction: nextNodeAction,
        addNodeAction: addNodeAction,
        getLayerLock: getLayerLock,
        setLayerLock: setLayerLock,
        removeLayerLock: removeLayerLock,
        layerIsType: layerIsType,
        layerIsTwoTypes: layerIsTwoTypes,
        layerIsFull: layerIsFull,
        layerNodeTypes: layerNodeTypes,
        setNode: setNode,
        getNode: getNode
    };
});