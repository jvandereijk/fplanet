/*globals define */
define(
    function () {
        'use strict';

        function GoalProgress(type, amount, nodeData, additional) {
            this.type = type;
            this.amount = amount;
            this.nodeData = nodeData;
            this.additional = additional; //additional goal data
        }

        return GoalProgress;
    });