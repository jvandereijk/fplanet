/*globals define */
define(
    function () {
        'use strict';

        function ShapeEvent(action, type, nodeData, parts) {
            this.action = action;
            this.type = type;
            this.nodeData = nodeData;
            this.parts = parts;
        }

        return ShapeEvent;

    });