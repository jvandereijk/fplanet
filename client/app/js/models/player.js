/*globals define */
define(['lib/storage'],
    function (storage) {
        'use strict';

        var name = '',
            language = '';

        function init() {
            var player = storage.get('player');
            if (!player) {
                player = {
                    name: 'bla',
                    language: 'en'
                };
                storage.set('player', player);
            }
            name = player.name;
            language = player.language;
        }

        return {
            init: init,
            get name() {
                return name;
            },
            get language() {
                return language;
            }
        };
    });