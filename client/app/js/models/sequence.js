//Interval helper object, to check if certain amount of (ingame) time has passed
/*globals define */
define(
    function () {
        'use strict';

        //noinspection UnnecessaryLocalVariableJS
        var Sequence = function (o) {
            if (!o) return null;
            var current,
                timeAlive,
                active,
                finished,
                stringSequence,
                config = initSequence();
            //console.log('NEW sequence ' + o.id);

            function initSequence() {
                var s = JSON.parse(JSON.stringify(o));
                /* reset sequence data */
                s.delay = s.delay || 0;
                s.duration = s.duration || 0;
                s.loop = s.loop || false;
                s.speed = s.speed || 1;

                timeAlive = 0;
                current = -1;
                active = false;
                finished = false;
                stringSequence = (s.items + '' === s.items);
                //console.log('INIT sequence ' + o.id);
                return s;
            }

            function update(event) {
                timeAlive += event.delta;
                if (!active && timeAlive > config.delay) {
                    activateSequence();
                }
                if (!finished) {
                    if (config.duration > 0 && timeAlive > config.delay + config.duration) {
                        endSequence();
                    } else {
                        processSequence();
                    }
                }
            }

            function activateSequence() {
                active = true;
                current = -1;
                //console.log('ACTIVATE sequence ' + o.id + ' timeAlive:' + timeAlive + ' > delay:' + config.delay + '  current:' + current);
            }

            function endSequence() {
                //console.log('END sequence ' + o.id);
                finished = true;
                if (config.loop) {
                    initSequence();
                }
            }

            function processSequence() {
                var i, delay = config.delay;
                if (stringSequence) {
                    //console.log(o.id + ' ' + timeAlive + ' > ' + (config.delay + (config.items.length / 2) * config.interval));
                    if (timeAlive > config.delay + (config.items.length / 2 + 1) * config.interval) {
                        if (config.loop) {
                            //console.log(o.id + ' sequence reset');
                            timeAlive = 0;
                            current = -1;
                        } else {
                            //console.log(o.id + ' sequence finished');
                            finished = true;
                        }
                    } else {
                        for (i = 0; i < config.items.length; i += 2) {
                            delay += config.interval;
                            if (timeAlive > delay && i > current) {
                                current = i;
                                //if (i !== oldCurrent) console.log(o.id + ' timeAlive:' + timeAlive + ' > delay:' + delay + '  current:' + current);
                            }
                        }
                    }
                } else {
                    for (i = 0; i < config.items.length; i++) {
                        //get delay before 'next' item
                        delay += config.items[i].delay;
                        if (timeAlive > delay && i > current) {
                            current = i;
                            //if (i !== oldCurrent) console.log('GOTO NEXT' + o.id + ' timeAlive:' + timeAlive + ' > delay:' + delay + '  current:' + current);
                        }
                        //get duration of 'next' item
                        if (config.items[i].items.length > 0) delay += (config.items[i].items.length / 2 + 1) * config.items[i].interval;
                    }
                }
            }

            return {
                update: update,
                get current() {
                    var value = null;
                    if (current >= 0 && !finished) {
                        if (stringSequence) {
                            value = config.items.substr(current, 2);
                        } else {
                            value = config.items[current];
                        }
                    }
                    return {
                        index: current,
                        value: value
                    };
                }
            };
        };

        return Sequence;
    });