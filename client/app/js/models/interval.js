//Interval helper object, to check if certain amount of (ingame) time has passed
/*globals define */
define(
    function () {
        'use strict';

        var Interval = function (value) {
            var max = value,
                current = value;
            this.update = function (d) {
                if (current > 0) {
                    current -= d;
                    return false;
                } else {
                    current += max;
                    return true;
                }
            };
        };

        return Interval;

    });