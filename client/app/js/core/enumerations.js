/* dont use this module directly, use game.js as proxy instead */
/*globals define */
define([],
    function () {
        'use strict';
        var gameStates = { //always use enum values
            Startup: 0,
            Loading: 1,
            Menu: 2,
            //Start: 3,
            Playing: 4,
            //EndLevel: 5,
            Paused: 6,
            GameOver: 7,
            Credits: 8
        };

        var gameObjectBehaviors = {
            Boll: 1,
            Block: 2,
            Wedge: 4,
            Cluster: 16,
            Bomb: 256
        };

        return {
            gameStates: gameStates,
            gameObjectBehaviors: gameObjectBehaviors
        };
    });