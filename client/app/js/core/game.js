/*globals define */
define(['core/assets',
        'models/player',
        'models/goalProgress',
        'controllers/goals',
        'core/events'
    ],
    function (gameAssets, player, GoalProgress, goals, events) {
        'use strict';

        var debug = false,
            state = gameAssets.enums.gameStates.Startup,
            speed = 1.0,
            speedMultiplier = 1.0,
            level = null,
            maxPlanetHealth = 100,
            planetHealth = maxPlanetHealth,
            mode = 0,
            score = 0,
            bonus = 0,
            bonusTimer = 0,
            cellBonus = 0,
            levelId = 0;

        function load() {
            state = gameAssets.enums.gameStates.Loading;
            player.init();
            return gameAssets.init(onLoadPartComplete, player.language, onLoadComplete);
        }

        function onLoadPartComplete(args) {
            events.trigger('gamePartLoaded', args);
        }

        function onLoadComplete() {
            events.trigger('gameLoaded', true);
        }

        function initLevel(lvlId) {
            score = 0;
            bonus = 1;
            bonusTimer = 0;
            cellBonus = 0;
            planetHealth = maxPlanetHealth;
            levelId = lvlId;
            state = gameAssets.enums.gameStates.Loading;
            speedMultiplier = 1.0;
            speed = gameAssets.data('GameModes')[mode].speed;
            level = JSON.parse(JSON.stringify(gameAssets.level(mode, levelId)));
            goals.create(level.goals);
        }

        events.on('addScore', function (id, score) {
            addScore(score, 'event');
        });

        events.on('gameOver', function (/*id, reason*/) {
            setState(gameAssets.enums.gameStates.GameOver); //todo: decide game over or empty inner layer ?
        });

        function setState(value) {
            var old = state;
            state = value;
            events.trigger('stateChanged', {
                oldVal: old,
                newVal: state
            });
        }

        function addScore(value, source) {
            if (source !== 'event') { //dont apply bonus when source == 'event'
                bonusTimer = value < 0 ? 0 : bonusTimer + value * 500;
                bonus = Math.floor(bonusTimer / 5000) + 1;
                if (bonus > 5) {
                    bonus = 5;
                    bonusTimer = 25000;
                }
                value = value * bonus + cellBonus * bonus | 0;
                cellBonus += 0.2;
            }
            if (value > 0) {
                score += value;
                goals.progress(new GoalProgress('score', value));
                if (source !== 'event') {
                    events.trigger('scoreAdded', {
                        value: value,
                        source: source
                    });
                }
            }

            if (value > 0) {
                goals.progress(new GoalProgress('single-score', value));
            }
        }

        function update(event) {
            goals.update(event);

            if (bonusTimer > 0) {
                bonusTimer -= event.delta;
            }
            if (bonusTimer < 0) {
                bonusTimer = 0;
            }
            if (cellBonus > 0) {
                cellBonus -= event.delta;
            }
            if (cellBonus < 0) {
                cellBonus = 0;
            }

            bonus = Math.floor(bonusTimer / 5000) + 1;
            if (bonus > 10) bonus = 10;

            /*if (speedMultiplier < 1) { //slowly recover speed
             speedMultiplier += event.delta * 0.0001;
             if (speedMultiplier > 1) {
             speedMultiplier = 1;
             }
             console.log(speedMultiplier);
             }*/
        }

        return {
            get mode() {
                return mode;
            },
            set mode(value) {
                if (!gameAssets.data('GameModes')[value]) {
                    throw 'GameMode ' + value + ' does not exists.';
                }
                mode = value;
            },
            get levelId() {
                return levelId;
            },
            get level() {
                return level;
            },
            get goals() {
                return goals;
            },
            get score() {
                return score;
            },
            get bonus() {
                return bonus;
            },
            get state() {
                return state;
            },
            set state(value) {
                setState(value);
            },
            get debug() {
                return debug;
            },
            set debug(value) {
                debug = value;
                events.trigger('debugChanged', debug);
            },
            get speed() {
                return speed * speedMultiplier;
            },
            get speedMultiplier() {
                return speedMultiplier;
            },
            set speedMultiplier(m) {
                speedMultiplier = m;
            },
            /*get assets() {
             return gameAssets;
             },*/
            get planetHealth() {
                return planetHealth;
            },
            set planetHealth(value) {
                var old = planetHealth;
                planetHealth = value;
                if (planetHealth < 1) planetHealth = 0;
                if (planetHealth > maxPlanetHealth) planetHealth = maxPlanetHealth;
                if (old !== planetHealth) events.trigger('planetHealthChanged', {
                    oldVal: old,
                    newVal: planetHealth,
                    maxVal: maxPlanetHealth
                });
                if (planetHealth === 0) {
                    var looseGoal = goals.findCurrentTarget('hidden-killed'); //todo: refactor to goals.js
                    if (looseGoal) {
                        if (!looseGoal.complete) {
                            looseGoal.complete = true;
                            events.trigger('goalCompleted', looseGoal);
                        }
                    } else {
                        events.trigger('gameOver', 'no-planet-health');
                    }
                }
            },
            /*get states() {
             return gameAssets.enums.gameStates;
             },
             get objectBehaviors() {
             return gameAssets.enums.gameObjectBehaviors;
             },
             /*get enemyTypes() {
             return globals.enemyTypes;
             },*/
            load: load,
            initLevel: initLevel,
            update: update,
            addScore: addScore
        };
    });