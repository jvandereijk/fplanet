/*globals define */
define(['lib/pubsub',
    'models/shapeEvent'
], function (pubSub, ShapeEvent) {
    'use strict';

    function triggerShapeDetected(shapeType, nodeData, parts) {
        pubSub.publish('shapeDetected', new ShapeEvent('detected', shapeType, nodeData, parts));
    }

    function trigger(msg, data) {
        pubSub.publish(msg, data);
    }

    function on(msg, callback) {
        pubSub.subscribe(msg, callback);
    }

    function off(msg) {
        pubSub.unsubscribe(msg);
    }

    return {
        ShapeEvent: ShapeEvent,
        triggerShapeDetected: triggerShapeDetected,
        trigger: trigger,
        on: on,
        off: off
    };

});