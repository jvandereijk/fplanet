/*globals define */
define(['lib/create',
        'core/enumerations'],
    function (createJs, enumerations) {
        'use strict';

        var queue,
            resources,
            gamedata,
            levelData,
            tileSize = 640 / 12,
            gameWidth = 640,
            gameHeight = 980;

        function init(handleFileLoad, language, handleComplete) {
            queue = new createJs.LoadQueue(false);
            queue.addEventListener('fileload', handleFileLoad);
            queue.addEventListener('complete', function () {
                resources = queue.getResult('resources');
                gamedata = queue.getResult('gamedata');
                levelData = queue.getResult('levels');

                if (handleComplete) {
                    handleComplete();
                }
            });

            var manifest = [
                {
                    id: 'resources',
                    src: 'data/resources-' + language + '.json'
                },
                {
                    id: 'gamedata',
                    src: 'data/gamedata.json'
                },
                {
                    id: 'levels',
                    src: 'data/levels.json'
                },
                {
                    id: 'planet-gray',
                    src: 'img/planet/gray.png'
                },
                {
                    id: 'planet-fplanet2',
                    src: 'img/planet/fplanet2.png'
                },
                {
                    id: 'planet-moon',
                    src: 'img/planet/fmoon.png'
                },
                {
                    id: 'planet-face',
                    src: 'img/planet/face.png'
                },
                {
                    id: 'planet-cot',
                    src: 'img/planet/cot.png'
                },
                {
                    id: 'planet-blue-ring',
                    src: 'img/planet/blue-ring.png'
                },
                {
                    id:'bg-cot-0',
                    src:'img/background/layer_0/cot.png'
                },
                {
                    id:'bg-cot-1',
                    src:'img/background/layer_1/cot_001.png'
                },
                {
                    id:'bg-cot-2',
                    src:'img/background/layer_2/cot.png'
                },
                ,
                {
                    id:'bg-blue-ring-0',
                    src:'img/background/layer_0/blue-ring.png'
                },
                {
                    id:'bg-blue-ring-1',
                    src:'img/background/layer_1/blue-ring.png'
                },
                {
                    id:'bg-blue-ring-2',
                    src:'img/background/layer_2/blue-ring.png'
                },
                {
                    id: 'bg-background-0',
                    src: 'img/background/clouds.jpg'
                },
                {
                    id: 'bg-clouds2-0',
                    src: 'img/background/clouds2.jpg'
                },
                {
                    id: 'bg-fuel1-0',
                    src: 'img/background/fuel1.jpg'
                },
                {
                    id: 'bg-fuel4-0',
                    src: 'img/background/fuel4.jpg'
                },
                {
                    id: 'lightning-0',
                    src: 'img/background/flightning.jpg'
                },
                {
                    id: 'ufo',
                    src: 'img/ufo.png'
                },
                {
                    id: 'goodguys',
                    src: 'img/critters/goodguys.png'
                },
                {
                    id: 'round',
                    src: 'img/critters/round.png'
                },
                {
                    id: 'beestje3',
                    src: 'img/critters/beestje03_voorbeeld.png'
                },
                {
                    id: 'face-big-eyes',
                    src: 'img/critters/face/eyes-big.png'
                },
                ,
                {
                    id: 'face-eyes-small',
                    src: 'img/critters/face/eyes-small.png'
                },
                {
                    id: 'face-eyes-black',
                    src: 'img/critters/face/eyes-black.png'
                },{
                    id: 'face-eyes-white',
                    src: 'img/critters/face/eyes-white.png'
                },
                {
                    id: 'missiles',
                    src: 'img/missiles.png'
                },
                {
                    id: 'explosion',
                    src: 'img/explosion.png'
                }
            ];

            queue.loadManifest(manifest);
            return manifest.length;
        }

        function get(id) {
            return queue.getResult(id);
        }

        function text(id) {
            return resources[id];
        }

        function font(id) {
            return resources['Font-' + id];
        }


        function data(id) {
            return gamedata[id];
        }

        function level(mode, lvl) {
            //var id = gamedata.GameMode[mode].id + '-' + lvl;
            for (var i = 0; i < levelData.length; i++) {
                if (levelData[i].mode === gamedata.GameModes[mode].id && levelData[i].id === lvl.toString()) {
                    return levelData[i];
                }
            }
            return null;
        }


        function nextLevel(mode, lvl) {
            //var id = gamedata.GameMode[mode].id + '-' + lvl;
            var myLevels = levels(mode);
            for (var i = 0; i < myLevels.length; i++) {
                if (myLevels[i].id === lvl.toString()) {
                    return i + 1 < myLevels.length ? myLevels[i + 1] : null;
                }
            }
            return null;
        }

        function levels(mode) {
            var myLevels = [];
            for (var i = 0; i < levelData.length; i++) {
                if (levelData[i].mode === gamedata.GameModes[mode].id) {
                    myLevels.push(levelData[i]);
                }
            }
            return myLevels;
        }

        return {
            init: init,
            get: get,
            text: text,
            font: font,
            data: data,
            level: level,
            nextLevel: nextLevel,
            levels: levels,
            get defaults() {
                return gamedata.defaults;
            },
            get gameData() {
                return gamedata;
            },
            enums: enumerations,
            get tileSize() {
                return tileSize;
            },
            set gameWidth(value) {
                gameWidth = value;
                //tileSize = 53;
                //tileSize = gameWidth / 12;
            },
            get gameWidth() {
                return gameWidth;
            },
            set gameHeight(value) {
                gameHeight = value;
            },
            get gameHeight() {
                return gameHeight;
            }
        };
    });