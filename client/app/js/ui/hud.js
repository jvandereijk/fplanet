/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'models/player',
        'ui/controls',
        'ui/score'
    ],
    function (createJs, assets, game, events, player, controls, score) {
        'use strict';

        var container,
            titleContainer,
            dialogContainer,
            activeDialog,
            activeInstruction,
            menuButton;

        function create() {
            if (container) destroy(); //make sure we cleaned up properly
            container = new createJs.Container();
            menuButton = controls.createMenuIconButton(assets.tileSize * 0.35, assets.tileSize * 0.2, assets.tileSize, assets.tileSize);
            menuButton.addEventListener('click', onMenuClick);
            titleContainer = new createJs.Container();
            titleContainer.addEventListener('click', onTitleClick);

            container.addChild(menuButton);
            container.addChild(titleContainer);
            container.addChild(score.create());

            if (!dialogContainer) { //restore dialog after pause
                dialogContainer = new createJs.Container();
            }
            container.addChild(dialogContainer);

            events.on('stateChanged', onStateChanged);
            events.on('levelStarted', onLevelStarted);
            events.on('goalStarted', onGoalStarted);
            events.on('goalProgress', onGoalProgress);
            events.on('goalCompleted', onGoalCompleted);
            events.on('keyDown', onKeyDown);

            return container;
        }

        function destroy() {
            if (menuButton) menuButton.removeEventListener('click', onMenuClick);
            if (titleContainer) titleContainer.removeEventListener('click', onTitleClick);
            events.off(onStateChanged);
            events.off(onGoalStarted);
            events.off(onLevelStarted);
            events.off(onGoalProgress);
            events.off(onGoalCompleted);
            events.off(onKeyDown);
        }

        function onMenuClick(ev) {
            game.state = assets.enums.gameStates.Paused;
        }

        function onTitleClick() {
            showInstruction(game.goals.current);
        }


        function onStateChanged(id, state) {
            /*	if (container) {
             switch (state.newVal) {
             case assets.enums.gameStates.GameOver:
             gameOverContainer.alpha = 1;
             titleContainer.alpha = 0;
             break;
             default:
             gameOverContainer.alpha = 0;
             titleContainer.alpha = 1;
             break;
             }
             }*/
        }

        function onLevelStarted(id, lvl) {
            titleContainer.removeAllChildren();
        }

        function onGoalStarted(id, goal) {
            onGoalProgress(id, goal);
            showInstruction(goal, function onGoalStartedDialogClose() {
                events.trigger('goalStartedConfirmed', goal);
            });
        }

        function onGoalProgress(id, goal) {
            var i, target, progress, text, color;

            titleContainer.removeAllChildren();

            for (i = 0; i < goal.targets.length; i++) {
                target = goal.targets[i];
                if (target.type.substr(0, 7) === 'hidden-') break;
                color = target.subtype ? createJs.Graphics.getRGB(assets.defaults.critter[target.subtype].color.r,
                    assets.defaults.critter[target.subtype].color.g,
                    assets.defaults.critter[target.subtype].color.b) : null;
                progress = target.compare === 'abs' ? Math.abs(target.progress) : target.progress;
                progress = (progress > target.amount ? target.amount : progress);
                progress += '/' + target.amount;

                text = assets.text('Goal-' + target.type);
                if (!text) text = target.type.capitaliseFirstLetter();
                switch (target.type) {
                    case 'survive':
                        text = 'Seconds left';
                        progress = target.amount - target.progress;
                        if (progress < 0) progress = 0;
                        break;
                }

                text += ' ' + progress;

                if (target.compare === 'max') {
                    text += ' ' + assets.text('max');
                }
                titleContainer.addChild(controls.createOutlinedTitle(text, assets.tileSize * 2, (i + 0.4) * assets.tileSize * 0.6, assets.gameWidth - assets.tileSize * 4, assets.tileSize * 0.5, color));
                /*if (target.type === 'survive') {
                 text = 'Planet health ' + Math.round(game.planetHealth);
                 color = game.planetHealth > 70 ? '#9f9' : game.planetHealth > 40 ? '#ff9' : game.planetHealth > 15 ? '#f95' : '#a55';
                 titleContainer.addChild(controls.createOutlinedTitle(text, assets.tileSize * 2, (i + 1.4) * assets.tileSize * 0.6, assets.gameWidth - assets.tileSize * 4, assets.tileSize * 0.5, color));

                 }*/
            }
        }

        function onGoalCompleted(id, goal) {
            onGoalProgress(id, goal);
            //if (!goal.finished) {
            showInstruction(goal, function onGoalCompletedDialogClose() {
                events.trigger('goalCompletedConfirmed', goal);
            });
            //}
        }

        function onKeyDown(id, evt) {
            if (evt.keyCode === 32 || evt.keyCode === 13) { // space or enter ? close dialog
                if (activeDialog) {
                    activeDialog.close();
                }
            }
        }

        function showInstruction(goal, callback) {

            var text = goal.complete && callback ? goal.text.succes[player.language] : goal.text.start[player.language], //if !callback always show instruction
                key = goal.id + '.' + text;

            if (text.length < 1) return;

            function openDialog() {
                activeInstruction = key;
                activeDialog = startDialog(text, function openDialogCallback() {
                    if (callback) {
                        callback();
                    }
                    activeInstruction = null;
                    activeDialog = null;
                });
            }

            if (activeInstruction !== key) {
                if (activeInstruction && activeDialog) {
                    //close previous, and then open new after delay
                    activeDialog.close();
                    activeInstruction = null;
                    activeDialog = null;

                    setTimeout(openDialog, 250);
                } else {
                    //open new
                    openDialog();
                }
            }
        }

        function startDialog(text, openCallback) {
            if (dialogContainer) {
                dialogContainer.removeAllChildren();
            }
            var closing = false,
                dialog = controls.createDialog(text, assets.gameWidth * 0.5, assets.gameHeight * 0.75, assets.gameWidth - assets.tileSize * 4, assets.tileSize * 0.5);
            dialogContainer.addChild(dialog);

            //animate in: animate from planet
            dialog.set({
                alpha: 0,
                scaleX: 0,
                scaleY: 0
            });

            createJs.Tween.get(dialog, {
                override: true
            }).to({
                alpha: 1,
                scaleX: 1,
                scaleY: 1,
                x: assets.tileSize * 2,
                y: assets.tileSize * 3
            }, 200, createJs.Ease.easeIn);

            function onDialogCloseClick() {
                close(); //call close without args
            }

            function close(closeCallback) {
                //animate out: animate to menu button (as a clue that you can find the Instruction there too)
                if (!closing) {
                    closing = true;
                    dialog.removeEventListener('click', onDialogCloseClick);
                    createJs.Tween.get(dialog, {
                        override: true
                    }).to({
                        alpha: 0,
                        scaleX: 0.1,
                        scaleY: 0.1,
                        x: assets.tileSize,
                        y: assets.tileSize
                    }, 200, createJs.Ease.easeIn)
                        .call(function onDialogClosed() {
                            dialogContainer.removeChild(dialog);
                            if (openCallback) openCallback(); //optional callback when dialog closes
                            if (closeCallback) closeCallback(); //additional optional callback when directly closing
                        });
                }
            }

            dialog.addEventListener('click', onDialogCloseClick);
            setTimeout(close, 2000 + text.length * 30);

            return {
                close: close //return handle to close this dialog
            };
        }

        function addShape(shape){
            container.addChild(shape);
        }

        function removeShape(shape){
            container.removeChild(shape);
        }

        function update(event) {
            if (container) {
                if (game.state === assets.enums.gameStates.Playing) {
                    score.update(event);
                }
            }
        }

        return {
            create: create,
            update: update,
            destroy: destroy,
            addShape: addShape,
            removeShape: removeShape
        };
    });