/*globals define */
define(['lib/create',
        'core/assets',
        'core/events'
    ],
    function (createJs, assets, events) {
        'use strict';

        var container, progressText, progressOutlineText;

        function create(w, h) {

            container = new createJs.Container();

            var shape = new createJs.Shape();
            shape.graphics.beginFill(createJs.Graphics.getRGB(0, 0, 0, 0.9));
            shape.graphics.rect(0, 0, w, h);
            container.addChild(shape);
            container.addChild(progressOutlineText = new createJs.Text('0 %', 'bold ' + assets.tileSize + 'px subconscious_limboregular', '#000').set({
                textAlign: 'center',
                snapToPixel: true,
                outline: assets.tileSize * 0.25,
                x: w / 2,
                y: (h / 2) - assets.tileSize
            }));

            container.addChild(progressText = new createJs.Text('0 %', 'bold ' + assets.tileSize + 'px subconscious_limboregular', '#fff').set({
                textAlign: 'center',
                snapToPixel: true,
                x: w / 2,
                y: (h / 2) - assets.tileSize
            }));


            return container;
        }

        function setProgress(progress) {
            progressText.text = progress + ' %';
            progressOutlineText.text = progress + ' %';
        }

        function setText(text) {
            progressText.text = text;
            progressOutlineText.text = text;
        }

        return {
            create: create,
            setProgress: setProgress,
            setText: setText
        };
    });