/*globals define */
define(['lib/create',
        'core/assets',
        'core/events',
        'ui/hud',
        'ui/menu'
    ],
    function (createJs, assets, events, hud, menu) {
        'use strict';

        var container,
            gameState,
            menuContainer,
            hudContainer;

        function create() {
            if (container) {destroy();}
            container = new createJs.Container();
            hudContainer = new createJs.Container();
            menuContainer = menu.create();

            //animate game state changes
            events.on('stateChanged', onStateChanged);
            return container;
        }

        function destroy() {
            hud.destroy();
            events.off(onStateChanged);
        }

        function onStateChanged(id, state) {
            if (state.newVal === assets.enums.gameStates.Playing) {
                container.removeChild(menuContainer);
                if (!hudContainer) {
                    hudContainer = hud.create();
                }
                hudContainer.set({
                    alpha: 0,
                    x: -assets.tileSize * 2
                });
                container.addChild(hudContainer);
                createJs.Tween.get(hudContainer, {
                    override: true
                }).to({
                    alpha: 1,
                    x: 0
                }, 200, createJs.Ease.easeOut);
            } else {
                container.removeChild(hudContainer);
                //hud.destroy();
                menuContainer.set({
                    alpha: 0,
                    y: -assets.tileSize * 2
                });
                container.addChild(menuContainer);
                createJs.Tween.get(menuContainer, {
                    override: true
                }).to({
                    alpha: 1,
                    y: 0
                }, 800, createJs.Ease.elasticOut);
            }
            gameState = state.newVal;
        }

        function update(event) {
            switch (gameState) {
                case assets.enums.gameStates.Menu:
                    hudContainer = null;
                /* fall through*/
                case assets.enums.gameStates.GameOver:
                case assets.enums.gameStates.Paused:
                    menu.update(event);
                    break;
                case assets.enums.gameStates.Instructions:
                case assets.enums.gameStates.Playing:
                    hud.update(event);
            }
        }

        return {
            create: create,
            update: update,
            destroy: destroy
        };
    }
);