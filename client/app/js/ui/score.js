/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events'
    ],
    function (createJs, assets, game, events) {
        'use strict';

        var container,
            parentContainer,
            scoreContainer,
            bonusContainer,
            bonusColors = ['57a', '68d', '79b', '8a9', '9b7', 'ab5', 'bc3', 'cd2', 'de1', 'ff0'],

            bonusLabel,
            bonusOutline,
            scoreLabel,
            scoreOutline;

        function create() {
            if (container) destroy();
            container = new createJs.Container();

            bonusContainer = new createJs.Container();
            bonusContainer.x = assets.gameWidth;
            bonusOutline = new createJs.Text('x' + game.bonus, 'bold ' + (assets.tileSize * 0.25) + 'px ' + assets.font('Dialog-Strong'), '#444').set({
                textAlign: 'right',
                x: -assets.tileSize * 0.35,
                y: assets.tileSize * 0.7
            });
            bonusLabel = new createJs.Text('x' + game.bonus, 'bold ' + (assets.tileSize * 0.25) + 'px ' + assets.font('Dialog'), '#' + bonusColors[0]).set({
                textAlign: 'right',
                x: -assets.tileSize * 0.325,
                y: assets.tileSize * 0.725
            });
            bonusContainer.addChild(bonusOutline);
            bonusContainer.addChild(bonusLabel);

            scoreContainer = new createJs.Container();
            scoreContainer.x = assets.gameWidth; //right align text left of 0, for easier scale animation

            scoreOutline = new createJs.Text(game.score, 'bold ' + (assets.tileSize * 0.5) + 'px ' + assets.font('Dialog-Strong'), '#000').set({
                textAlign: 'right',
                x: -assets.tileSize * 0.35,
                y: assets.tileSize * 0.2
            });
            scoreContainer.addChild(scoreOutline);

            scoreLabel = new createJs.Text(game.score, 'bold ' + (assets.tileSize * 0.5) + 'px ' + assets.font('Dialog'), '#fff').set({
                textAlign: 'right',
                x: -assets.tileSize * 0.3,
                y: assets.tileSize * 0.15
            });

            scoreContainer.addChild(scoreLabel);
            container.addChild(bonusContainer);
            container.addChild(scoreContainer);

            bonusContainer.cache(-assets.tileSize, assets.tileSize * 0.7, assets.tileSize, assets.tileSize * 0.5);
            scoreContainer.cache(-assets.gameWidth * 0.5, 0, assets.gameWidth * 0.5, assets.tileSize * 0.8);

            events.on('scoreAdded', onScoreAdded);
            return container;
        }

        function destroy() {
            events.off(onScoreAdded);
        }

        function onScoreAdded(id, data) {
            var score = data.value;
            var color = '#000';
            if (data.source.type && assets.defaults.critter[data.source.type]) {color = assets.defaults.critter[data.source.type].color;}
            if (data.source.type && assets.defaults.missile[data.source.type]) {color = assets.defaults.missile[data.source.type].color;}
            var fontSize = (assets.tileSize + score + 0.5 | 0);
            var scoreText = '' + (score + 0.5 | 0);
            var shape = new createJs.Container();
            var text;
            shape.addChild(new createJs.Text(scoreText, 'bold ' + fontSize + 'px ' + assets.font('Main'), createJs.Graphics.getRGB(color.r, color.g, color.b)).set({
                textAlign: 'left',
                outline: fontSize / 10
            }));
            shape.addChild(text = new createJs.Text(scoreText, 'bold ' + fontSize + 'px ' + assets.font('Main'), '#fff').set({
                textAlign: 'left'
            }));

            shape.cache(0, 0, text.getMeasuredWidth(), text.getMeasuredHeight());
            container.addChild(shape);

            //animate score
            var c = parentContainer.localToLocal(data.source.x !== undefined ? data.source.x : data.source.shape.x, data.source.y !== undefined ? data.source.y : data.source.shape.y, container); //rotating world location to fixed local
            shape.set({
                x: c.x,
                y: c.y,
                scaleX: 0.5,
                scaleY: 0.5,
                regX: fontSize / 2,
                regY: fontSize / 2
            });
            createJs.Tween.get(shape)
                .to({
                    x: c.x,
                    y: assets.gameHeight / 3,
                    scaleX: 1,
                    scaleY: 1
                }, 800, createJs.Ease.circOut)
                .to({
                    x: score > 0 ? assets.gameWidth : assets.gameWidth * game.level.stage.x,
                    y: score > 0 ? 0 : assets.gameHeight * game.level.stage.y,
                    scaleX: 0.3,
                    scaleY: 0.3,
                    alpha: 0.3
                }, 400, createJs.Ease.easeIn)
                .call(function animationDone() {
                    container.removeChild(shape);
                });
        }

        function update(event) {
            var roundedScore = Math.round(game.score);
            if (scoreLabel.text !== roundedScore) {
                scoreContainer.uncache();
                scoreLabel.color = game.score >= 0 ? '#fff' : '#e16a5e';
                scoreLabel.text = roundedScore;
                scoreOutline.color = game.score >= 0 ? '#000' : '#70352f';
                scoreOutline.text = roundedScore;
                scoreContainer.cache(-assets.gameWidth * 0.5, 0, assets.gameWidth * 0.5, assets.tileSize * 1.4);
                createJs.Tween.get(scoreContainer)
                    .to({
                        scaleX: 1.5,
                        scaleY: 1.5
                    }, 200, createJs.Ease.backIn)
                    .to({
                        scaleX: 1,
                        scaleY: 1
                    }, 100, createJs.Ease.circOut);
            }
            var roundedBonus = Math.round(game.bonus);
            if (bonusLabel.text !== 'x' + roundedBonus) {
                bonusContainer.uncache();
                bonusLabel.text = 'x' + roundedBonus;
                bonusLabel.color = '#' + bonusColors[roundedBonus - 1];
                bonusOutline.text = 'x' + roundedBonus;
                bonusContainer.cache(-assets.tileSize, assets.tileSize * 0.7, assets.tileSize, assets.tileSize * 0.5);
            }
        }

        function setParentContainer(container) {
            parentContainer = container;
        }

        return {
            create: create,
            update: update,
            setParentContainer: setParentContainer
        };
    });