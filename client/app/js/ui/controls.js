/*globals define */
define(['lib/create',
        'core/assets'
    ],
    function (createJs, assets) {
        'use strict';

        function createTitle(title, x, y, w, fontSize, color, align) {
            var t1, container = new createJs.Container();

            container.addChild(new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Main'), '#000').set({
                textAlign: align || 'left',
                snapToPixel: true,
                lineWidth: w,
                x: fontSize * -0.05,
                y: fontSize * 0.05
            }));
            container.addChild(t1 = new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Main'), color || '#FFF').set({
                textAlign: align || 'left',
                lineWidth: w,
                snapToPixel: true
            }));

            var alignX = align === 'center' ? w / 2 : align === 'right' ? w : 0;
            container.cache(0 - alignX, 0, w, t1.getMeasuredHeight() * 1.2);
            return container.set({
                x: x,
                y: y
            });
        }

        function createOutlinedTitle(title, x, y, w, fontSize, color, align) {
            var t1, container = new createJs.Container(),
                outlineW = fontSize * 0.15;

            container.addChild(new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Dialog-Strong'), '#000').set({
                textAlign: align || 'left',
                snapToPixel: true,
                outline: outlineW,
                x: outlineW
            }));

            container.addChild(t1 = new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Dialog-Strong'), color || '#FFF').set({
                textAlign: align || 'left',
                snapToPixel: true,
                x: outlineW
            }));
            w += outlineW * 2; //outline compensation
            var alignX = align === 'center' ? w / 2 : align === 'right' ? w : 0;
            container.cache(0 - alignX, 0, w, t1.getMeasuredHeight() * 1.1);
            return container.set({
                x: x,
                y: y
            });
        }

        function createText(title, x, y, w, fontSize) {
            var container = new createJs.Container();
            var t1 = new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Dialog'), '#fff').set({
                textAlign: 'center',
                lineWidth: w - fontSize, //- fontSize to create some padding
                snapToPixel: true,
                x: w * 0.5,
                y: fontSize / 2
            });

            container.addChild(t1);
            var height = t1.getMeasuredHeight() + fontSize * 3; //add vertical padding
            container.cache(-1, -1, w + 2, height + 2);
            return container.set({
                x: x,
                y: y
            });
        }

        function createDialog(title, x, y, w, fontSize) {
            var container = new createJs.Container();
            var t1 = new createJs.Text(title, 'bold ' + fontSize + 'px ' + assets.font('Dialog'), '#1a4e80').set({
                textAlign: 'center',
                lineWidth: w - fontSize, //- fontSize to create some padding
                snapToPixel: true,
                x: w * 0.5,
                y: fontSize / 2
            });
            var height = t1.getMeasuredHeight() + fontSize * 3; //add vertical padding
            container.addChild(new createJs.Shape(new createJs.Graphics().f(createJs.Graphics.getRGB(255, 255, 255, 0.5)).rr(0, 0, w, height, 10)));
            container.addChild(new createJs.Shape(new createJs.Graphics().s('#1a4e80').rr(0, 0, w, height, 10)));

            container.addChild(t1);
            var bw = w * 0.2;
            container.addChild(createTextButton('OK', w * 0.5 - fontSize * 2, t1.getMeasuredHeight() + fontSize, fontSize * 4, fontSize));

            container.cache(-1, -1, w + 2, height + 2);
            return container.set({
                x: x,
                y: y
            });
        }

        function createTextButton(title, x, y, w, h) {
            var container = new createJs.Container();

            container.addChild(new createJs.Shape(new createJs.Graphics().f(createJs.Graphics.getRGB(255, 255, 255, 1)).rr(0, 0, w, h, 5)));
            container.addChild(new createJs.Shape(new createJs.Graphics().s('#1a4e80').rr(0, 0, w, h, 5)));

            var u = new createJs.Text(title, (h * 0.7) + 'px ' + assets.font('Dialog-Strong'), '#1a4e80').set({
                x: w * 0.5,
                y: h * 0.1,
                textAlign: 'center'
            });

            container.addChild(u);
            container.cache(-1, -1, w + 2, h + 2);
            return container.set({
                x: x,
                y: y
            });
        }

        function createMenuIconButton(x, y, w, h) {
            var container = new createJs.Container();
            var shape1 = new createJs.Shape(new createJs.Graphics().f('#000')
                .dr(w * 0.075 | 0, h * 0.135, w * 0.755, h * 0.2)
                .dr(w * 0.075 | 0, h * 0.435, w * 0.755, h * 0.2)
                .dr(w * 0.075 | 0, h * 0.735, w * 0.755, h * 0.2));
            var shape2 = new createJs.Shape(new createJs.Graphics().f('#fff')
                .dr(w * 0.12, h * 0.12, w * 0.76, h * 0.16)
                .dr(w * 0.12, h * 0.42, w * 0.76, h * 0.16)
                .dr(w * 0.12, h * 0.72, w * 0.76, h * 0.16));

            var hit = new createJs.Shape();
            hit.graphics.f(createJs.Graphics.getRGB(0, 0, 0, 0.01)).dr(w * 0.05, h * 0.15, w * 0.8, h * 0.8); //hitarea
            container.addChild(hit);
            container.addChild(shape1);
            container.addChild(shape2);
            container.set({
                x: x,
                y: y
            });
            container.cache(-1, -1, w + 2, h + 2);
            return container;
        }

        return {
            createTitle: createTitle,
            createOutlinedTitle: createOutlinedTitle,
            createText: createText,
            createDialog: createDialog,
            createTextButton: createTextButton,
            createMenuIconButton: createMenuIconButton
        };

    }
);