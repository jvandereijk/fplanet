/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'models/player',
        'controllers/scene',
        'ui/controls'
    ],
    function (createJs, assets, game, events, player, scene, controls) {
        'use strict';

        var container,
            levelContainer,
            inGameContainer,
            gameOverContainer,
            gameOverReason,
            goalText,
            quitButton,
            resumeButton,
            retryButton1,
            retryButton2,
            menuButton,
            menuItems;

        function create() {
            if (container) destroy();
            container = new createJs.Container();

            //create transpparent overlay
            var bg = new createJs.Shape(new createJs.Graphics().f('#000').dr(-assets.tileSize * 2, -assets.tileSize * 2, assets.gameWidth + assets.tileSize * 4, assets.gameHeight + assets.tileSize * 4));
            bg.set({
                alpha: 0.2
            });
            container.addChild(bg);

            levelContainer = new createJs.Container();
            inGameContainer = new createJs.Container().set({
                visible: false
            });
            gameOverContainer = new createJs.Container().set({
                visible: false
            });

            var startX = assets.gameWidth / 2 - assets.tileSize * 3.3 * 1.5; //center buttons on screen
            var levels, i, j, x, y, menuItem, modeY = 1.5;

            menuItems = [];

            for (i = 0; i < assets.data('GameModes').length; i++) {
                levels = assets.levels(i);

                levelContainer.addChild(controls.createTitle(assets.text('GameMode-' + assets.data('GameModes')[i].id), startX + assets.tileSize * 1.5, assets.tileSize * modeY * 1.5, assets.gameWidth, assets.tileSize * 0.5));

                for (j = 0; j < levels.length; j++) {
                    x = 1 + j % 5;
                    y = 0.5 + modeY + (j / 5 | 0);

                    menuItem = controls.createTextButton(parseInt(levels[j].id).toString(), startX + assets.tileSize * x * 1.5, assets.tileSize * y * 1.5, assets.tileSize, assets.tileSize * 0.85);
                    menuItem.mode = i;
                    menuItem.lvl = levels[j].id;
                    menuItem.addEventListener('click', onLevelClick);
                    menuItems.push(menuItem);
                    levelContainer.addChild(menuItem);
                }
                modeY = y + 1.5;
            }


            //resume button
            inGameContainer.addChild(resumeButton = controls.createTextButton(assets.text('Continue'), assets.gameWidth / 3, assets.tileSize * 6.5, assets.gameWidth / 3, assets.tileSize));
            resumeButton.addEventListener('click', onResume);
            //retry button
            inGameContainer.addChild(retryButton1 = controls.createTextButton(assets.text('Retry'), assets.gameWidth / 3, assets.tileSize * 8, assets.gameWidth / 3, assets.tileSize));
            retryButton1.addEventListener('click', onRetryLevel);
            //quit to menu button
            inGameContainer.addChild(quitButton = controls.createTextButton(assets.text('Quit'), assets.gameWidth / 3, assets.tileSize * 9.5, assets.gameWidth / 3, assets.tileSize));
            quitButton.addEventListener('click', onQuitLevel);


            gameOverContainer.addChild(controls.createTitle('gameOver', assets.gameWidth * 0.5, assets.gameHeight * 0.25, assets.gameWidth - assets.tileSize * 2, assets.tileSize, '#FA5', 'center'));
            //retry button
            gameOverContainer.addChild(retryButton2 = controls.createTextButton(assets.text('Retry'), assets.gameWidth / 3, assets.tileSize * 8, assets.gameWidth / 3, assets.tileSize));
            retryButton2.addEventListener('click', onRetryLevel);
            //quit to menu button (gameoveR)
            gameOverContainer.addChild(menuButton = controls.createTextButton(assets.text('Menu'), assets.gameWidth / 3, assets.tileSize * 9.5, assets.gameWidth / 3, assets.tileSize));
            menuButton.addEventListener('click', onQuitLevel);


            container.addChild(inGameContainer);
            container.addChild(levelContainer);
            container.addChild(gameOverContainer);

            events.on('stateChanged', onStateChanged);
            events.on('goalStarted', onGoalStarted);
            events.on('levelStarted', onLevelStarted);
            events.on('goalCompleted', onGoalCompleted);
            events.on('gameOver', onGameOver);
            return container;
        }

        function destroy() {
            quitButton.removeEventListener('click', onQuitLevel);
            resumeButton.removeEventListener('click', onResume);
            menuButton.removeEventListener('click', onQuitLevel);
            retryButton1.removeEventListener('click', onRetryLevel);
            retryButton2.removeEventListener('click', onRetryLevel);
            events.off(onStateChanged);
            events.off(onGoalStarted);
            events.off(onLevelStarted);
            events.off(onGoalCompleted);
            events.off(onGameOver);

            while (menuItems.length) {
                var item = menuItems.pop();
                item.removeEventListener('click', onLevelClick);
                item = null;
            }
        }

        function onGoalStarted(id, goal) {
            if (goalText) {
                inGameContainer.removeChild(goalText);
            }
            if (goal.text.start[player.language]) {
                inGameContainer.addChild(goalText = controls.createText(assets.text('Mission:') + ' ' + goal.text.start[player.language], assets.tileSize * 2, assets.tileSize * 3, assets.gameWidth - assets.tileSize * 4, assets.tileSize * 0.5));
            }
        }

        function onLevelStarted() {
            inGameContainer.removeChild(goalText);
            goalText = null;
        }

        function onGoalCompleted(/*id, goal*/) {
            if (goalText) {
                inGameContainer.removeChild(goalText);
            }
        }

        function onGameOver(id, reason) {
            gameOverContainer.removeChild(gameOverReason);
            if (reason) {
                var text = assets.text('GameOver-' + reason);
                if (reason === 'goal-failed') {
                    text += ' ' + game.goals.current.text.start[player.language];
                }
                gameOverContainer.addChild(gameOverReason = controls.createText(text, assets.tileSize * 2, assets.gameHeight * 0.32, assets.gameWidth - assets.tileSize * 4, assets.tileSize * 0.5));
            }
        }

        function onRetryLevel() {
            scene.initLevel(game.level.id);
        }

        function onQuitLevel() {
            scene.quit();
        }

        function onLevelClick(evt) {
            game.mode = evt.target.parent.mode;
            //console.log('load level' + evt.target.parent.lvl + ' mode:' + game.mode);
            scene.initLevel(evt.target.parent.lvl);
        }

        function onResume() {
            scene.resume();
        }

        function onStateChanged(id, state) {
            switch (state.newVal) {
                case assets.enums.gameStates.GameOver:
                    gameOverContainer.visible = true;
                    inGameContainer.visible = false;
                    levelContainer.visible = false;
                    break;
                default:
                    if (scene.isStarted) {
                        gameOverContainer.visible = false;
                        inGameContainer.visible = true;
                        levelContainer.visible = false;
                    } else {
                        gameOverContainer.visible = false;
                        inGameContainer.visible = false;
                        levelContainer.visible = true;
                    }
            }
        }

        function update(/*event*/) {
        }

        return {
            create: create,
            update: update,
            destroy: destroy
        };
    });