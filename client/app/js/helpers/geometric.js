/*globals define */
define(
    function () {
        'use strict';

        function getRectangleFromAngles(startAngle, endAngle, padding, distance) {
            //get rectangular dimensions for caching
            var sx = distance * Math.cos(startAngle);
            var sy = distance * Math.sin(startAngle);
            var ex = distance * Math.cos(endAngle);
            var ey = distance * Math.sin(endAngle);

            return {
                x: (sx < ex ? sx : ex) - padding,
                y: (sy < ey ? sy : ey) - padding,
                w: (sx < ex ? ex - sx : sx - ex) + padding * 2,
                h: (sy < ey ? ey - sy : sy - ey) + padding * 2
            };
        }

        function isInsideCircle(x, y, r, centerX, centerY) {
            if (centerX === undefined) centerX = 0;
            if (centerY === undefined) centerY = 0;
            return Math.pow(x - centerX, 2) + Math.pow(y - centerY, 2) < Math.pow(r, 2);
        }

        return {
            getRectangleFromAngles: getRectangleFromAngles,
            isInsideCircle: isInsideCircle
        };


    });