/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'helpers/anim'
    ],
    function (createJs, assets, game, anim) {
        'use strict';


        function create(shapeEvent, strokeWeight, color, callback) {
            var shape;

            switch (shapeEvent.type) {
                case 'ring':
                    shape = ring(shapeEvent, strokeWeight, color);
                    anim.zoom(shape, 2.5, 500, null, true);
                    anim.fade(shape, 0, 500, null, true).call(function () {
                        if (callback) callback(shapeEvent, shape);
                    });
                    break;
                case 'stack':
                    shape = stack(shapeEvent, strokeWeight, color);
                    if (game.level.stage.dragLocked) {
                        anim.zoom(shape, 2.5, 500, null, true);
                        anim.fade(shape, 0, 500, null, true).call(function () {
                            if (callback) callback(shapeEvent, shape);
                        });
                    } else {
                        anim.zoom(shape, 0.5, 1000, createJs.Ease.circOut, true).call(function () {
                            if (callback) callback(shapeEvent, shape);
                        });
                    }

                    break;
                case 'triangle':
                    shape = triangle(shapeEvent, strokeWeight, color);
                    anim.zoom(shape, 2.5, 500, null, true);
                    anim.fade(shape, 0, 500, null, true).call(function () {
                        if (callback) callback(shapeEvent, shape);
                    });
                    break;
                case 'point':
                    shape = point(shapeEvent, strokeWeight, color);
                    anim.zoom(shape, 2.5, 300, null, true);
                    anim.fade(shape, 0, 300, null, true).call(function () {
                        if (callback) callback(shapeEvent, shape);
                    });
                    break;
            }
            return shape;
        }

        function ring(shapeEvent, strokeWeight, color) {
            var shape = new createJs.Shape(),
                distance = assets.tileSize * (shapeEvent.nodeData.layer + 1);
            shape.graphics.setStrokeStyle(strokeWeight, 0, 1).beginStroke(color);
            shape.graphics.arc(0, 0, distance, 0, Math.PI * 2, false);
            var w = distance + strokeWeight * 4;
            shape.cache(-w, -w, w * 2, w * 2);
            return shape;
        }

        function stack(shapeEvent, strokeWeight, color) {
            var shape = new createJs.Shape(),
                tx = shapeEvent.parts[shapeEvent.parts.length - 1].x - shapeEvent.parts[0].x,
                ty = shapeEvent.parts[shapeEvent.parts.length - 1].y - shapeEvent.parts[0].y;
            shape.graphics.setStrokeStyle(strokeWeight, 0, 1).beginStroke(color);
            shape.graphics.moveTo(0, 0);
            shape.graphics.lineTo(tx, ty).endStroke();
            shape.cache((tx < 0 ? tx : 0) - strokeWeight * 2, (ty < 0 ? ty : 0) - strokeWeight * 2, Math.abs(tx) + strokeWeight * 4, Math.abs(ty) + strokeWeight * 4);

            shape.x = shapeEvent.parts[0].x;
            shape.y = shapeEvent.parts[0].y;
            return shape;
        }

        function triangle(shapeEvent, strokeWeight, color) {
            var shape = new createJs.Shape(),
                rect = {
                    x: shapeEvent.nodeData.x,
                    y: shapeEvent.nodeData.y,
                    x2: shapeEvent.nodeData.x,
                    y2: shapeEvent.nodeData.y
                };

            if (shapeEvent.parts[0].x < rect.x) rect.x = shapeEvent.parts[0].x;
            if (shapeEvent.parts[0].y < rect.y) rect.y = shapeEvent.parts[0].y;
            if (shapeEvent.parts[shapeEvent.parts.length - 1].x < rect.x) rect.x = shapeEvent.parts[shapeEvent.parts.length - 1].x;
            if (shapeEvent.parts[shapeEvent.parts.length - 1].y < rect.y) rect.y = shapeEvent.parts[shapeEvent.parts.length - 1].y;
            if (shapeEvent.parts[0].x > rect.x2) rect.x2 = shapeEvent.parts[0].x;
            if (shapeEvent.parts[0].y > rect.y2) rect.y2 = shapeEvent.parts[0].y;
            if (shapeEvent.parts[shapeEvent.parts.length - 1].x > rect.x2) rect.x2 = shapeEvent.parts[shapeEvent.parts.length - 1].x;
            if (shapeEvent.parts[shapeEvent.parts.length - 1].y > rect.y2) rect.y2 = shapeEvent.parts[shapeEvent.parts.length - 1].y;

            shape.graphics.setStrokeStyle(strokeWeight, 0, 1).beginStroke(color);
            shape.graphics.moveTo(shapeEvent.nodeData.x, shapeEvent.nodeData.y);
            shape.graphics.lineTo(shapeEvent.parts[0].x, shapeEvent.parts[0].y);
            shape.graphics.lineTo(shapeEvent.parts[shapeEvent.parts.length - 1].x, shapeEvent.parts[shapeEvent.parts.length - 1].y);
            shape.graphics.lineTo(shapeEvent.nodeData.x, shapeEvent.nodeData.y);
            shape.graphics.endStroke();

            shape.cache(rect.x - strokeWeight * 2, rect.y - strokeWeight * 2, rect.x2 - rect.x + strokeWeight * 4, rect.y2 - rect.y + strokeWeight * 4);

            return shape;
        }

        function point(shapeEvent, strokeWeight, color) {
            var shape = new createJs.Shape(),
                distance = assets.tileSize * 0.05;
            shape.graphics.setStrokeStyle(strokeWeight, 0, 1).beginStroke(color);
            shape.graphics.drawCircle(shapeEvent.nodeData.x, shapeEvent.nodeData.y, distance);
            var w = distance + strokeWeight * 4;
            shape.cache(shapeEvent.nodeData.x - w, shapeEvent.nodeData.y - w, w * 2, w * 2);
            return shape;
        }

        return {
            create: create
        };

    });