/*globals define */
define(['lib/create'
    ],
    function (createJs) {
        'use strict';

        var Grayscale = new createJs.ColorMatrixFilter([
            0.30, 0.30, 0.30, 0, 0, // red component
            0.30, 0.30, 0.30, 0, 0, // green component
            0.30, 0.30, 0.30, 0, 0, // blue component
            0, 0, 0, 1, 0 // alpha
        ]);


        var Sepia = new createJs.ColorMatrixFilter([
            0.39, 0.77, 0.19, 0, 0, // red component
            0.35, 0.68, 0.17, 0, 0, // green component
            0.27, 0.53, 0.13, 0, 0, // blue component
            0, 0, 0, 1, 0 // alpha
        ]);


        function componentToHex(c) {
            var hex = c.toString(16);
            return hex.length === 1 ? '0' + hex : hex;
        }

        function rgbToHex(r, g, b) {
            return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
        }

        function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        }

        return {
            Grayscale: Grayscale,
            Sepia: Sepia,
            rgbToHex: rgbToHex,
            hexToRgb: hexToRgb
        };
    });