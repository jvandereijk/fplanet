/*globals define */
define([
        'models/graph/circularGraph'
    ],
    function (circularGraph) {
        'use strict';

        function checkEncapsulatedBy(node) {
            var i,
                tmpNode,
                outerRightContent,
                outerLeftContent,
                rightNode,
                leftNode;

            //find first 'other' type on right
            for (i = 1; i <= node.ringSize; i++) {
                tmpNode = circularGraph.getNode(node.layer + '_' + (node.cell + i).mod(node.ringSize));
                if (tmpNode.content === null) return false;
                if (tmpNode.content.type !== node.content.type) {
                    outerRightContent = tmpNode.content;
                    break;
                } else {
                    if (!rightNode) rightNode = tmpNode;
                }
            }

            //find first 'other' type on left
            for (i = node.ringSize; i >= 0; i--) { //check left
                tmpNode = circularGraph.getNode(node.layer + '_' + (node.cell + i).mod(node.ringSize));
                if (tmpNode.content === null) return false;
                if (tmpNode.content.type !== node.content.type) {
                    outerLeftContent = tmpNode.content;
                    break;
                } else {
                    if (!leftNode) leftNode = tmpNode;
                }
            }

            //if left and right are same type, convert this node plus it's 2 neighbours
            if (outerLeftContent && outerLeftContent.type === outerRightContent.type) {

                circularGraph.addNodeAction('change', node, {
                    type: outerRightContent.type,
                    value: outerRightContent.value
                });
                if (rightNode) {
                    circularGraph.addNodeAction('change', rightNode, {
                        type: outerRightContent.type,
                        value: outerRightContent.value
                    });
                }
                if (leftNode) {
                    circularGraph.addNodeAction('change', leftNode, {
                        type: outerRightContent.type,
                        value: outerRightContent.value
                    });
                }
                return true;
            }
            return false;
        }

        function findStack(node) {
            var stack1 = findStackWithAngle(node, 'parents').sort(), //default sorts works as long as total layers<10
                filtered1 = stack1.filter(removeDuplicates),
                stack2 = findStackWithAngle(node, 'children').sort(),
                filtered2 = stack2.filter(removeDuplicates);
            return filtered1.length >= filtered2.length ? filtered1 : filtered2;
        }

        function removeDuplicates(elem, pos, self) {
            return self.indexOf(elem) === pos;
        }

        function findStackWithAngle(node, subType, direction, angle) { //subType is 'parents' or 'children'
            if (!node) {
                return [];
            }
            var i, sub, path = [],
                subPaths = [];

            if (node.content && !node.locked) {
                path.push(node.id());
            } else {
                return [];
            }

            for (i = 0; i < node[subType].length; i++) {
                subPaths.push([]);
                sub = circularGraph.getNode(node[subType][i]);
                if (sub.content && !sub.locked) {
                    var sAngle = sub.radians;
                    var sDirection = node.radians - sub.radians;
                    if (angle === undefined) {
                        subPaths[i] = findStackWithAngle(sub, subType, sDirection, sAngle);
                    } else {
                        if (direction * sDirection > 0 || direction === sDirection) { //check if direction is the same (both negative or both positive, or same (0))
                            if (sDirection - direction < 0.1) {
                                subPaths[i] = findStackWithAngle(sub, subType, sDirection, sAngle);
                            }
                        }
                    }
                }

            }

            if (node[subType].length === 0) return path;

            //find longest sub path
            var p = 0;
            for (i = 0; i < node[subType].length; i++) {
                if (subPaths[i].length >= subPaths[p].length) p = i;
            }

            return path.concat(subPaths[p]);
        }

        //returns all children recursively with same type, an exception is made for nodes with 4 children, if the center 2 are of same type, thats ok too
        function allChildrenWithSameType(node) {
            var i, ids = [],
                children = [],
                pattern, count;
            if (node.content) {
                pattern = [];
                count = 0;
                for (i = 0; i < node.children.length; i++) {
                    var child = circularGraph.getNode(node.children[i]);
                    if (!child.locked && child.content && child.content !== 0 && child.content.type === node.content.type) {
                        ids.push(child.id());
                        children = children.concat(allChildrenWithSameType(child));
                        pattern.push([child.cell, 1]);
                        count++;
                    } else {
                        pattern.push([child.cell, 0]);
                    }


                }
            }
            //return empty array if test fails, but check for exemption first
            if (count < node.children.length) {
                if (count >= 2 && pattern.length === 4) {
                    for (i = 1; i < 4; i++) {
                        if (pattern[i][0] - 6 > pattern[i - 1][0]) { //example: shift cell 2_0 to top of pattern, since it should be 'higher' than 2_15, 1_16 and 2_17 on the circular grid
                            pattern.push(pattern.shift(0));
                        }
                    }
                    if (pattern[1][1] !== 1 || pattern[2][1] !== 1) { //check if at least middle 2 nodes are requested type
                        return [];
                    }
                } else return []; //has otherTypes, so return nothing
            }

            //return valid child id's
            return ids.concat(children);

        }

        function getLayerNodesByType(layer, type) {
            var cellCount = circularGraph.cellCount(layer);
            var i, node, nodes = [];
            if (layer > 0) {
                for (i = 0; i < cellCount; i++) {
                    node = circularGraph.getNode(layer + '_' + i);
                    if (node.content && node.content.type === type) {
                        nodes.push(node);
                    }
                }
            }
            return nodes;
        }


        return {
            checkEncapsulatedBy: checkEncapsulatedBy,
            findStack: findStack,
            allChildrenWithSameType: allChildrenWithSameType,
            getLayerNodesByType: getLayerNodesByType
        };


    }
);