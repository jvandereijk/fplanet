/*globals define */
define(['lib/create'
    ],
    function (createJs) {
        'use strict';

        function zoom(obj, scale, duration, ease, dontOverride) {
            return createJs.Tween.get(obj, {
                override: dontOverride !== true
            }).to({
                scaleX: scale,
                scaleY: scale
            }, duration, ease);
        }

        function fade(obj, alpha, duration, ease, dontOverride) {
            return createJs.Tween.get(obj, {
                override: dontOverride !== true
            }).to({
                alpha: alpha
            }, duration, ease);
        }

        function translate(obj, x, y, duration, ease, dontOverride) {
            return createJs.Tween.get(obj, {
                override: dontOverride !== true
            }).to({
                x: x,
                y: y
            }, duration, ease);
        }

        function shake(obj, duration, force, ease) {
            return zoom(obj, 1 + 0.3 * force, duration, ease).call(function () {
                zoom(obj, 1 - 0.15 * force, duration, ease).call(function () {
                    zoom(obj, 1 + 0.2 * force, duration, ease).call(function () {
                        zoom(obj, 1 - 0.1 * force, duration, ease).call(function () {
                            zoom(obj, 1 + 0.1 * force, duration, ease).call(function () {
                                zoom(obj, 1 - 0.05 * force, duration, ease).call(function () {
                                    zoom(obj, 1, duration, ease);
                                });
                            });
                        });
                    });
                });
            });
        }

        function explode(obj, duration, delay) {
            delay = delay || 0;
            duration = duration || 500;
            var t = duration / 17; //animpart duration multiplier

            return createJs.Tween.get(obj).wait(delay).to({
                scaleX: 0.1,
                scaleY: 0.1
            }, t, createJs.Ease.backIn)
                .to({
                    scaleX: 0.5,
                    scaleY: 0.5
                }, t * 3, createJs.Ease.backIn)
                .to({
                    scaleX: 0.2,
                    scaleY: 0.2
                }, t * 3, createJs.Ease.backIn)
                .to({
                    scaleX: 0.6,
                    scaleY: 0.6
                }, t * 3, createJs.Ease.backIn)
                .to({
                    scaleX: 0.3,
                    scaleY: 0.3
                }, t * 3, createJs.Ease.backIn)
                .to({
                    scaleX: 10,
                    scaleY: 10
                }, t * 4, createJs.Ease.backIn);
        }

        return {
            zoom: zoom,
            fade: fade,
            translate: translate,
            shake: shake,
            explode: explode
        };
    });