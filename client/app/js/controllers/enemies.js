/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'controllers/outerSpace',
        'models/goalProgress',
        'actors/enemyShip',
        'actors/enemyMissile',
        'actors/antiMissile',
        'actors/shields',
        'helpers/anim'
    ],
    function (createJs, assets, game, events, outerSpace, GoalProgress, EnemyShip, enemyMissile, antiMissile, shields, anim) {
        'use strict';

        var container,
            enemyConfig,
            enemies,
            sinceWaveStart,
            currentWave,
            missiles,
            antiMissiles,
            noMoreWaves,
            zoomedOut;

        function create() {
            if (container) destroy();
            events.on('pointerTap', onPointerTap);
            events.on('stateChanged', onStateChanged);
            events.on('levelExit', destroy);
            events.on('launchMissile', launchMissile);
            container = new createJs.Container();
            missiles = [];
            antiMissiles = [];
            noMoreWaves = false;
            currentWave = -1;
            enemies = [];
            enemyConfig = null;
            zoomedOut = false;
            if (game.level.enemy) {
                enemyConfig = JSON.parse(JSON.stringify(game.level.enemy));
                /*clone and store waves data */
                prepareNextWave();
            }

            return container;
        }

        function destroy() {
            events.off(onPointerTap);
            events.off(onStateChanged);
            events.off(destroy);
            events.off(launchMissile);
            pause(); //stop all enemies and missiles
        }

        function onStateChanged(msg, state) {
            if (state.newVal !== assets.enums.gameStates.Playing) {
                pause();
            }
            if (state.newVal === assets.enums.gameStates.Playing) {
                resume();
            }
        }

        function zoom(amount) {
            if (!enemyConfig) return;
            var i, scale = amount < 0 ? 0.75 : 1;
            zoomedOut = scale === 0.75;
            for (i = enemies.length - 1; i >= 0; i--) {
                createJs.Tween.get(enemies[i].transform, {
                    override: true
                }).to({
                    scaleX: scale * enemies[i].baseScaleX,
                    scaleY: scale * enemies[i].baseScaleY
                }, 300, createJs.Ease.backOut);
            }
        }

        function prepareNextWave() {
            var i, waveEnemies;
            currentWave++;
            enemies = []; //todo: cleanup enemies/missiles
            sinceWaveStart = 0;
            if (currentWave < enemyConfig.items.length) {
                waveEnemies = enemyConfig.items[currentWave].items;
                for (i = 0; i < waveEnemies.length; i++) {
                    enemies.push(new EnemyShip(waveEnemies[i]));
                }
            } else if (enemyConfig.loop) {
                currentWave = -1;
                enemyConfig = JSON.parse(JSON.stringify(game.level.enemy));
                /* reset wave data */
                prepareNextWave();
            } else {
                noMoreWaves = true;
            }
        }

        function isWaveFinished() {
            for (var i = enemies.length - 1; i >= 0; i--) {
                if (!enemies[i].done) return false;
            }
            return true;
        }

        function update(event) {
            if (!enemyConfig || noMoreWaves) return;
            if (isWaveFinished()) prepareNextWave();
            var i, j, missile, oldState, x, y, enemy, antiMissile, damage;

            sinceWaveStart += event.delta;

            for (i = enemies.length - 1; i >= 0; i--) {
                enemy = enemies[i];

                if (!enemy.done) {

                    if (!enemy.active && sinceWaveStart > enemyConfig.items[currentWave].delay + enemy.config.delay) {
                        container.addChild(enemy.shape);
                        enemy.active = true;
                        enemy.show();
                    }
                    if (enemy.active && sinceWaveStart > enemyConfig.items[currentWave].delay + enemy.config.duration) {
                        removeEnemy(enemy);
                    }
                }

                if (enemy.active && !enemy.done) {
                    enemy.update(event);
                }

                for (j = antiMissiles.length - 1; j >= 0; j--) {
                    antiMissile = antiMissiles[j];
                    antiMissile.update(event, enemy);

                    x = antiMissile.shape.x;
                    y = antiMissile.shape.y;

                    if (enemy.visible && enemy.hitTest(x, y)) { //enemy is hit by defensive missile
                        createExplosion(x, y, 1);
                        container.removeChild(antiMissile.shape);
                        antiMissiles.splice(j, 1);

                        enemy.health -= 10;
                        if (enemy.health <= 0) {
                            game.goals.progress(new GoalProgress('enemy-killed', 1));
                            game.addScore(enemy.maxHealth * 0.2, enemy.shape);
                            removeEnemy(enemy);


                            break;
                        }

                    } else if (antiMissile.timeAlive <= 0) {
                        createExplosion(x, y, 0.5);
                        container.removeChild(antiMissile.shape);
                        antiMissiles.splice(j, 1);
                    }
                }
            }

            for (j = missiles.length - 1; j >= 0; j--) {
                missile = missiles[j];

                oldState = missile.state;
                missile.update(event);

                if (missile.state !== oldState) {

                    switch (missile.state) {
                        case enemyMissile.states.launch:
                            //console.log('launch missile ' + missile.id);
                            if (missile.enemy) {
                                outerSpace.addMissile(missile);
                                missile.launch(missile.enemy.transform.x, missile.enemy.transform.y + missile.enemy.height * 0.5);
                            } else {
                                missiles.splice(j, 1);
                                missile = null;
                            }
                            break;
                        case enemyMissile.states.impact: //destroyed by impact
                            game.planetHealth -= missile.damage * 10; //todo: * 10 should be configurable;
                            missile.explode();
                            events.trigger('cameraShake', 1); //normal shake
                            break;
                        case enemyMissile.states.destroyed:
                            //console.log('destroy missile ' + missile.id);
                            outerSpace.removeMissile(missile);
                            missiles.splice(j, 1);
                            createJs.Tween.removeTweens(missile);
                            missile = null;
                            break;
                    }
                } else if (missile.state === enemyMissile.states.launch && missile.tween) {
                    if (missile.tween.position > missile.tween.duration * 0.5) { //check shields impact
                        damage = missile.damage;
                        if (shields.checkMissileImpact(missile)) {
                            missile.explode();
                            game.addScore(missile.strength, missile);
                            missile.tween.setPaused(true);
                        }
                        if (missile.damage != damage) {
                            missile.addExplosion(0.75);
                        }
                    }
                }
            }
        }

        function launchMissile(id, missile) {
            missiles.push(missile);
        }

        function removeEnemy(enemy) {
            enemy.done = true;
            enemy.hide().call(function () {
                container.removeChild(enemy.shape);
            });

        }

         //add ground to air missile top enemy space
        function addAntiMissile(src, dest, sourceContainer, force) {
            var missile;
            src = sourceContainer.localToLocal(src.x, src.y, container);
            dest = sourceContainer.localToLocal(dest.x, dest.y, container);
            missile = antiMissile.create(src, dest, force);
            container.addChild(missile.shape);
            antiMissiles.push(missile);
        }

        function createExplosion(x, y, scale) { //add exploding animation
            var explosion,
                sheet = new createJs.SpriteSheet({
                    images: [assets.get('explosion')],
                    frames: {
                        width: 128,
                        height: 128,
                        regX: 64,
                        regY: 64,
                        count: 6
                    },
                    framerate: 20
                });
            scale = (scale === undefined ? 1 : scale) * assets.tileSize / 128;
            explosion = new createJs.Sprite(sheet);
            explosion.set({
                scaleX: scale,
                scaleY: scale,
                x: x,
                y: y
            });
            explosion.gotoAndPlay(1);
            container.addChild(explosion);
            setTimeout(function () {
                container.removeChild(explosion);
                explosion = null;
            }, 1000);
        }

        function pause() {
            for (var i = missiles.length - 1; i >= 0; i--) {
                if (missiles[i].tween) missiles[i].tween.setPaused(true); //pause missile tween
                if (missiles[i].shape && missiles[i].shape.children.length > 0) missiles[i].shape.children[0].paused = true; //pause explosion animation
            }
        }

        function resume() {
            for (var i = missiles.length - 1; i >= 0; i--) {
                if (missiles[i].tween) missiles[i].tween.setPaused(false); //resume missile tween
                if (missiles[i].shape && missiles[i].shape.children.length > 0) missiles[i].shape.children[0].paused = false; //resume explosion animation
            }
        }

        function onPointerTap(id, event) {
            //if (zoomedOut) {//can only tapped when zoomed out
            for (var i = missiles.length - 1; i >= 0; i--) { //check all touches
                for (var j = 0; j < event.gesture.touches.length; j++) {
                    var c = outerSpace.container.globalToLocal(event.gesture.touches[j].pageX, event.gesture.touches[j].pageY);
                    if (missileHitTest(missiles[i].shape, c.x, c.y)) {
                        missiles[i].damage -= zoomedOut ? 0.25 : 0.1; //more damage when zoomed out
                        if (missiles[i].damage > 0) {
                            missiles[i].addExplosion(0.5);
                        } else {
                            missiles[i].explode();
                            game.addScore(-1, missiles[i]);
                        }
                    }
                }
            }
            //}
        }

        function missileHitTest(shape, x, y) {
            var target = {
                x: shape.x - assets.tileSize * 0.5,
                y: shape.y - assets.tileSize * 0.5
            };
            return x > target.x && y < target.y + assets.tileSize && x < target.x + assets.tileSize && y > target.y;
        }


        return {
            create: create,
            addAntiMissile: addAntiMissile,
            zoom: zoom,
            update: update
        };
    });