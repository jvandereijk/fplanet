/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'models/graph/circularGraph',
        'models/goalProgress',
        'actors/shields',
        'actors/critter',
        'helpers/shapeDetection',
        'helpers/anim'
    ],
    function (createJs, assets, game, events, circularGraph, GoalProgress, shields, critter, shapeDetection, anim) {
        'use strict';
        var container, radius, stageConfig;

        function create(config) {
            stageConfig = config;
            container = new createJs.Container();
            radius = stageConfig.planet.r * assets.tileSize;
            return container;
        }

        function fillNode(object, layer, cell, duration) {
            var node = circularGraph.setNode(layer, cell, {
                type: object.type,
                shape: null,
                value: object.value,
                behavior: object.behavior
            });
            if (node) { //create shape
                node.content.shape = critter.create(
                    object.type,
                    node.radians,
                        radius + assets.tileSize * (node.layer - 1),
                    object.shape, duration);
                container.addChild(node.content.shape);
                if (circularGraph.layerIsType(layer)) {
                    emptyLayer(layer, cell, true);
                }
                object.behavior.onHit(node, true); // isOrigin=true;
                return node;
            } else {
                //console.log('cell already taken');
                return false;
            }
        }

        function emptyNode(node) {
            container.removeChild(node.content.shape);
            node.content.shape = null;
            circularGraph.emptyNode(node.layer, node.cell);
        }

        function emptyLayer(layer, cell, emptyEqualParents) {
            var i, data, equalParents, cellCount = circularGraph.cellCount(layer);

            //remove layer lock
            var lock = circularGraph.getLayerLock(layer);
            var triggeringNode = lock && lock.data ? lock.data.triggeringNode : circularGraph.getNode(layer, cell);
            circularGraph.removeLayerLock(layer, 'change');
            equalParents = emptyEqualParents ? shapeDetection.getLayerNodesByType(layer - 1, triggeringNode.content.type) : [];

            //trigger event
            events.triggerShapeDetected('ring', triggeringNode.data());

            //empty layer
            for (i = 1; i <= cellCount; i++) {
                var c = cell + i >= cellCount ? cell + i - cellCount : cell + i;
                if (i === cellCount) {
                    data = new events.ShapeEvent('removed', 'ring', triggeringNode.data(), new Array(cellCount + equalParents.length)); //todo: send real parts instead of empty array
                }
                circularGraph.addNodeAction('empty', circularGraph.getNode(layer, c), data);
            }
            for (i = 0; i < equalParents.length; i++) {
                circularGraph.addNodeAction('empty', equalParents[i], null);
            }
        }


        function handleDestroy(onSucces) {
            var nodeAction = circularGraph.nextNodeAction('destroy');
            if (nodeAction !== null) {
                var node = circularGraph.getNode(nodeAction.id);
                if (nodeAction.data.blowUpNeighbours) { //blowup parents/ neighbours ?
                    var delay = nodeAction.data.delay + 250;
                    var lNode = circularGraph.getNode(node.layer, (node.cell - 1).mod(node.ringSize)); //left
                    if (lNode.layer > 0 && lNode.content !== null) {
                        circularGraph.addNodeAction('destroy', lNode, {
                            blowUpNeighbours: false,
                            delay: delay
                        });
                        delay += 250;
                    }
                    var rNode = circularGraph.getNode(node.layer, (node.cell + 1).mod(node.ringSize)); //right
                    if (rNode.layer > 0 && rNode.content !== null) {
                        circularGraph.addNodeAction('destroy', rNode, {
                            blowUpNeighbours: false,
                            delay: delay
                        });
                        delay += 250;
                    }
                    for (var i = 0; i < node.parents.length; i++) {
                        var parentNode = circularGraph.getNode(node.parents[i]);
                        if (parentNode.layer > 0 && parentNode.content !== null) {
                            circularGraph.addNodeAction('destroy', parentNode, {
                                blowUpNeighbours: false,
                                delay: delay
                            });
                            delay += 250;
                        }
                    }
                }
                destroyNode(nodeAction);
            }
        }

        function destroyNode(nodeAction) {
            var node = circularGraph.getNode(nodeAction.id);
            if (node.content && node.content.shape) {
                node.content.shape.uncache(); //restore black color
                anim.explode(node.content.shape, 510, nodeAction.data.delay).call(function shapeDestroyed() {
                    node.locked = false; //release destroy lock
                    //negative score when blown to pieces
                    game.addScore(-node.content.value, node.data());
                    emptyNode(node);
                });
            }
        }

        function handleChangeType(onSucces) {
            var nodeAction = circularGraph.nextNodeAction('change'),
                node, shape, scale;
            if (nodeAction !== null) {
                node = circularGraph.getNode(nodeAction.id);
                if (node.content !== null) { // check if not already altered
                    node.content.type = nodeAction.data.type;
                    node.content.value = nodeAction.data.value;
                    node.content.behavior = require('behaviors/behaviors').get(assets.defaults.critter[node.content.type].behavior);
                    container.removeChild(node.content.shape);

                    shape = critter.create(node.content.type, node.radians, radius + assets.tileSize * (node.layer - 1));
                    node.content.shape = shape;
                    //encapsulate(node);

                    container.addChild(shape);
                    node.content.behavior.onHit(node, true);
                    scale = shape.scaleX;
                    shape.scaleX = scale * 0.35;
                    shape.scaleY = scale * 0.35;
                    createJs.Tween.get(shape).to({
                        scaleX: scale,
                        scaleY: scale
                    }, 500, createJs.Ease.backIn);
                    //onHit(node.layer,node.cell); //todo check outer match too
                    if (circularGraph.layerIsType(node.layer)) {
                        emptyLayer(node.layer, node.cell, true);
                    }
                    game.addScore(node.content.value, node.data());
                    game.goals.progress(new GoalProgress('change', 1, node.data()));
                    events.triggerShapeDetected('point', node.data());
                }
            }
        }

        function handleMove() {
            var nodeAction = circularGraph.nextNodeAction('move');
            if (nodeAction !== null) {
                var node = circularGraph.getNode(nodeAction.id);
                var target = circularGraph.getNode(nodeAction.data);
                if (node.content !== null && node.content.shape !== null && node.content !== 0 && !node.locked) { //, 500)) {

                    var object = node.content; //remember object
                    var distance = radius + assets.tileSize * (target.layer - 1);
                    container.addChild(object.shape);
                    createJs.Tween.get(object.shape).to({
                        x: Math.round(distance * Math.cos(target.radians)),
                        y: Math.round(distance * Math.sin(target.radians))
                    }, 250, createJs.Ease.backIn).call(function handleMoveDone() {
                        target.locked = false; //release target lock
                        fillNode(object, target.layer, target.cell); //create at target
                        container.removeChild(object.shape);
                    });
                    circularGraph.emptyNode(node.layer, node.cell); //remove origin
                } else {
                    console.error('cant move');
                }
            }
        }

        function handleEmpty() {
            var nodeAction = circularGraph.nextNodeAction('empty');
            if (nodeAction !== null) {
                var node = circularGraph.getNode(nodeAction.id);

                if (node.content !== null && node.content.value !== null && !node.locked) {
                    // createJs.Sound.play('click');
                    //score.add(node.content.value, node.content, container);

                    node.locked = true;
                    var scale = node.content.shape.scaleX;
                    createJs.Tween.get(node.content.shape).to({
                        scaleX: scale * 1.2,
                        scaleY: scale * 1.2
                    }, 50, createJs.Ease.backIn).call(function () {

                        createJs.Tween.get(node.content.shape).to({
                            scaleX: 0,
                            scaleY: 0
                        }, 100, createJs.Ease.backIn).call(function () {

                            node.locked = false;
                            container.removeChild(node.content.shape);
                            node.content.shape = null;
                            circularGraph.emptyNode(node.layer, node.cell);
                            //}
                            if (nodeAction.data && nodeAction.data.action && nodeAction.data.action === 'removed') {
                                handleShapeRemoved(nodeAction.data);
                            }
                        });
                    });
                }
            }
        }

        function handleShapeRemoved(shapeEvent) {
            switch (shapeEvent.type) {
                case 'stack':
                    game.addScore(Math.pow(shapeEvent.parts.length - 3, 2) * shapeEvent.nodeData.value * 5, shapeEvent.nodeData);
                    game.goals.progress(new GoalProgress('stack', 1, shapeEvent.nodeData, shapeEvent.parts.length));
                    break;
                case 'triangle':
                    game.addScore((shapeEvent.parts.length + 1) * shapeEvent.nodeData.value * shapeEvent.nodeData.layer, shapeEvent.nodeData);
                    game.goals.progress(new GoalProgress('triangle', 1, shapeEvent.nodeData));
                    break;
                case 'ring':
                    shields.addStrength(shapeEvent.nodeData.type, shapeEvent.nodeData.radians, shapeEvent.parts.length); //count
                    game.addScore(shapeEvent.nodeData.value * shapeEvent.parts.length * shapeEvent.nodeData.layer, shapeEvent.nodeData);
                    game.goals.progress(new GoalProgress('ring', 1, shapeEvent.nodeData));
                    game.goals.progress(new GoalProgress('ring-layer-' + shapeEvent.nodeData.layer, 1, shapeEvent.nodeData));
                    break;
            }
        }

        return {
            create: create,
            fillNode: fillNode,
            handleDestroy: handleDestroy,
            handleChangeType: handleChangeType,
            handleMove: handleMove,
            handleEmpty: handleEmpty
        };
    });