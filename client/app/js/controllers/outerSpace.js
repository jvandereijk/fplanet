/*globals define, require */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'models/graph/circularGraph',
        'helpers/shapeDetection',
        'helpers/util',
        'controllers/nodes',
        'actors/emitter',
        'actors/planet',
        'actors/critter'
    ],
    function (createJs, assets, game, events, circularGraph, shapeDetection, util, nodes, emitter, planet, critter) {
        'use strict';

        var container,
            containerGood,
            containerBad,
            hud,
            transform,
            paused,
            speed,
            speedIncrease,
            interval,
            minInterval,
            timeToNextobject,
            objectList,
            objectSize,
            maxDistance,
            stageConfig;

        function create(config) {
            stageConfig = config;
            if (container) {
                destroy();
            }
            container = new createJs.Container();
            containerGood = new createJs.Container();
            containerBad = new createJs.Container();

            // keep 0,0 in stage center:
            transform = {
                scaleX: 1,
                scaleY: 1,
                r: 0
            };

            paused = false;
            objectList = [];
            maxDistance = (stageConfig.planet.r + 20) * assets.tileSize;

            initOuterSpace(true);

            container.addChild(containerGood);
            container.addChild(containerBad);

            events.on('goalStarted', initOuterSpace);
            events.on('pointerTap', onPointerTap);
            events.on('pointerDoubletap', onPointerDoubletap);
            events.on('keyUp', onKeyUp);

            hud = require('ui/hud');

            return container;
        }

        function destroy() {
            events.off(initOuterSpace);
            events.off(onPointerTap);
            events.off(onPointerDoubletap);
            events.off(onKeyUp);
            emitter.destroy();
        }

        function initOuterSpace() {
            //overwrite level config with goal config when needed/possible
            var outerSpace = game.goals.current ? util.extend({}, game.level.outerSpace, game.goals.current.outerSpace) : JSON.parse(JSON.stringify(game.level.outerSpace));
            if (!outerSpace.types) {
                outerSpace.types = assets.defaults.critter; //get default critter types
            }

            interval = outerSpace.interval;
            minInterval = interval / 3;
            timeToNextobject = interval;

            speed = 10 * assets.tileSize * game.speed * outerSpace.speed || 0;
            speedIncrease = outerSpace.speedIncrease || 0;
            emitter.create(outerSpace);
        }

        //change changeable critters on tap
        function onPointerTap(id, event) {
            for (var i = objectList.length - 1; i >= 0; i--) { //check all touches
                for (var j = 0; j < event.gesture.touches.length; j++) {
                    var c = container.globalToLocal(event.gesture.touches[j].pageX, event.gesture.touches[j].pageY);
                    if (objectHitTest(objectList[i].shape, c.x, c.y)) {
                        objectList[i].behavior.onTap(objectList[i], containerGood);
                    }
                }
            }
        }

        //speed up critter interval on double tap
        function onPointerDoubletap(id, event) {
            timeToNextobject -= 1000; //speed up critter create sequence by tapping any where
        }

        function onKeyUp(id, event) {
            if (event.keyCode === 13) {
                timeToNextobject -= 1000; //speed up critter create sequence by pressing enter
            }
            if (event.keyCode === 32) {
                //change all changeables
                for (var i = objectList.length - 1; i >= 0; i--) {
                    objectList[i].behavior.onTap(objectList[i], containerGood);
                }
            }
        }

        function objectHitTest(shape, x, y) {
            var target = {
                x: shape.x - assets.tileSize * 0.5,
                y: shape.y - assets.tileSize * 0.5
            };
            return x > target.x && y < target.y + assets.tileSize && x < target.x + assets.tileSize && y > target.y;
        }

        function createobject() {
            if (objectList.length > 300) {
                //too many objects active
                return;
            }

            var object = emitter.createObject(maxDistance, speed);
            if (!object) {
                return;
            }
            if (object.type === 'delay') {
                timeToNextobject = interval;
            } else {
                object.shape = critter.create(object.type, 0, 0);
                object.shapeMarker = critter.setMarker(object.type);

                object.shape.alpha = 0;  //set to 0 before fade in
                containerGood.addChild(object.shape);

                hud.addShape(object.shapeMarker);


                objectList.push(object);
                timeToNextobject = interval / object.speed * assets.tileSize * 5 / game.speed;
            }
        }

        function togglePause() {
            paused = !paused;
        }

        function zoom(amount) {
            //if(isZooming){}
            var scale = amount < 0 ? 0.75 : 1;
            createJs.Tween.get(transform, {
                override: true
            }).to({
                scaleX: scale,
                scaleY: scale,
                alpha: amount < 0 ? 0.5 : 1
            }, 300, createJs.Ease.backOut);
            createJs.Tween.get(containerGood, {
                override: true
            }).to({
                alpha: amount < 0 ? 0.25 : 1
            }, 200, createJs.Ease.backOut);
        }

        function addMissile(missile) {
            containerBad.addChild(missile.shape);
        }

        function removeMissile(missile) {
            containerBad.removeChild(missile.shape);
        }

        function update(event) {
            //if (event.delta > 100) event.delta = 100; //dont go crazy on delta when app is out of focus/paused  > no longer eneded, using 'createJs.Ticker.maxDelta = 75' instead

            if (!paused && speed) {
                if (interval > minInterval) {
                    interval -= (event.delta * interval * 0.000001);
                }
                if (speedIncrease && speed < assets.tileSize * 18) {
                    speed += speedIncrease * event.delta * 0.001;
                }
                timeToNextobject -= event.delta;
                if (timeToNextobject <= 0) {
                    createobject();
                }
            }
            emitter.update(event);
            var i = objectList.length;
            while (i--) {
                if (i < 0) {
                    return;
                } //item removed
                var object = objectList[i];
                var delta = object.speed * event.delta / 1000 * game.speed;
                if (delta > objectSize) { //don't go crazy on movement
                    delta = objectSize * 0.75;
                }
                if (findDropSpot(object, delta)) {
                    //destroy object
                    objectList.splice(i--, 1);
                    containerGood.removeChild(object.shape);
                    if (object.shapeMarker) {
                        hud.removeShape(object.shapeMarker);
                    }
                    object = null;
                    return;
                }
                object.behavior.updateFalling(object, delta * game.speed);

                //recalculate delta, since object may be bouncing off
                delta = object.speed * event.delta / 1000 * game.speed;
                if (delta > objectSize) { //don't go crazy on movement
                    delta = objectSize * 0.75;
                }

                //todo: move next 3 lines to behavior

                object.distance -= delta; // gravity (sort of :)
                object.shape.x = object.distance * Math.cos(object.r * Math.PI / 180);
                object.shape.y = object.distance * Math.sin(object.r * Math.PI / 180);
                if (object.shape.alpha < 1) {
                    object.shape.alpha += event.delta * 0.001;
                }
                if (object.changeable && object.changeable.current < 0) {
                    critter.updateType(object.shape, object.changeable.subtypes[  Math.random() * object.changeable.subtypes.length | 0]);
                }
                updateMarker(object);
            }

            container.setTransform(transform.x, transform.y, transform.scaleX, transform.scaleY, transform.r);
        }

        function updateMarker(object) {
            var viewDistance, scale;
            if (object.shapeMarker) {
                viewDistance = stageConfig.y * assets.gameHeight;
                if (object.distance > viewDistance) {
                    object.shapeMarker.x = object.shape.x + assets.gameWidth * 0.5;
                    scale = ( (object.distance - viewDistance) / maxDistance);
                    object.shapeMarker.scaleX = scale;
                    object.shapeMarker.scaleY = scale;

                    if (object.changeable && object.changeable.current < 0) {
                        critter.setMarker(object.changeable.subtypes[  Math.random() * object.changeable.subtypes.length | 0], object.shapeMarker);
                    }
                } else {
                    hud.removeShape(object.shapeMarker);
                    object.shapeMarker = null;
                }
            }
        }

        function findDropSpot(object, delta) {
            if (object.distance < -maxDistance || object.distance > maxDistance) { //out of distance, destroy
                //score.add(-object.value, object.type, object.shape.x, object.shape.y, container); //negative score when blown to pieces
                return true; // true destroys object in outerspace
            }
            if (transform.scaleX < 1) {
                if (object.distance < stageConfig.planet.r * assets.tileSize * transform.scaleX) {
                    game.planetHealth -= 2;
                    planet.addExplosion();
                    return true;
                }
                return false;
            } //miss planet

            var layer = ((object.distance - stageConfig.planet.r * assets.tileSize + assets.tileSize) / assets.tileSize) + 0.5 | 0;
            if (layer > circularGraph.layerCount() || layer < 1) {
                return false;
            } // not in grid range (yet)

            return object.behavior.findDropSpot(object, layer, delta, assets.tileSize,
                function succes(o, l, c) {
                    if (o.changeable && o.changeable.current < 0) {  // if no type is selected on rainbow critter, choose a random one
                        o.type = o.changeable.subtypes[  Math.random() * o.changeable.subtypes.length | 0];
                        o.value = assets.defaults.critter[o.type].value;
                    }
                    return nodes.fillNode(o, l, c); //collision found callback
                }, function bounce(layer, cell) {
                    /*var i, stack = shapeDetection.findStack(circularGraph.getNode(layer + '_' + cell));
                     for (i = stack.length - 1; i >= 0; i--) {
                     circularGraph.addNodeAction('destroy', circularGraph.getNode(stack[i]), {
                     blowUpNeighbours: false,
                     delay: 100 * (stack.length - i) / speed * interval // faster blowups at faster drop speed
                     });
                     }
                     game.planetHealth -= 5 * stack.length;
                     */

                    object.speed *= -1; //bounce
                    return false; //false for not destroying
                });
            //return found;
        }


        return {
            create: create,
            update: update,
            destroy: destroy,
            zoom: zoom,
            addMissile: addMissile,
            removeMissile: removeMissile,
            togglePause: togglePause,
            get container() {
                return container;
            }
        };
    });