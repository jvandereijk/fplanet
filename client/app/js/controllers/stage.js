/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'models/interval',
        'helpers/anim',
        'helpers/shapeAnims',
        'models/graph/circularGraph',
        'models/GoalProgress',
        'controllers/nodes',
        'controllers/outerSpace',
        'controllers/enemies',
        'actors/shields',
        'actors/planet',
        'ui/score'
    ],
    function (createJs, assets, game, events, Interval, anim, shapeAnims, circularGraph, GoalProgress, nodes, outerSpace, enemies, shields, planet, score) {
        'use strict';

        var stageTransform,
            rotateStep,
            container,
            rotateGroupContainer,
            outerSpaceContainer,
            enemiesContainer,
            planetContainer,
            shieldContainer,
            shieldAlpha,
            nodeContainer,
            healthContainer,
            healthAlpha,
            graph,
            aiming,
            dragLockTimer,
            stacksReady;

        var emptyInterval = new Interval(20);
        var moveInterval = new Interval(20);
        var changeInterval = new Interval(20);
        var destroyInterval = new Interval(20);

        function create() {
            stageTransform = {
                x: 0,
                y: 0,
                scaleX: 1,
                scaleY: 1,
                r: 0
            };
            shieldAlpha = 0.4;
            rotateStep = 0;
            aiming = false;
            dragLockTimer = 0;
            stacksReady = [];
            graph = circularGraph.create(game.level.stage.layerCount);

            container = new createJs.Container();
            rotateGroupContainer = new createJs.Container();
            score.setParentContainer(container);

            container.addChild(enemiesContainer = enemies.create());
            container.addChild(healthContainer = new createJs.Container());

            healthAlpha = 0;
            showPlanetHealth();

            rotateGroupContainer.addChild(planetContainer = planet.create(game.level.stage));
            rotateGroupContainer.addChild(nodeContainer = nodes.create(game.level.stage));
            rotateGroupContainer.addChild(shieldContainer = shields.create(game.level.stage));
            shieldContainer.alpha = shieldAlpha;

            container.addChild(rotateGroupContainer);
            container.addChild(outerSpaceContainer = outerSpace.create(game.level.stage)); // keep in front of stage

            events.on('shapeDetected', onShapeDetected);
            events.on('planetHealthChanged', onPlanetHealthChanged);

            return container;
        }

        function destroy() {
            circularGraph.destroy();
            outerSpace.destroy();
            planet.destroy();

            events.off(onShapeDetected);
            events.off(onPlanetHealthChanged);
        }

        /*function onLevelExit() {
         anim.zoom(stageTransform, 0, 500, createJs.Ease.cubicInOut);
         }*/

        function update(event) {
            outerSpace.update(event);
            enemies.update(event);
            planet.update(event);
            var node;
            if (emptyInterval.update(event.delta)) {
                nodes.handleEmpty();
            }
            if (moveInterval.update(event.delta)) {
                nodes.handleMove();
            }
            if (changeInterval.update(event.delta)) {
                nodes.handleChangeType();
            }
            if (destroyInterval.update(event.delta)) {
                nodes.handleDestroy();

            }

            //update nodes (check for graph changes)
            for (var layer = 0; layer <= circularGraph.layerCount(); layer++) {
                var s = circularGraph.cellCount(layer);
                for (var cell = 0; cell < s; cell++) {
                    node = graph[layer + '_' + cell];
                    if (node.content !== null && node.content !== 0 && !node.locked) {
                        node.content.behavior.nodeUpdate(node, assets.tileSize, game.level.stage.planet.r);
                    }
                }
            }

            if (dragLockTimer > 0) {
                dragLockTimer -= event.delta;
                if (dragLockTimer < 0) dragLockTimer = 0;
            }

            stageTransform.r += game.level.stage.rotationSpeed * event.delta / 1000;
            while (stageTransform.r >= 360) {
                stageTransform.r -= 360;
            }
            while (stageTransform.r < 0) {
                stageTransform.r += 360;
            }

            rotateGroupContainer.setTransform(stageTransform.x, stageTransform.y, 1, 1, stageTransform.r); // rotate all
            healthContainer.setTransform(0, 0, stageTransform.scaleX, stageTransform.scaleY, 0);
            planetContainer.setTransform(0, 0, stageTransform.scaleX, stageTransform.scaleY, 0); // scale planet
            nodeContainer.setTransform(0, 0, stageTransform.scaleX, stageTransform.scaleY, 0); // scale nodes

            outerSpaceContainer.setTransform(stageTransform.x, stageTransform.y, 1, 1, 0); // for dragging

            //var shieldScale = 1 + (stageTransform.scaleX - 0.75) * 6;
            var shieldScale = stageTransform.scaleX * stageTransform.scaleX * stageTransform.scaleX;
            shieldContainer.setTransform(0, 0, shieldScale, shieldScale, 0); // scale planet
        }

        //draw shape detection indicator, and possibly launch defence missile
        function onShapeDetected(id, shapeEvent) {
            var shape = shapeAnims.create(shapeEvent, assets.tileSize * 0.25, createJs.Graphics.getRGB(255, 255, 200, 0.7), onEndAnimation);
            nodeContainer.addChild(shape);
            if (shapeEvent.type === 'stack') {
                stacksReady.push({
                    event: shapeEvent,
                    shape: shape
                });
            }
        }

        function onEndAnimation(shapeEvent, shape) {
            if (shapeEvent.type === 'stack') {
                //handleStackReady(shapeEvent, shape, 0);
                dragLockTimer = 500;
                endDrag();
            } else {
                if (shapeEvent.type === 'triangle') {
                    game.planetHealth += shapeEvent.parts.length * 2; //heal planet
                }
                nodeContainer.removeChild(shape);
            }
        }

        function handleStackReady(shapeEvent, shape) {
            if (nodeContainer.getChildIndex(shape) >= 0) { // check if shape still in scene or already destroyed by drag/aim
                if (game.level.stage.dragLocked) {
                    nodeContainer.removeChild(shape);
                } else if (aiming || true) {
                    var force = Math.sqrt(stageTransform.x * stageTransform.x + stageTransform.y * stageTransform.y) / (assets.tileSize * 4);
                    enemies.addAntiMissile(shapeEvent.parts[0], shapeEvent.parts[shapeEvent.parts.length - 1], rotateGroupContainer, force); //shoot laser beam/missile into enemy space
                    nodeContainer.removeChild(shape);
                } else {
                    //not aiming, don't shoot
                    anim.fade(shape, 0, 250);
                    anim.zoom(shape, 0, 250, null, true).call(function () {
                        nodeContainer.removeChild(shape);
                    });
                }
                for (var i = stacksReady.length - 1; i >= 0; i--) {
                    if (stacksReady[i].shape === shape) {
                        stacksReady.splice(i, 1);
                    }
                }
            }
        }

        function onPlanetHealthChanged(msg, health) {
            showPlanetHealth(health.newVal, health.maxVal);
        }

        function showPlanetHealth(current, max) {
            var i, color = '#fff', //(game.planetHealth > 70 ? '#9f9' : game.planetHealth > 40 ? '#ff9' : game.planetHealth > 15 ? '#f95' : '#f55'),
                shape = new createJs.Shape(),
                maxSegments = 24,
                healthPercentage = current !== undefined ? current / max : 1,
                bcolor = (healthPercentage > 0.7 ? '#05a' : healthPercentage > 0.4 ? '#aa5' : healthPercentage > 0.15 ? '#a51' : '#a11'),
                segments = maxSegments;

            if (current !== undefined) {
                healthAlpha = healthPercentage > 0.3 ? 0 : 0.7 - healthPercentage;
                segments = Math.ceil(healthPercentage * maxSegments);
                healthContainer.alpha = 0.75;
                anim.fade(healthContainer, healthAlpha, 3000, createJs.Ease.backIn);
            } else {
                healthContainer.alpha = 0;
            }

            healthContainer.removeAllChildren();

            var radius = game.level.stage.planet.r * assets.tileSize * 0.78;
            shape.graphics.setStrokeStyle(assets.tileSize * 0.12, 0, 1)
                .beginStroke(bcolor).arc(0, 0, radius, 0, Math.PI * 2, false);
            healthContainer.addChild(shape);
            for (i = 0; i < segments; i++) {
                healthContainer.addChild(createHealthSegment(radius, i, maxSegments, color, assets.tileSize * 0.06));
            }
        }

        function createHealthSegment(distance, index, max, color, weight) {
            var startAngle, endAngle, shape = new createJs.Shape();

            startAngle = (1 - (index + 1) / max) * Math.PI * 2 + 0.015 - Math.PI / 2;
            endAngle = (1 - (index) / max) * Math.PI * 2 - 0.015 - Math.PI / 2;
            shape.graphics.setStrokeStyle(weight, 0, 1)
                .beginStroke(color)
                .arc(0, 0, distance, startAngle, endAngle, false);
            return shape;
        }


        function rotation(r) {
            if (typeof r === 'number' && isFinite(r)) {
                stageTransform.r = r;
            } else {
                return stageTransform.r;
            }
        }

        function rotate(amount) {
            if (typeof amount === 'number' && isFinite(amount)) {
                stageTransform.r += amount;
            }
        }

        function zoom(amount) {
            var scale = amount < 0 ? 0.75 : (amount > 0 ? 1.3 : 1);

            anim.zoom(stageTransform, scale, 300, createJs.Ease.backOut);
            anim.fade(nodeContainer, amount >= 0 ? 1 : 0.1, 200, createJs.Ease.backOut); //Math.pow(scale, 5), 200, createJs.Ease.backOut);
            anim.fade(shieldContainer, amount < 0 ? 1 : shieldAlpha, 200, createJs.Ease.backOut);

            anim.fade(healthContainer, amount >= 0 ? healthAlpha : 0.75, amount >= 0 ? 1000 : 200);
            //console.log('z:' + (amount >= 0 ? healthAlpha : 0.75));

            planet.setActive(amount >= 0);
            shields.setActive(amount < 0);
            outerSpace.zoom(amount);
            enemies.zoom(amount);

        }

        function drag(x, y) {
            if (dragLockTimer < 1) {
                aiming = true;
                if (y < assets.tileSize * -4) {
                    y = assets.tileSize * -4;
                }
                if (x < assets.tileSize * -4) {
                    x = assets.tileSize * -4;
                }
                if (x > assets.tileSize * 4) {
                    x = assets.tileSize * 4;
                }
                stageTransform.x = -x;
                stageTransform.y = -y;
            }
            return dragLockTimer < 1;
        }

        function endDrag() {

            if (stageTransform.y > assets.tileSize * 3) {
                game.goals.progress(new GoalProgress('drag-down', 1));
            }
            //shoot ready stacks into space
            for (var i = stacksReady.length - 1; i >= 0; i--) {
                handleStackReady(stacksReady[i].event, stacksReady[i].shape);
            }
            aiming = false;
            anim.translate(stageTransform, 0, 0, 200, createJs.Ease.elasticOut);
        }

        return {
            create: create,
            update: update,
            destroy: destroy,
            rotation: rotation,
            rotate: rotate,
            zoom: zoom,
            drag: drag,
            endDrag: endDrag
        };
    });