/*globals define */
define(['core/events',
        'models/goalProgress'
    ],
    function (events, GoalProgress) {
        'use strict';

        var goals,
            startTime,
            currentGoal,
            currentTime,
            surviveTimeElapsed;

        function create(myGoals) {
            var i, j;

            goals = myGoals;
            currentGoal = 0;
            startTime = 0;
            currentTime = 0;
            surviveTimeElapsed = 0;
            if (goals) {
                for (i = 0; i < goals.length; i++) {
                    for (j = 0; j < goals[i].targets.length; j++) {
                        var target = goals[i].targets[j];
                        if (target.progress === undefined) {
                            target.progress = 0;
                        }
                        if (target.compare === 'max') {
                            target.complete = true;
                        }
                    }
                }
            }
        }

        function update(event) {
            if (!goals || !goals.length) {
                return;
            }

            var i, target, goal = goals[currentGoal];
            surviveTimeElapsed += event.delta;
            currentTime += event.delta;

            if (!goal.started) {
                startTime += event.delta;
                if (startTime > goal.delay && !goal.started) {

                    //start goal start sequence
                    goal.started = true;
                    events.trigger('goalStarted', goal);
                }
            } else if (surviveTimeElapsed > 1000) {
                for (i = 0; i < goal.targets.length; i++) {
                    if (goal.targets[i].type === 'survive') {
                        progress(new GoalProgress('survive', 1));
                    }
                }
                surviveTimeElapsed -= 1000;
            } else {
                for (i = 0; i < goal.targets.length; i++) {
                    target = goal.targets[i];
                    if (target.maxDelay && target.progress > 0) {
                        if (currentTime - target.lastTime > target.maxDelay) {
                            checkComplete(target);
                            if (!target.complete) {
                                //console.log('timed out reset');
                                target.progress -= 1; //reset timer
                                events.trigger('goalProgress', goal);
                            }
                        }
                    }
                }
            }
        }

        //todo: add destroy method + cleanup
        function progress(goalProgress) {
            var type = goalProgress.type,
                amount = goalProgress.amount,
                subtype = goalProgress.nodeData ? goalProgress.nodeData.type : null;

            if (!goals || !goals.length) {
                return;
            }
            if (currentGoal >= goals.length) {
                return; //level already completed
            }

            var i, myReward, goal = goals[currentGoal],
                targetsComplete = 0;

            if (goal.started) {
                for (i = 0; i < goal.targets.length; i++) {
                    var target = goal.targets[i];
                    if (target.type === type && (target.subtype === undefined || target.subtype === subtype)) {

                        if (!target.additional || (goalProgress.additional && target.additional === goalProgress.additional)) { //if additional goal, check that first
                            if (target.compare === '==') {  //== needs exact amount, no adding of progress amounts
                                target.progress = amount;
                            } else {
                                target.progress += amount; //adding allowed
                            }

                            if (target.maxDelay) {
                                target.lastTime = currentTime;
                            }
                            checkComplete(target);
                        }

                        events.trigger('goalProgress', goals[currentGoal]);
                    }
                    if (target.complete) {
                        targetsComplete++;
                    }
                }
                if (targetsComplete >= goal.numberOfTargetsRequired && !goal.complete) {
                    goal.complete = true;

                    myReward = goal.reward ? goal.reward.score : null;
                    if (myReward) {
                        events.trigger('addScore', myReward);
                    }

                    events.trigger('goalCompleted', goals[currentGoal]);
                    if (currentGoal + 1 >= goals.length) {
                        events.trigger('levelExit', goals[currentGoal]);
                    }
                }
            }
        }

        function checkComplete(target) {

            switch (target.compare) {
                case '==':
                    target.complete = target.progress === target.amount;
                    break;
                case '>=':
                    target.complete = target.progress >= target.amount;
                    break;
                case '<=':
                    target.complete = target.progress <= target.amount;
                    break;
                case 'max':
                    target.complete = target.progress <= target.amount;
                    if (target.progress > target.amount) {
                        events.trigger('gameOver', 'goal-failed');
                    }
                    break;
                case 'abs':
                    if (Math.abs(target.progress) >= target.amount) {
                        target.complete = true;
                    }
                    break;
                case 'cantCompleteBwahahaa':
                    break;
                default:
                    if (target.progress >= target.amount) {
                        target.complete = true;
                    }
            }
        }

        function findCurrentTarget(type) {
            if (goals) {
                var i, j;
                for (i = 0; i < goals.length; i++) {
                    for (j = 0; j < goals[i].targets.length; j++) {
                        if (goals[i].targets[j].type === type) return goals[i];
                    }
                }
            }
            return false;
        }

        events.on('goalCompletedConfirmed', function (/*id, goal*/) {

            //go to next goal
            startTime = 0;
            currentGoal++;

            //check if level is completed
            if (currentGoal >= goals.length) {
                //start level completed sequence
                currentGoal = 0;
                events.trigger('levelCompleted', true);
            }
        });

        return {
            create: create,
            update: update,
            progress: progress,
            findCurrentTarget: findCurrentTarget,
            get current() {
                return goals ? goals[currentGoal] : null;
            }
        };
    });