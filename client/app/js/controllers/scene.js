/*globals define */
define(['lib/create',
        'core/assets',
        'core/game',
        'core/events',
        'controllers/stage',
        'actors/background',
        'models/goalProgress',
        'helpers/anim',
        'helpers/geometric'
    ],
    function (createJs, assets, game, events, stage, background, GoalProgress, anim, geometric) {
        'use strict';

        var canvas,
            container,
            rootScale,
            startRotation,
            startAngle,
            prevAngle,
            planetDrag,

            stageContainer,
            backgroundContainer,
            sceneStarted,
            levelFinished,
            stageTransform,

            preventRotateTimer = 0, //delay to prevent rotate after zooming in or out with pinch

            keyDownPressed,
            keyLeftPressed,
            keyRightPressed,
            keyRotateSpeed,
            keyDegreeschanged,
            keyDragX,
            keyDragY,
            keyDragR;

        function create(canvasObj) {
            if (container) destroy();
            canvas = canvasObj;
            sceneStarted = false;
            levelFinished = false;
            keyRotateSpeed = 1;
            keyDegreeschanged = 0;
            prevAngle = 0;
            container = new createJs.Container();
            backgroundContainer = background.create();
            container.addChild(backgroundContainer);

            events.on('pointerDragstart', onPointerDragstart);
            events.on('pointerDrag', onPointerDrag);
            events.on('pointerRelease', onPointerRelease);
            events.on('pointerPinchin', onPointerPinchin);
            events.on('pointerPinchout', onPointerPinchout);
            events.on('keyDown', onKeyDown);
            events.on('keyUp', onKeyUp);
            events.on('stateChanged', onStateChanged);
            events.on('levelCompleted', onLevelCompleted);
            events.on('levelStarted', onLevelStarted);
            events.on('levelExit', onLevelExit);
            events.on('cameraShake', onCameraShake);
            events.on('inputReset', onInputReset);
            container.set({
                regX: assets.gameWidth / 2,
                regY: assets.gameHeight / 2,
                x: assets.gameWidth / 2,
                y: assets.gameHeight / 2
            });
            rootScale = canvas.height / assets.gameHeight;
            return container;
        }

        function update(event) {

            if (preventRotateTimer > 0) {
                preventRotateTimer -= event.delta;
            } else preventRotateTimer = 0;
            background.update(event);
            switch (game.state) {
                case assets.enums.gameStates.Playing:
                    processInput(event);
                    game.update(event);
                    if (!levelFinished) stage.update(event);
                    break;
            }
            if (stageContainer) stageContainer.setTransform(stageTransform.x, stageTransform.y, stageTransform.scaleX, stageTransform.scaleY); // scale planet
        }

        function destroy() {
            events.off(onPointerDragstart);
            events.off(onPointerDrag);
            events.off(onPointerRelease);
            events.off(onPointerPinchin);
            events.off(onPointerPinchout);
            events.off(onKeyDown);
            events.off(onKeyUp);
            events.off(onStateChanged);
            events.off(onLevelStarted);
            events.off(onLevelExit);
            events.off(onCameraShake);
            events.off(onInputReset);
            stage.destroy();
        }

        function initLevel(nr) {
            events.trigger('levelLoading', true);

            setTimeout(function () {
                levelFinished = false;
                game.initLevel(nr);

                stageTransform = {
                    x: assets.gameWidth * game.level.stage.x,
                    y: assets.gameHeight * game.level.stage.y,
                    scaleX: 0,
                    scaleY: 0
                };
                if (backgroundContainer) container.removeChild(backgroundContainer);
                if (stageContainer) container.removeChild(stageContainer);

                container.addChild(backgroundContainer = background.create());
                container.addChild(stageContainer = stage.create());

                sceneStarted = true;
                game.state = assets.enums.gameStates.Playing;
                events.trigger('levelStarted', true);
            }, 100); //allow loading events to trigger
        }

        function quit() {
            if (sceneStarted) {
                stage.destroy();
                sceneStarted = false;
                game.state = assets.enums.gameStates.Menu;
                background.fade(0.2, 1000, createJs.Ease.cubicInOut);
            }
        }

        function pause() {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                game.state = assets.enums.gameStates.Paused;
            }
        }

        function resume() {
            if (sceneStarted && game.state === assets.enums.gameStates.Paused) {
                game.state = assets.enums.gameStates.Playing;
                //anim.zoom(stageTransform, 1, 250, createJs.Ease.quadOut);
            }
        }

        //fade game when paused
        function onStateChanged(msg, state) {
            if (container) {
                if (state.newVal === assets.enums.gameStates.Playing) {
                    stageContainer.uncache();
                    backgroundContainer.uncache();
                    createJs.Tween.get(container, {
                        override: true
                    }).to({
                        alpha: 1
                    }, 200, createJs.Ease.easeIn);
                } else {
                    createJs.Tween.get(container, {
                        override: true
                    }).to({
                        alpha: 0.9
                    }, 200, createJs.Ease.easeOut);
                }
                if (stageTransform) {
                    if (state.newVal === assets.enums.gameStates.Paused) {
                        anim.zoom(stageTransform, 5, 250, createJs.Ease.quadOut);
                    }
                    if (state.newVal === assets.enums.gameStates.Menu) {
                        anim.zoom(stageTransform, 100, 500, createJs.Ease.quadOut);
                    }
                    if (state.newVal === assets.enums.gameStates.Playing) {
                        anim.zoom(stageTransform, 1, 250, createJs.Ease.quadOut);
                    }
                }
                if (state.newVal === assets.enums.gameStates.Paused) {
                    onInputReset();
                }
            }
        }

        function onLevelCompleted(id, goal) {
            anim.zoom(stageTransform, 0, 1000, createJs.Ease.cubicInOut);
            anim.fade(backgroundContainer, 0.2, 1000, createJs.Ease.cubicInOut).call(function () {

                quit();
                var next = assets.nextLevel(game.mode, game.levelId);
                if (next) {
                    initLevel(next.id);
                } //else 'no more levels found');

            });
        }

        function onLevelStarted() {
            anim.zoom(stageTransform, 1, 1000, createJs.Ease.cubicInOut);
            background.fade(1, 1000, createJs.Ease.cubicInOut);
        }

        function onLevelExit() {
            levelFinished = true;
            //outerSpace.togglePause();
        }

        function onInputReset() {
            keyRotateSpeed = 1;
            keyDegreeschanged = 0;
            prevAngle = 0;
            keyDownPressed = false;
            keyLeftPressed = false;
            keyRightPressed = false;
            keyDragY = 0;
            keyDragX = 0;
            keyDragR = 0;
            endDrag();
        }

        //process user input
        function onPointerDragstart(id, event) {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                event.gesture.preventDefault();

                //for (var t = 0, len = touches.length; t < 1 /*< len*/ ; t++) {
                var px = (window.innerWidth - (canvas.width/window.devicePixelRatio || 1)) / 2; //padding outside canvas
                var py = (window.innerHeight - (canvas.height/window.devicePixelRatio || 1)) / 2; //padding outside canvas
                var x = (canvas.width/window.devicePixelRatio || 1) * game.level.stage.x - event.gesture.touches[0].pageX + px;
                var y = (canvas.height/window.devicePixelRatio || 1) * game.level.stage.y - event.gesture.touches[0].pageY + py;

                if (!game.level.stage.dragLocked) {
                    var planetRadius = (game.level.stage.planet.r * 0.5) * assets.tileSize * 1.6 * rootScale;
                    planetDrag = geometric.isInsideCircle(x, y, planetRadius);
                }

                if (!preventRotateTimer) {
                    startRotation = stage.rotation();
                    if (planetDrag) {
                        y -= game.level.stage.planet.r * assets.tileSize; // when drasgging planet, adjust center of rotation for aiming
                    }
                    startAngle = Math.round(Math.getDegrees(x, y) * 180 / Math.PI).maxToZero(360);
                }
            }
        }

        function onPointerDrag(id, event) {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                var touches = event.gesture.touches;
                event.gesture.preventDefault();

                //for (var t = 0, len = touches.length; t < 1 /*< len*/ ; t++) {
                var px = (window.innerWidth - canvas.width/window.devicePixelRatio || 1) / 2; //padding outside canvas
                var py = (window.innerHeight - canvas.height/window.devicePixelRatio || 1) / 2; //padding outside canvas
                var x = (canvas.width/window.devicePixelRatio || 1) * game.level.stage.x - event.gesture.touches[0].pageX + px;
                var y = (canvas.height/window.devicePixelRatio || 1) * game.level.stage.y - event.gesture.touches[0].pageY + py;

                if (!game.level.stage.dragLocked && planetDrag) {
                    if (y < game.level.stage.planet.r * assets.tileSize * 2) {
                        planetDrag = stage.drag(x, y);
                    } else {
                        endDrag();
                    }
                }


                if (!preventRotateTimer) {
                    if (planetDrag) y -= game.level.stage.planet.r * assets.tileSize; // when drasgging planet, adjust center of rotation for aiming
                    var angle = Math.round(Math.getDegrees(x, y) * 180 / Math.PI).maxToZero(360);

                    //find delta change
                    var degreesChanged = (angle - prevAngle).maxToZero(360);
                    if (degreesChanged > 180) {
                        degreesChanged -= 360;
                    }
                    if (degreesChanged > 1 || degreesChanged < -1) {
                        prevAngle = angle;
                        game.goals.progress(new GoalProgress('rotate', degreesChanged));
                    }

                    var r = startRotation + startAngle - angle;
                    stage.rotation(r);
                    background.rotation(r);

                }

            }
        }

        function onPointerRelease(id, event) {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                event.gesture.preventDefault();
                if (!game.level.stage.dragLocked) {
                    endDrag();
                }
            }
        }

        function onPointerPinchin(id, event) {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                event.gesture.preventDefault();
                preventRotateTimer = 100;
                zoomOut();
            }
        }

        function onPointerPinchout(id, event) {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                event.gesture.preventDefault();
                preventRotateTimer = 150;
                zoomIn();
            }
        }

        function onKeyDown(id, evt) {
            if (evt.keyCode === 80 || evt.keyCode === 27) { // 'p' or 'esc'  for pause/unpause
                if (game.state === assets.enums.gameStates.Playing) {
                    pause();
                } else if (game.state === assets.enums.gameStates.Paused) {
                    resume();
                }
            }

            /*if (evt.keyCode === 77) { // 'm'  for pause objects from outerSpace
             outerSpace.togglePause();
             }*/
            if (evt.keyCode === 68) { // 'd'  for debug mode
                game.debug = !game.debug;
            }
            if (evt.keyCode === 37) { // left arrow
                if (!keyLeftPressed && !keyRightPressed) {
                    keyRotateSpeed = -0.5;
                    keyLeftPressed = true;
                }
            }
            if (evt.keyCode === 39) { // right arrow
                if (!keyLeftPressed && !keyRightPressed) {
                    keyRotateSpeed = 0.5;
                    keyRightPressed = true;
                }
            }
            if (evt.keyCode === 38) { // up arrow
                zoomOut();
            }
            if (evt.keyCode === 40) { // down arrow
                if (!game.level.stage.dragLocked && (!keyDownPressed || !planetDrag)) {
                    keyDragY = 0;
                    keyDragX = 0;
                    keyDragR = 0;
                    keyDownPressed = true;
                    planetDrag = true;
                    startRotation = stage.rotation();
                }
            }
        }

        function onKeyUp(id, evt) {
            if (evt.keyCode === 38) { // up arrow
                zoomIn();
            }
            if (evt.keyCode === 37) { // left arrow
                keyLeftPressed = false;
            }
            if (evt.keyCode === 39) { // right arrow
                keyRightPressed = false;
            }
            if (evt.keyCode === 40) { // down arrow
                if (!game.level.stage.dragLocked) {
                    endDrag();
                }
            }
        }

        function processInput(event) {
            var degrees = event.delta * 0.1;
            //drag
            if (keyDownPressed && !game.level.stage.dragLocked) {
                if (keyDragY > -assets.tileSize * 4) {
                    keyDragY -= event.delta * 0.2 - keyDragY * 0.2;
                }

                if (keyLeftPressed) {
                    //if (keyDragX < assets.tileSize * 4) keyDragX += event.delta * 0.2 + keyDragX * 0.05;
                    keyDragR += event.delta * 0.05;
                }
                if (keyRightPressed) {
                    //if (keyDragX > -assets.tileSize * 4) keyDragX -= event.delta * 0.2 - keyDragX * 0.05;
                    keyDragR -= event.delta * 0.05;
                }
                if (!stage.drag(keyDragX, keyDragY)) {
                    //reste if drag not allowed
                    keyDragX = 0;
                    keyDragY = 0;
                    keyDragR = 0;
                    keyDownPressed = false;
                }
                //console.log(keyDragR);
                stage.rotation(startRotation + keyDragR); //max 30 degrees
                background.rotation(startRotation + keyDragR);

            } else {
                //rotate
                if (keyLeftPressed) {
                    keyRotateSpeed -= 0.25;
                    if (keyRotateSpeed < -3) keyRotateSpeed = -3;
                } else if (keyRightPressed) {
                    keyRotateSpeed += 0.25;
                    if (keyRotateSpeed > 3) keyRotateSpeed = 3;
                } else if (keyRotateSpeed > 0) {
                    keyRotateSpeed -= 0.5;
                    if (keyRotateSpeed < 0) keyRotateSpeed = 0;
                } else if (keyRotateSpeed < 0) {
                    keyRotateSpeed += 0.5;
                    if (keyRotateSpeed > 0) keyRotateSpeed = 0;
                }
                degrees *= keyRotateSpeed;
                if (degrees) {
                    stage.rotate(degrees);
                    background.rotate(degrees);
                    keyDegreeschanged += degrees;
                }

                if (keyDegreeschanged > 5) {
                    game.goals.progress(new GoalProgress('rotate', 5));
                    keyDegreeschanged -= 5;
                }
                if (keyDegreeschanged < -5) {
                    game.goals.progress(new GoalProgress('rotate', -5));
                    keyDegreeschanged += 5;
                }
            }
        }

        function endDrag() {
            if (planetDrag) {
                //console.log('end drag');
                stage.rotation(startRotation);
                background.rotate(startRotation);
                stage.endDrag();
                keyDownPressed = false;
                planetDrag = false;
            }
        }

        function zoomOut() {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing && !game.level.stage.zoomLocked) {
                stage.zoom(-1);
                background.zoom(-1);
                //if (game.speedMultiplier === 1) game.speedMultiplier = 0.01; //slow down speed, speed will slowly recover to 0.5 in game.js while zoomed out
                game.speedMultiplier = 0.125;
                game.goals.progress(new GoalProgress('zoomOut', 1));
            }
        }

        function zoomIn() {
            if (sceneStarted && game.state === assets.enums.gameStates.Playing) {
                stage.zoom(0);
                background.zoom(0);
                game.speedMultiplier = 1;
                game.goals.progress(new GoalProgress('zoomIn', 1));
            }
        }

        function onCameraShake(msg, force) {
            anim.shake(container, 25, force, createJs.Ease.linear);
        }

        return {
            create: create,
            initLevel: initLevel,
            quit: quit,
            resume: resume,
            update: update,
            destroy: destroy,
            get isStarted() {
                return sceneStarted;
            }
        };
    });