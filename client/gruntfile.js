    // Voor connect task
    var mountFolder = function (connect, dir) {
        'use strict';
        return connect.static(require('path').resolve(dir));
    };

    /*global module*/
    module.exports = function (grunt) {
        'use strict';

        grunt.initConfig({

            connect: {
                options: {
                    port: 9007,
                    // change this to '0.0.0.0' to access the server from outside
                    hostname: '0.0.0.0' //'localhost'
                    //keepalive: true
                },
                dev: {
                    options: {
                        middleware: function (connect) {
                            return [
                                mountFolder(connect, 'app')
                                ];
                        }
                    }
                },
                build: {
                    options: {
                        middleware: function (connect) {
                            return [
                                mountFolder(connect, 'dist')
                                ];
                        }
                    }
                }
            },

            open: {
                dev: {
                    path: 'http://localhost:<%= connect.options.port %>'
                },
                build: {
                    path: 'http://localhost:<%= connect.options.port %>'
                }
            },

            watch: {
                dev: {
                    files: ['app/data/levels/**/*.json'],
                    tasks: ['concat']
                }
            },

            concat: {
                options: {
                    banner: '[',
                    separator: ',',
                    footer: ']',
                    process: function (src, filepath) {
                        //var id = filepath.substr(16, filepath.length - 21).replace('/level', '');
                        var ids = filepath.substr(16, filepath.length - 21).split('/');
                        var head = '\n    "mode": "' + ids[0] + '",' +
                            '\n    "id": "' + ids[1].replace('level-', '') + '",';

                        return src.replace('{', '{' + head);

                        //return src.replace('{', '{\n    "id": "' + id + '",');
                    }
                },
                dist: {
                    src: ['app/data/levels/**/*.json'],
                    dest: 'app/data/levels.json'
                }
            },

            //todo:path require.js to work with cocoon.js ? see: http://blog.ludei.com/cocoonjs-a-survival-guide/
            requirejs: {
                compile: {
                    options: {
                        mainConfigFile: 'app/js/config.js',
                        baseUrl: 'app/js',
                        name: 'main',
                        include: ['config'],
                        out: 'dist/game.js',
                        uglify: {
                            toplevel: true,
                            max_line_length: 1000
                        }
                    }
                }
            }
        });

        grunt.registerTask('dev', function (target) {
            grunt.task.run([
                'connect:dev',
                'open:dev',
                'watch:dev'
            ]);
        });

        grunt.registerTask('build', function (target) {
            grunt.task.run([
                'requirejs',
                'connect:build',
                'open:build',
                'watch:dev'
            ]);
        });

        grunt.registerTask('resources', function (target) {
            grunt.task.run([
                'concat'
            ]);
        });

        grunt.loadNpmTasks('grunt-contrib-connect');
        grunt.loadNpmTasks('grunt-open');
        grunt.loadNpmTasks('grunt-contrib-requirejs');
        //grunt.loadNpmTasks('grunt-contrib-uglify');
        //grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-watch');

        // Default task(s).
        grunt.registerTask('default', ['dev']);
    };