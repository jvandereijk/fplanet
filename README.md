#F the planet 
(save the planet by working together)

##Documentation:
* [Wiki](https://bitbucket.org/jvandereijk/fplanet/wiki/Home)
* [Prototype](http://1o43tp.axshare.com)
* [Pinterest Roy](http://www.pinterest.com/boompje/f_planet_game/)
* [Pinterest Martijn](http://www.pinterest.com/IxD4all/comics-characters-and-gaming/)
* [Dev links](https://bitbucket.org/jvandereijk/fplanet/wiki/devlinks)

##Demo
* [v0.4](http://demo.jackvandereijk.nl/v0.4)
* [v0.5](http://demo.jackvandereijk.nl/v0.5-xSw3md)
* [v0.9](http://demo.jackvandereijk.nl/v0.9-ep)
* [v0.10](http://demo.jackvandereijk.nl/v0.10-je)
* [v0.13](http://demo.jackvandereijk.nl/v0.13-oo)
* [v0.14](http://demo.jackvandereijk.nl/v0.14-ju/)
* [v0.15](http://demo.jackvandereijk.nl/v0.15-aup)

##Downloads:
* latest project [zip file](https://bitbucket.org/jvandereijk/fplanet/get/master.zip)

##Running game locally:
* install [node.js](http://nodejs.org)
* install [grunt.js](http://gruntjs.com/getting-started)
* clone fplanet repository (see 'Clone' button on [fplanet](https://bitbucket.org/jvandereijk/fplanet) for instructions)
* open console in fplanet project directory 
* type: 'npm install'

From now on you can just type 'grunt' in the console in the project directory to run.